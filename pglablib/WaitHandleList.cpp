﻿#include "WaitHandleList.h"
#include "win32event.h"

WaitHandleList::WaitHandleList() = default;
WaitHandleList::~WaitHandleList() = default;

WaitResultValue WaitHandleList::add(HANDLE h)
{
	m_waitHandles.push_back(h);
	return WAIT_OBJECT_0 + static_cast<DWORD>(m_waitHandles.size() - 1);
}

WaitResultValue WaitHandleList::add(Win32Event &e)
{
	return add(e.handle());
}

DWORD WaitHandleList::count() const
{
	return static_cast<DWORD>(m_waitHandles.size());
}

void WaitHandleList::clear()
{
	m_waitHandles.clear();
}

WaitHandleList::operator const HANDLE*() const
{
	return m_waitHandles.data();
}
