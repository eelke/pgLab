﻿#ifndef QUERYGENERATOR_H
#define QUERYGENERATOR_H

#include <QString>
#include <memory>

class PgDatabaseCatalog;
class PgClass;

namespace Querygen {


class QueryGeneratorFactory;
using QueryGeneratorFactoryPtr = std::shared_ptr<QueryGeneratorFactory>;

class Update;
using UpdatePtr = std::shared_ptr<Update>;

class ColumnRef;
using ColumnRefPtr = std::shared_ptr<ColumnRef>;

class FQTableName {
public:
	QString nsName;
	QString tableName;
};


class ColumnRef {
public:
	virtual QString getFCQS() const = 0;
};

 class Update {
public:
	Update(QString ns, QString tbl, QString alias)
		: table{ns, tbl}
	{}

private:
	FQTableName table;
	QString tableAlias;
};

using UpdatePtr = std::shared_ptr<Update>;


class QueryGeneratorFactory {
public:
	QueryGeneratorFactory(std::shared_ptr<PgDatabaseCatalog> catalog);

	UpdatePtr update(QString ns, QString table_name, QString alias = QString());
	UpdatePtr update(const PgClass &table_class, QString alias = QString());
private:
	std::shared_ptr<PgDatabaseCatalog> m_catalog;
};

} // end namespace Querygen

#endif // QUERYGENERATOR_H
