﻿#ifndef WIN32EVENT_H
#define WIN32EVENT_H

#include <WinSock2.h>

#include <windows.h>
/** Simpel wrapper around a Win32 Event object.

Mostly to make cleanup automatic.*/
class Win32Event {
public:
	enum class Reset { Auto=0, Manual=1 };
	enum class Initial { Clear=0, Set=1 };

	Win32Event(Reset r, Initial is)
		: hEvent(CreateEvent(
			nullptr,  // _In_opt_ LPSECURITY_ATTRIBUTES lpEventAttributes,
			BOOL(r),  // _In_     BOOL                  bManualReset,
			BOOL(is), // _In_     BOOL                  bInitialState,
			nullptr   //_In_opt_ LPCTSTR               lpName
			))
	{}

	Win32Event(Reset r, Initial is, int sock, long net_events)
		: Win32Event(r, is)
	{
		WSAEventSelect(sock, hEvent, net_events);
	}

	~Win32Event()
	{
		CloseHandle(hEvent);
	}

	Win32Event(const Win32Event &) = delete;
	Win32Event &operator=(const Win32Event &) = delete;

	void set() { SetEvent(hEvent); }

	void reset() { ResetEvent(hEvent); }

	HANDLE handle() { return hEvent; }
private:
	HANDLE hEvent;
};

#endif // WIN32EVENT_H
