#pragma once

#include <QString>
#include <QStringList>

class ConnectionConfig;

class TestConnectionResult {
public:
    /// Creates an instance signalling failure
    static TestConnectionResult Failed(QString msg);
    /// Creates a result instance signalling success
    static TestConnectionResult Success(QStringList databases);

    TestConnectionResult() = default;

    bool ok() const;
    QString message() const;
    const QStringList& databases() const;
private:
    bool Ok;
    QString Message;
    QStringList Databases;

    TestConnectionResult(bool ok, QString msg, QStringList databases);
};

/// Test if a connection can be opened using the details in cc
///
/// It returns either the error message from the string on failure
/// or a list of databases the details give access to.
TestConnectionResult TestConnection(const ConnectionConfig &cc);
