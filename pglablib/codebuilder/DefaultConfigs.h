﻿#ifndef DEFAULTCONFIGS_H
#define DEFAULTCONFIGS_H

#include <memory>

class LanguageConfig;
class PgDatabaseCatalog;

//std::shared_ptr<const LanguageConfig> getDefaultCppLanguageConfig();
std::shared_ptr<const LanguageConfig> getPglabCppLanguageConfig(std::shared_ptr<PgDatabaseCatalog> catalog);

#endif // DEFAULTCONFIGS_H
