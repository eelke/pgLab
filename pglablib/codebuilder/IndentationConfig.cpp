﻿#include "IndentationConfig.h"

IndentationConfig::IndentationConfig() = default;

IndentationConfig::IndentationConfig(int tab_size, int indentation_size, bool use_tabs)
	: m_tabSize(tab_size)
	, m_indentationSize(indentation_size)
	, m_useTabs(use_tabs)
{}

/** Returns a string with the right amount of tabs and spaces for the
 * requested indentation level.
 */
QString IndentationConfig::getIndentString(int level) const
{
	int spaces = level * m_indentationSize;
	int tabs = 0;
	if (m_useTabs) {
		tabs = spaces / m_tabSize;
		spaces -= tabs * m_tabSize;
	}

	if (tabs > 0)
		return QString(tabs, '\t') + QString(spaces, ' ');
	else
		return QString(spaces, ' ');
}
