﻿#ifndef CODEBUILDER_H
#define CODEBUILDER_H

#include "Pgsql_declare.h"
#include <QString>
#include <vector>
#include "TypeMappings.h"

class LanguageConfig;
class QTextStream;

class CodeBuilder {
public:
	class ColumnData {
	public:
		Oid oid;
		QString columnName;
		QString varName; ///< either field of the struct or in loop local var name
	};
	using ColumnDataList = std::vector<ColumnData>;

	ColumnDataList createColumnDataList(const Pgsql::Result &result) const;

	void setLanguageConfig(std::shared_ptr<const LanguageConfig> config){
		m_configuration = config;
	}

	void GenCodeForExecutingQuery(QTextStream &q, const QString &query, const Pgsql::Result &result, QString structname);
	void GenReturnStructDefinition(QTextStream &q, const ColumnDataList &columns, QString structname) const;

// Generating code for performing query and going through the result
// - Code for executing the query
// - Code for looping the result
// - Code for processing a single row
//  - Declaration of struct for holding single row result
	QString columnNameToVariableName(QString column_name) const;
	TypeMappingResult getTypeName(Oid dbtype) const;
private:

	std::shared_ptr<const LanguageConfig> m_configuration;

	void genStructFields(QTextStream &q, const ColumnDataList &columns) const;
	void genFieldRetrieval(QTextStream &q, const ColumnDataList &columns) const;
	void genFieldDeclaration(QTextStream &q, const QString &format, const QString &field_name, Oid column_type) const;

};

#endif // CODEBUILDER_H
