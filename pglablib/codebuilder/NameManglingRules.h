﻿#ifndef NAMEMANGLINGRULES_H
#define NAMEMANGLINGRULES_H

#include <QString>
#include <QRegularExpression>
#include <vector>

/** Defines how a database result fieldname should be converted into a variable
 * name in the target language.
 *
 */
class NameManglingRules {
public:
	enum class CollisionHandling {
		Restrict, ///< An error will be reported and no code generated
		Fqn, ///< Turn into fully qualified name (table_column)
		Number ///< A number will be appended to fields that have the same name
	};

	enum class CaseConversion {
		AsIs,
		Upper,
		Lower
	};

	class ReplaceRule {
	public:
		QRegularExpression pattern;
		QString replace;
		bool nextToUpper = false;
	};
	using ReplaceRules = std::vector<ReplaceRule>;

	ReplaceRules replaceRules;
//		{ {"[ -_]", QRegularExpression::OptimizeOnFirstUsageOption }, "", true }

	//CollisionHandling CollisionHandling = CollisionHandling::Restrict;
	CaseConversion caseConversion = CaseConversion::AsIs; ///< overall case conversion rule
	//CaseConversion caseFirstChar = CaseConversion::AsIs; ///< case of the first char
//	bool camelCase = false; ///< removes underscores and make first char after underscore uppercase

	void apply(const ReplaceRule &rule, QString &in) const;

	QString transform(const QString &input) const;


};

#endif // NAMEMANGLINGRULES_H
