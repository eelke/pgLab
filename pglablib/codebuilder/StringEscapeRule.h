﻿#ifndef STRINGESCAPERULE_H
#define STRINGESCAPERULE_H

#include <QString>
#include <boost/optional.hpp>

enum class ConvertToNumericEscape {
	Utf8, ///< Generates upto four escapes for 4 utf8 chars
	Utf16, ///< Generates upto four escapes for 2 utf16 chars
	Utf32, ///< Generates a single escape for the unicode codepoint
};


enum class NumericEscapeFormat {
	Decimal,
	HexUpper,
	HexLower
};

class CharToNumericConversion {
public:
	ConvertToNumericEscape m_toNumericEscape;
	NumericEscapeFormat m_numericEscapeFormat;
	int m_minNumericDigits, m_maxNumericDigits;
};

/** The basic method of applying a StringEscapeRule is to find all matches
 * in the string for the m_matchRegex.
 *
 * if m_numericConversion is set then the match is taken out of the string and replaced
 * with one or more numerical escapes.
 * if no conversion is needed the match is simple prefixed with m_prefixWidth
 */
class StringEscapeRule {
public:
	/** Regular expression that finds characters in the string that need replacing.
	 * Each character to replace should always generate one match.
	 */
	QString m_matchRegex;
	/** As often a simple escape character like \ or a doubling scheme is used this
	 * field often provides enough flexibility.
	 */
	QString m_prefixWith;
//	std::optional<CharToNumericConversion> m_numericConversion;
};

#endif // STRINGESCAPERULE_H
