﻿#ifndef STRINGLITERALRULES_H
#define STRINGLITERALRULES_H

#include "StringEscapeRule.h"
#include <vector>

class QTextStream;

class StringLiteralRules {
public:
	/** What should be put in front of the string to make it a string literal
	 *
	 * For instance a plain char string in C this would just be a double quote "
	 * But for a C++11 utf8 string literal it would be u8"
	 */
	QString m_stringStart;
	/** Similar to stringStart
	 */
	QString m_stringEnd;

	/** If true the assumption is that the newlines
	 * in the source string are not escaped so no line breaking is done.
	 * However we should still get a multiline string if the source was.
	 *
	 * When false the assumption is that we can split the string using stringEnd,
	 * stringStart and the concatOperator. There is a preference to split on whitespace.
	 */
	bool m_supportsMultiLine;
	QString m_concatOperator;

	std::vector<StringEscapeRule> m_escapeRules;

	void OutputString(QTextStream &stream, QString string_literal);
};


#endif // STRINGLITERALRULES_H
