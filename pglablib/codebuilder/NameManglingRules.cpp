﻿#include "NameManglingRules.h"

void NameManglingRules::apply(const ReplaceRule &rule, QString &in) const
{
	int from = 0;
	int pos;
	QRegularExpressionMatch match;
	while ((pos = in.indexOf(rule.pattern, from, &match)) >= 0) {
		int len = match.capturedLength();
		in.replace(pos, len, rule.replace);
		from = pos + rule.replace.size();
		if (rule.nextToUpper)
			in[from] = in[from].toUpper();
	}
}

QString NameManglingRules::transform(const QString &input) const
{
	QString result;
	if (caseConversion == CaseConversion::Lower)
		result = input.toLower();
	else if (caseConversion == CaseConversion::Upper)
		result = input.toUpper();
	else
		result = input;

	for (auto rule : replaceRules) {
		apply(rule, result);
	}

	return result;
}
