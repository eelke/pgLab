﻿#ifndef LANGUAGECONFIG_H
#define LANGUAGECONFIG_H

#include <QString>
#include "Pgsql_oids.h"
#include "TypeMappings.h"

class NameManglingRules;
class StructureTemplate;
class IndentationConfig;
class ResultLoopTemplate;
/**
 *
 */
class LanguageConfig {
public:
	LanguageConfig();

	QString columnNameToFieldName(const QString& column_name) const;
	TypeMappingResult getTypeName(Oid dbtype) const;

	void setNameManglingRules(std::shared_ptr<const NameManglingRules> name_mangling_rules);
	std::shared_ptr<const TypeMappings> typeMappings() const;
	void setTypeMappings(std::shared_ptr<const TypeMappings> type_mappings);
	std::shared_ptr<const StructureTemplate> structureTemplate() const;
	void setStructureTemplate(std::shared_ptr<const StructureTemplate> structure_template);
	std::shared_ptr<const IndentationConfig> indentationConfig() const;
	void setIndentationConfig(std::shared_ptr<const IndentationConfig> indentation_config);
	std::shared_ptr<const ResultLoopTemplate> resultLoopTemplate() const;
	void setResultLoopTemplate(std::shared_ptr<const ResultLoopTemplate> result_loop_template);
private:
	/** Default template for declaring a variable of the correct type.
	 * exmaple: "{$type} {$varname};"
	 */
	//QString varDeclTemplate;
	std::shared_ptr<const NameManglingRules> m_varNaming;
	std::shared_ptr<const TypeMappings> m_typeMappings;
	std::shared_ptr<const StructureTemplate> m_structureTemplate;
	std::shared_ptr<const IndentationConfig> m_indentationConfig;
	std::shared_ptr<const ResultLoopTemplate> m_resultLoopTemplate;

	enum class VariableStrategy {
		UseLocalVariables,
		DeclareClass
	};
};

#endif // LANGUAGECONFIG_H
