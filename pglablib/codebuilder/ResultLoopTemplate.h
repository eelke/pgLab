﻿#ifndef RESULTLOOPTEMPLATE_H
#define RESULTLOOPTEMPLATE_H

#include <QString>

class ResultLoopTemplate {
public:
	/**
	 * @brief m_loopTemplate
	 *
	 * Excepted template fields
	 * - structname: The typename of the struct/class
	 * - queryliteral: The query
	 * - assignfields: Replaced with the field assignment
	 */
	QString m_loopTemplate;
	/**
	 * @brief m_retrieveValueTemplate
	 *
	 * Excepted template fields
	 * - varname: the name of the field of the struct or normal variable
	 */
	QString m_retrieveValueTemplate;
};

#endif // RESULTLOOPTEMPLATE_H
