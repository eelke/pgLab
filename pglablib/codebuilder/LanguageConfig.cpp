﻿#include "LanguageConfig.h"
#include "NameManglingRules.h"
#include "TypeMappings.h"

LanguageConfig::LanguageConfig() = default;

QString LanguageConfig::columnNameToFieldName(const QString& column_name) const
{
	return m_varNaming->transform(column_name);
}

TypeMappingResult LanguageConfig::getTypeName(Oid dbtype) const
{
	return m_typeMappings->getTypeForOid(dbtype);
}

void LanguageConfig::setNameManglingRules(std::shared_ptr<const NameManglingRules> name_mangling_rules)
{
	m_varNaming = name_mangling_rules;
}

std::shared_ptr<const TypeMappings> LanguageConfig::typeMappings() const
{
	return m_typeMappings;
}

void LanguageConfig::setTypeMappings(std::shared_ptr<const TypeMappings> type_mappings)
{
	m_typeMappings = type_mappings;
}

std::shared_ptr<const StructureTemplate> LanguageConfig::structureTemplate() const
{
	return m_structureTemplate;
}

void LanguageConfig::setStructureTemplate(std::shared_ptr<const StructureTemplate> structure_template)
{
	m_structureTemplate = structure_template;
}

std::shared_ptr<const IndentationConfig> LanguageConfig::indentationConfig() const
{
	return m_indentationConfig;
}

void LanguageConfig::setIndentationConfig(std::shared_ptr<const IndentationConfig> indentation_config)
{
	m_indentationConfig = indentation_config;
}

std::shared_ptr<const ResultLoopTemplate> LanguageConfig::resultLoopTemplate() const
{
	return m_resultLoopTemplate;
}

void LanguageConfig::setResultLoopTemplate(std::shared_ptr<const ResultLoopTemplate> result_loop_template)
{
	m_resultLoopTemplate = result_loop_template;
}
