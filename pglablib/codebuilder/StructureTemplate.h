﻿#ifndef STRUCTURETEMPLATE_H
#define STRUCTURETEMPLATE_H

#include <QString>

class StructureTemplate {
public:

//	QString m_startTemplate; // class /$structname/ {\npublic:\n
//	QString m_endTemplate; // };
	QString m_structTemplate;
	QString m_fieldTemplate; // /$typename/ /$varname/;
	QString m_fieldSeparator; // use when you need something between fields but not after the last field
	int m_fieldIndentation = 1;
};

#endif // STRUCTURETEMPLATE_H
