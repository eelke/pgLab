﻿#ifndef INDENTATIONCONFIG_H
#define INDENTATIONCONFIG_H

#include <QString>

class IndentationConfig {
public:
	IndentationConfig();
	IndentationConfig(int tab_size, int indentation_size, bool use_tabs);

	/** Returns a string with the right amount of tabs and spaces for the
	 * requested indentation level.
	 */
	QString getIndentString(int level) const;

private:
	int m_tabSize = 8; ///< the size of a tab
	int m_indentationSize = 4; ///< Number of positions per level to indent
	bool m_useTabs = true; ///< Use tabs as much as possible instead of spaces when indenting
};


#endif // INDENTATIONCONFIG_H
