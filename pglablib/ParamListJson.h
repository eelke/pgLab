﻿#ifndef PARAMLISTJSON_H
#define PARAMLISTJSON_H

#include "json/json.h"
#include "ParamListModel.h"

Json::Value ParamListToJson(const t_ParamList &list);

#endif // PARAMLISTJSON_H
