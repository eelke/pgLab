﻿#ifndef FORMATTOSTREAM_H
#define FORMATTOSTREAM_H

#include <QString>
#include <functional>

class QTextStream;

/**
 * @brief FormatToStream replaces /%var%/ variables in a string with the values returned by the callback
 * @param stream The stream to which to write the result of the formatting operation
 * @param format The format string containing the vars.
 * @param field_callback A callable which returns the values for var.
 */
void FormatToStream(QTextStream &stream, QString format, std::function<void(QTextStream &, QString)> field_callback);

#endif // FORMATTOSTREAM_H
