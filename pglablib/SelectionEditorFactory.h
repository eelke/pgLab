﻿#ifndef SELECTIONEDITORFACTORY_H
#define SELECTIONEDITORFACTORY_H

#include "AbstractEditorFactory.h"

class AbstractModelFactory;

class SelectionEditorFactory: public AbstractEditorFactory {
	Q_OBJECT
public:
	SelectionEditorFactory(QObject *parent, AbstractModelFactory *model_factory, int key_column, int value_column);

	virtual QWidget *createEditor (QWidget *parent, const QStyleOptionViewItem &option,
								   const QModelIndex &index) const override;
	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

private:
	AbstractModelFactory *m_modelFactory = nullptr;
	int m_keyColumn = 0;
	int m_valueColumn = 0;
};

#endif // SELECTIONEDITORFACTORY_H
