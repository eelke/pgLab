﻿#include "ParamListJson.h"

Json::Value ParamToJson(const Param &param)
{
	Json::Value v;
	v["type"] = param.type.toUtf8().data();
	v["value"] = param.value.toUtf8().data();
	return v;
}

Param ParamFromJson(const Json::Value &json)
{
	Param p;

	return p;
}

Json::Value ParamListToJson(const t_ParamList &list)
{
	Json::Value root;
	for (const auto &param : list) {
		root.append(ParamToJson(param));
	}
	return root;
}

t_ParamList ParamListFromJson(const Json::Value &json)
{
	t_ParamList result;
	if (json.isArray())	{
		result.reserve(json.size());
		for (auto &e : json) {
			result.push_back(ParamFromJson(e));
		}
	}
	return result;
}
