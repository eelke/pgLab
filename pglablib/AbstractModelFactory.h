﻿#ifndef ABSTRACTMODELFACTORY_H
#define ABSTRACTMODELFACTORY_H

#include <QObject>

class QAbstractItemModel;

class AbstractModelFactory: public QObject {
	Q_OBJECT
public:
	using QObject::QObject;

	/// Create instance of a model
	///
	/// \param parent Will be passed to the constructor of the model.
	virtual QAbstractItemModel* createModel(QObject *parent = nullptr) const = 0;
};

#endif // ABSTRACTMODELFACTORY_H
