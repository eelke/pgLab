﻿#include "PgSequenceContainer.h"
#include "Pgsql_Connection.h"
#include "Pgsql_Col.h"
#include "PgDatabaseCatalog.h"
#include "PgNamespaceContainer.h"
#include <iterator>

std::string PgSequenceContainer::getLoadQuery() const
{
	std::string select = "SELECT pg_class.oid, relname, relnamespace, reltype, reloftype, "
		"  relowner, relam, relfilenode, reltablespace, relpages, "
		"  reltuples, reltoastrelid, relisshared, relpersistence, "
		"  relkind, relispopulated, relfrozenxid, relminmxid, "
		"  reloptions, relacl, \n"
		"  has_sequence_privilege(pg_class.oid, 'USAGE') AS priv_usage, \n"
		"  has_sequence_privilege(pg_class.oid, 'SELECT') AS priv_select, \n"
		"  has_sequence_privilege(pg_class.oid, 'UPDATE') AS priv_update \n";
	std::string from =
		"FROM pg_catalog.pg_class \n";

	// Starting with 12 oid are not a special column anymore
	if (lessThenVersion(120000))
		select += ", relhasoids ";
	// Starting with pg10 constanst data of sequences is retrieved from pg_sequence
	// instead of SELECTing the sequence
	if (minimumVersion(100000)) {
		select +=
			"  , seqtypid, seqstart, seqincrement, seqmax, \n"
			"  seqmin, seqcache, seqcycle \n";
		from +=
			"  JOIN pg_catalog.pg_sequence ON (pg_class.oid=seqrelid) \n";
	}

	return select +
		from +
		"WHERE relkind='S'";
}

PgSequence PgSequenceContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);
	Oid	class_oid = col.nextValue();
	QString name  = col.nextValue();
	Oid schema_oid = col.nextValue();

	PgSequence v(m_catalog, class_oid, name, schema_oid);
	Oid owner ;
	AclList acl_list;
	col >> v.type >> v.oftype
		>> owner >> v.am >> v.filenode >> v.tablespace >> v.pages_est
		>> v.tuples_est >> v.toastrelid >> v.isshared >> v.persistence
		>> v.kind >> v.ispopulated >> v.frozenxid >> v.minmxid
		>> v.options >> acl_list >> v.priv_usage >> v.priv_select >> v.priv_update;
	if (lessThenVersion(120000))
		col	>> v.hasoids;
	// Read pg_sequence fields
	if (minimumVersion(100000))
		col	>> v.typid >> v.start >> v.increment >> v.max >> v.min >> v.cache >> v.cycled;

	v.setOwnerOid(owner);
	v.setAcls(std::move(acl_list));

	return v;
}

void PgSequenceContainer::loadAll(Pgsql::Connection &conn)
{
	IPgContainer::loadAll(conn);

	for (auto && seq : m_container) {
		if (seq.priv_select) {
			auto fqn = seq.fullyQualifiedQuotedObjectName().toUtf8();
			// SELECTs on a sequence return less fields starting with pg10
			// the missing fields are now in pg_sequence
			std::string q("SELECT last_value, log_cnt, is_called");
			if (lessThenVersion(100000)) {
				q += ", start_value, increment_by, max_value, \n"
					 "    min_value, cache_value, is_cycled";
			}
			q += "\nFROM " + std::string(fqn.data(), static_cast<size_t>(fqn.count()));
			auto && res = conn.query(q.c_str());
			if (res.rows() == 1) {
				Pgsql::Row row(res, 0);
				Pgsql::Col col(row);
				col >> seq.last >> seq.log >> seq.called;
				if (lessThenVersion(100000)) {
					col >> seq.start >> seq.increment >> seq.max >> seq.min >> seq.cache >> seq.cycled;
					seq.typid = Pgsql::int8_oid; // bigint used to be the only choice
				}
			}
		}
	}
}
