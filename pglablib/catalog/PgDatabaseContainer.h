﻿#ifndef PGDATABASECONTAINER_H
#define PGDATABASECONTAINER_H

#include "PgContainer.h"
#include "PgDatabase.h"

namespace Pgsql {

	class Result;

}


class PgDatabaseContainer: public PgContainer<PgDatabase> {
public:
	using PgContainer<PgDatabase>::PgContainer;

	virtual std::string getLoadQuery() const override;
protected:
	PgDatabase loadElem(const Pgsql::Row &row) override;
private:
};


#endif // PGDATABASECONTAINER_H
