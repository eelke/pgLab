﻿#ifndef PGNAMESPACECONTAINER_H
#define PGNAMESPACECONTAINER_H

#include "PgNamespace.h"
#include "PgContainer.h"

namespace Pgsql {

	class Result;

}

class PgNamespaceContainer: public PgContainer<PgNamespace> {
public:
	using PgContainer<PgNamespace>::PgContainer;

	virtual std::string getLoadQuery() const override;
protected:
	virtual PgNamespace loadElem(const Pgsql::Row &row) override;
private:
};


#endif // PGNAMESPACECONTAINER_H
