﻿#include "PgType.h"
#include "Pgsql_Connection.h"

void operator<<(TypCategory &s, const Pgsql::Value &v)
{
	//s = static_cast<T>(v);
	const char *c = v.c_str();
	switch (*c) {
	case 'A':
		s = TypCategory::Array;
		break;
	case 'B':
		s = TypCategory::Boolean;
		break;
    case 'C':
        s = TypCategory::Composite;
        break;
    case 'D':
		s = TypCategory::DateTime;
		break;
	case 'E':
		s = TypCategory::Enum;
		break;
	case 'G':
		s = TypCategory::Geometric;
		break;
	case 'I':
		s = TypCategory::NetworkAddress;
		break;
	case 'N':
		s = TypCategory::Numeric;
		break;
	case 'P':
		s = TypCategory::Pseudo;
		break;
	case 'R':
		s = TypCategory::Range;
		break;
	case 'S':
		s = TypCategory::String;
		break;
	case 'T':
		s = TypCategory::Timespan;
		break;
	case 'U':
		s = TypCategory::UserDefined;
		break;
	case 'V':
		s = TypCategory::BitString;
		break;
	case 'X':
		s = TypCategory::Unknown;
		break;
	}
}

QString PgType::typeName() const
{
	return "TYPE";
}
