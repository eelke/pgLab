﻿#ifndef PGAUTHIDCONTAINER_H
#define PGAUTHIDCONTAINER_H

#include <vector>
#include "PgContainer.h"
#include "PgAuthId.h"

namespace Pgsql {

	class Result;

}

class PgAuthIdContainer: public PgContainer<PgAuthId> {
public:
	using PgContainer<PgAuthId>::PgContainer;

	virtual std::string getLoadQuery() const override;
protected:
	PgAuthId loadElem(const Pgsql::Row &row) override;
private:
};


#endif // PGAUTHIDCONTAINER_H
