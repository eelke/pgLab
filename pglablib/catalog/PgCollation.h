﻿#ifndef PGCOLLATION_H
#define PGCOLLATION_H

#include "PgNamespaceObject.h"
#include <QString>
#include <libpq-fe.h>
#include "Pgsql_Value.h"
//#include <vector>

class PgCollation: public PgNamespaceObject {
public:
	using PgNamespaceObject::PgNamespaceObject;

//	Oid oid; // oid
//    QString collname; // name
//    Oid collnamespace; // oid
//	Oid collowner; // oid
	int32_t collencoding; // integer
	QString collcollate; // name
	QString collctype; // name

	QString typeName() const override;
};

#endif // PGCOLLATION_H
