﻿#ifndef PGCOLLATIONCONTAINER_H
#define PGCOLLATIONCONTAINER_H

#include "PgContainer.h"
#include "PgCollation.h"

namespace Pgsql {

	class Result;

}


class PgCollationContainer: public PgContainer<PgCollation> {
public:
	using PgContainer<PgCollation>::PgContainer;

	virtual std::string getLoadQuery() const override;
protected:
	PgCollation loadElem(const Pgsql::Row &row) override;
private:
};


#endif // PGCOLLATIONCONTAINER_H
