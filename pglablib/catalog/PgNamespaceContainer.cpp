﻿#include "PgNamespaceContainer.h"
#include "Pgsql_Col.h"
#include "Pgsql_Result.h"
#include "Pgsql_Value.h"

std::string PgNamespaceContainer::getLoadQuery() const
{
	return "SELECT oid, nspname, nspowner, nspacl FROM pg_catalog.pg_namespace";
}

PgNamespace PgNamespaceContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);

	Oid oid = col.nextValue();
	QString name = col.nextValue();
	PgNamespace v(m_catalog, oid, name);
	Oid owner;
	AclList acl_list;
	col >> owner >> acl_list;
	v.setOwnerOid(owner);
	v.setAcls(std::move(acl_list));

	return v;
}

