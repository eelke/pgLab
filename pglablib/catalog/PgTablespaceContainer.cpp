﻿#include "PgTablespaceContainer.h"
#include "Pgsql_Connection.h"
#include "Pgsql_Col.h"
#include "Pgsql_declare.h"
#include "PgDatabaseCatalog.h"
#include <iterator>

std::string PgTablespaceContainer::getLoadQuery() const
{
	return
R"__SQL__(
SELECT oid, spcname, spcowner, spcacl, spcoptions
FROM pg_tablespace
)__SQL__";
}

PgTablespace PgTablespaceContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);
	Oid oid = col.nextValue();
	QString name = col.nextValue();
	Oid owner = col.nextValue();
	PgTablespace v(m_catalog, oid, name);
	v.setOwnerOid(owner);

	AclList acl_list;
	col >> acl_list;
	v.setAcls(std::move(acl_list));

	col >> v.options;
	return v;
}
