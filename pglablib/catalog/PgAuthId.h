﻿#ifndef PGAUTHID_H
#define PGAUTHID_H

#include "PgServerObject.h"
#include <libpq-fe.h>
#include <QString>
#include <QDateTime>

/**
 * @brief The PgAuthId class
 *
 * An AuthId is a database role
 */
class PgAuthId: public PgServerObject {
public:

	bool super;
	bool inherit;
	bool createRole;
	bool createDB;
	bool canlogin;
	bool replication;
	bool bypassRls;
	int connLimit;
	QDateTime validUntil;

	using PgServerObject::PgServerObject;
	QString typeName() const override;

};

#endif // PGAUTHID_H
