﻿#ifndef PGSERVEROBJECT_H
#define PGSERVEROBJECT_H

#include <QString>
#include "PgObject.h"
#include "PgAcl.h"
#include <boost/optional.hpp>

class PgAuthId;

/// Base object for objects that belong to a server
class PgServerObject: public PgObject {
public:
	using PgObject::PgObject;

	void setOwnerOid(Oid oid);
	Oid ownerOid() const;
	bool hasOwner() const;
	QString ownerName() const;
	const PgAuthId* owner() const;
	QString alterOwnerSql(const QString& ident) const;

    QString description;

	/**
	 * @brief setAcls
	 * @param acls Important: pass empty optional when acl IS NULL, pass empty list for empty array
	 */
	void setAcls(boost::optional<AclList> acls);
	QString aclString() const;
	/**
	 * @brief grantSql
	 * @return
	 */
	QString grantSql() const;

	/**
	 * @brief Returns a string containing all the possible privileges for this type of object.
	 *
	 * The default implementation returns an empty string.
	 *
	 * @return A string containing all posible privileges for this type of object.
	 */
	virtual QString aclAllPattern() const;

	virtual QString dropSql() const;
	virtual QString createSql() const;
    QString commentSql() const;

protected:
    virtual QString ddlTypeName() const;

private:
	Oid m_ownerOid = InvalidOid;
    const PgAuthId * m_owner = nullptr;
	boost::optional<AclList> m_acls;
};

#endif // PGSERVEROBJECT_H
