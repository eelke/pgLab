﻿#include "PgKeywordList.h"
#include <unordered_set>

namespace {

	using KeywordHT = std::unordered_set<Keyword>;

    #define PG_KEYWORD(a,b,c,d) Keyword{a,c},
	const KeywordHT _ScanKeywords = {
	//const Keyword _ScanKeywords[] = {
    #include "kwlist.h"

	};

}

const Keyword* getPgsqlKeyword(QString s)
{
	const Keyword *result	= nullptr;
	auto fr = _ScanKeywords.find(Keyword(s.toLower(), 0));
	if (fr != _ScanKeywords.end())
		result = &*fr;
	return result;
}

