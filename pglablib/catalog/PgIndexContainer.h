﻿#ifndef PGINDEXCONTAINER_H
#define PGINDEXCONTAINER_H

#include "PgContainer.h"
#include "PgIndex.h"
#include "Pgsql_declare.h"
#include <vector>

class PgIndexContainer : public PgContainer<PgIndex> {
public:
	using PgContainer<PgIndex>::PgContainer;

	virtual std::string getLoadQuery() const override;
	std::vector<PgIndex> getIndexesForTable(Oid table_oid) const;
protected:
	virtual PgIndex loadElem(const Pgsql::Row &row) override;
};

#endif // PGINDEXCONTAINER_H
