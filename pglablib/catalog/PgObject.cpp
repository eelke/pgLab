﻿#include "PgObject.h"
#include "SqlFormattingUtils.h"

PgObject::PgObject(PgDatabaseCatalog& cat, Oid oid, const QString &name)
	: m_catalog(&cat)
	, m_oid(oid)
	, m_name(name)
{}

PgObject::~PgObject()
{}

Oid PgObject::oid() const
{
	return m_oid;
}

const QString& PgObject::objectName() const
{
	return m_name;
}

void PgObject::setObjectName(const QString &name)
{
    m_name = name;
}

QString PgObject::quotedObjectName() const
{
    return quoteIdent(objectName());
}

QString PgObject::fullyQualifiedQuotedObjectName() const
{
	return quotedObjectName();
}

const PgDatabaseCatalog& PgObject::catalog() const
{
    return *m_catalog;
}


