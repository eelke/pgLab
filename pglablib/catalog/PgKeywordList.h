﻿#ifndef PGKEYWORDLIST_H
#define PGKEYWORDLIST_H

#include <QString>
#include <unordered_set>
#include <functional>
#include "util.h"

#define UNRESERVED_KEYWORD		0
#define COL_NAME_KEYWORD		1
#define TYPE_FUNC_NAME_KEYWORD	2
#define RESERVED_KEYWORD		3



using t_SymbolSet = std::unordered_set<QString>;


class Keyword {
public:
	Keyword(QString kw, int16_t cat)
		: m_keyword(kw), m_category(cat)
	{}

	const QString& getKeyword() const { return m_keyword; }
	int16_t getCategory() const { return m_category; }

	//bool operator<(const QString &s) const { return m_keyword < s; }
	bool operator<(const Keyword &kw) const { return m_keyword < kw.m_keyword; }
	bool operator==(const Keyword &kw) const { return m_keyword == kw.m_keyword; }
private:
	QString m_keyword;
	int16_t m_category;
};

namespace std {

  template <>
  struct hash<Keyword>
  {
	std::size_t operator()(const Keyword& s) const
	{
		return qHash(s.getKeyword());
	}
  };

}

const Keyword* getPgsqlKeyword(QString s);


#endif // PGKEYWORDLIST_H
