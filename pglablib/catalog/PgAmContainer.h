﻿#ifndef PGAMCONTAINER_H
#define PGAMCONTAINER_H

#include "PgContainer.h"
#include "PgAm.h"
#include "Pgsql_declare.h"
#include <vector>

class PgAmContainer : public PgContainer<PgAm> {
public:
	using PgContainer<PgAm>::PgContainer;

	virtual std::string getLoadQuery() const override;
protected:
	virtual PgAm loadElem(const Pgsql::Row &row) override;
};

#endif // PGAMCONTAINER_H
