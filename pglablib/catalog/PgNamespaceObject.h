﻿#ifndef PGSCHEMAOBJECT_H
#define PGSCHEMAOBJECT_H

#include <QString>
#include "PgDatabaseObject.h"
#include <libpq-fe.h>

class PgNamespace;

/// Base class for database objects that are part of a specific schema
class PgNamespaceObject: public PgDatabaseObject {
public:

	PgNamespaceObject(PgDatabaseCatalog& cat, Oid oid, const QString &name, Oid schema_oid);

	Oid nsOid() const;
//	void setSchemaOid(Oid oid);
	QString nsName() const;
	QString quotedNsName() const;
	/// Returns the schema name and object name with proper quotes
	QString fullyQualifiedQuotedObjectName() const override;

	const PgNamespace& ns() const;
private:
	Oid m_schemaOid = InvalidOid;
};

#endif // PGSCHEMAOBJECT_H
