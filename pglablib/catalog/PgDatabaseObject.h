﻿#ifndef PGDATABASEOBJECT_H
#define PGDATABASEOBJECT_H

#include "PgServerObject.h"

/// Base class for objects that are part of a database
class PgDatabaseObject: public PgServerObject {
public:
	using PgServerObject::PgServerObject;

};

#endif // PGDATABASEOBJECT_H
