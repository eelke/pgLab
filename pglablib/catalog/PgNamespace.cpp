﻿#include "PgNamespace.h"

bool PgNamespace::isSystemCatalog() const
{
	auto&& n = objectName();
	return n.startsWith("pg_")
		|| n == "information_schema";
}

QString PgNamespace::typeName() const
{
	return "SCHEMA";
}
