﻿#include "PgDatabaseContainer.h"
#include "Pgsql_Col.h"

std::string PgDatabaseContainer::getLoadQuery() const
{
    return
        "SELECT pg_database.oid, datname, datdba, encoding, pg_encoding_to_char(encoding), datcollate,\n"
        "  datctype, datistemplate, datallowconn, datconnlimit, dattablespace, \n"
        "  d.description, datacl\n"
        "FROM pg_database\n"
        "  LEFT JOIN pg_catalog.pg_shdescription AS d ON (objoid=pg_database.oid)";
}

PgDatabase PgDatabaseContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);

	Oid oid = col.nextValue();
	QString name = col.nextValue();
	PgDatabase v(m_catalog, oid, name);
    Oid owner;
    col >> owner >> v.encoding >> v.encodingString >> v.collate >> v.ctype >> v.isTemplate
            >> v.allowConn >> v.connLimit >> v.tablespace >> v.description;
    v.setOwnerOid(owner);
	AclList acl_list;
	col >> acl_list;
	v.setAcls(std::move(acl_list));

	return v;
}

