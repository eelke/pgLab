﻿#ifndef PGATTRIBUTECONTAINER_H
#define PGATTRIBUTECONTAINER_H

#include "PgContainer.h"
#include "PgAttribute.h"
#include "Pgsql_declare.h"
#include <vector>

class PgAttributeContainer : public PgContainer<PgAttribute, PgAttribute::Key> {
public:
	using PgContainer<PgAttribute, PgAttribute::Key>::PgContainer;

	virtual std::string getLoadQuery() const override;

	std::vector<PgAttribute> getColumnsForRelation(Oid oid) const;
protected:
	PgAttribute loadElem(const Pgsql::Row &row) override;
};

#endif // PGATTRIBUTECONTAINER_H
