﻿#include "PgLanguageContainer.h"
#include "Pgsql_Connection.h"
#include "Pgsql_Col.h"

std::string PgLanguageContainer::getLoadQuery() const
{
	return
		"SELECT oid, lanname, lanowner, lanispl, lanpltrusted, lanplcallfoid, laninline, lanvalidator, lanacl \n"
		"  FROM pg_language";
}

PgLanguage PgLanguageContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);
	Oid lan_oid = col.nextValue();
	QString name = col.nextValue();
	PgLanguage v(m_catalog, lan_oid, name);
	Oid owner;
	AclList acl_list;
	col >> owner >> v.ispl >> v.pltrusted >> v.plcallfoid >> v.inline_ >> v.validator >> acl_list;

	v.setOwnerOid(owner);
	v.setAcls(std::move(acl_list));

	return v;
}
