﻿#include "PgDatabase.h"
#include "PgDatabaseCatalog.h"
#include "SqlFormattingUtils.h"
#include <QStringBuilder>

QString PgDatabase::typeName() const
{
    return "DATABASE";
}

QString PgDatabase::aclAllPattern() const
{
    // Create
    // Connect
    // Temporary
    return "CcT";
}

QString PgDatabase::dropSql() const
{
    return "DROP DATABASE " % quotedObjectName() % ";";
}

QString PgDatabase::createSql() const
{
    QString s = "CREATE DATABASE " % quotedObjectName();

    if (hasOwner())
        s += "\n  OWNER " % quoteIdent(ownerName());
    if (!dbTemplate.isEmpty())
        s += "\n  TEMPLATE " % escapeLiteral(dbTemplate);
    if (!encodingString.isEmpty())
        s += "\n  ENCODING " % escapeLiteral(encodingString);
    if (!collate.isEmpty())
        s += "\n  LC_COLLATE " % escapeLiteral(collate);
    if (!ctype.isEmpty())
        s += "\n  LC_TYPE " % escapeLiteral(ctype);
    if (tablespace != InvalidOid)
    {
        auto ns = getTablespaceDisplayString(catalog(), tablespace);
        if (ns != "pg_default")
            s += "\n  TABLESPACE " % quoteIdent(ns);
    }
    if (!allowConn)
        s += "\n  ALLOW_CONNECTIONS FALSE";
    if (connLimit >= 0)
        s += "\n  CONNECTION LIMIT " % QString::number(connLimit);
    if (isTemplate)
        s += "\n  IS_TEMPLATE TRUE";
    s += ";";
    return s;
}
