﻿#ifndef PGCONSTRAINTCONTAINER_H
#define PGCONSTRAINTCONTAINER_H

#include "PgContainer.h"
#include "PgConstraint.h"
#include "Pgsql_declare.h"
#include <vector>
#include <optional>

class PgConstraintContainer : public PgContainer<PgConstraint> {
public:
	using PgContainer<PgConstraint>::PgContainer;

	virtual std::string getLoadQuery() const override;
	//std::vector<PgConstraint> getIndexesForTable(Oid table_oid) const;
	std::vector<PgConstraint> getFKeyForTableColumn(Oid relid, int16_t attnum) const;

	std::vector<PgConstraint> getConstraintsForRelation(Oid relid) const;
	std::optional<PgConstraint> getPrimaryForRelation(Oid relid) const;
	std::vector<PgConstraint> getReferencedForRelation(Oid relid) const;
    std::vector<PgConstraint> getSupportedByIndex(Oid indexrelid) const;
protected:
	virtual PgConstraint loadElem(const Pgsql::Row &row) override;
};


#endif // PGCONSTRAINTCONTAINER_H
