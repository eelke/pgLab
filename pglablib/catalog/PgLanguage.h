﻿#ifndef PGLANGUAGE_H
#define PGLANGUAGE_H

#include "PgDatabaseObject.h"
#include <QString>
#include <libpq-fe.h>
//#include "Pgsql_Value.h"

class PgLanguage: public PgDatabaseObject {
public:

//	Oid     oid;
//	QString name;
//	Oid     owner;
	bool    ispl; // true "user installable language" language like plpgsql perl en pythong, false for internal, c and sql
	bool    pltrusted;
	Oid     plcallfoid;
	Oid     inline_;
	Oid     validator;

	using PgDatabaseObject::PgDatabaseObject;

	QString createSql() const;
	QString dropSql() const;

	bool isC() const { return objectName() == "c"; }
	bool isInternal() const { return objectName() == "internal"; }
	bool isPlpgsql() const { return objectName() == "plpgsql"; }
	bool isSql() const { return objectName() == "sql"; }

	QString typeName() const override;
};

#endif // PGLANGUAGE_H
