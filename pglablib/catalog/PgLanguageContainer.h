﻿#ifndef PGLANGUAGECONTAINER_H
#define PGLANGUAGECONTAINER_H

#include "PgLanguage.h"
#include "PgContainer.h"

namespace Pgsql {

	class Result;

}

class PgLanguageContainer: public PgContainer<PgLanguage> {
public:
	using PgContainer<PgLanguage>::PgContainer;

	virtual std::string getLoadQuery() const override;

protected:
	virtual PgLanguage loadElem(const Pgsql::Row &row) override;
private:
};


#endif // PGLANGUAGECONTAINER_H
