﻿#ifndef PGNAMESPACE_H
#define PGNAMESPACE_H

#include "PgDatabaseObject.h"
#include <QString>
#include <libpq-fe.h>


/// Object representing a namespace within a database
class PgNamespace: public PgDatabaseObject {
public:
	using PgDatabaseObject::PgDatabaseObject;

	bool isSystemCatalog() const;
	QString typeName() const override;
};

#endif // PGNAMESPACE_H
