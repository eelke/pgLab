﻿#ifndef PGAM_H
#define PGAM_H

#include "Pgsql_declare.h"
#include <QString>

/**
 * @brief The PgAm class
 *
 * AM = Access Method, for indexes
 */
class PgAm {
public:
	Oid oid;
	QString name;

	PgAm();

	bool operator==(Oid rhs) const { return oid == rhs; }
	bool operator<(Oid rhs) const { return oid < rhs; }
	bool operator<(const PgAm &rhs) const { return oid < rhs.oid; }
};

#endif // PGAM_H
