﻿#ifndef PGACL_H
#define PGACL_H

#include "Pgsql_Value.h"
#include <QString>
#include <vector>

enum class Privilege {
	Select,
	Update,
	Insert,
	Delete,
	Truncate,
	References,
	Trigger,
	Execute,
	Usage,
	Create,
	Connect,
	Temporary
};

char privilegeToChar(Privilege p);

enum class PrivValue {
	No, Yes, YesWithGrant
};

class PgAcl {
public:
	PgAcl() = default;
	explicit PgAcl(const QString &acl);
	void setFromString(const QString &acl);

	PrivValue privilege(Privilege priv) const;
	PrivValue privilege(char c) const;
	const QString& grantee() const { return m_grantee; }
	const QString& grantor() const { return m_grantor; }
	QString singleString() const;

	QString sql(const QString &all_pattern, const QString &grant_on_object, const QString &column) const;
	// Returns true when the list of privileges is empty
	bool empty() const;
private:
	QString m_grantee;
	QString m_grantor;
	QString m_privileges;

};

using AclList = std::vector<PgAcl>;

void operator<<(PgAcl &acl, const Pgsql::Value &v);

namespace Pgsql {
	template <>
	PgAcl StringToArrayElem<PgAcl>(std::string_view sv);
}


#endif // PGACL_H
