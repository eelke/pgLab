﻿#include "PgInheritsContainer.h"
#include "Pgsql_Col.h"
#include "PgDatabaseCatalog.h"
#include <algorithm>

std::vector<Oid> PgInheritsContainer::getParentsOf(Oid oid) const
{
	std::vector<Oid> result;
	auto&& iter = std::lower_bound(m_container.begin(), m_container.end(), PgInherits::Key{ oid, 1 });
	for (;iter != m_container.end() && iter->relid == oid; ++iter)
		result.push_back(iter->parent);
	return result;
}

std::string PgInheritsContainer::getLoadQuery() const
{
	return
		"SELECT inhrelid, inhparent, inhseqno \n"
		"  FROM pg_inherits";
}


PgInherits PgInheritsContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);
	PgInherits v;
	col >> v.relid >> v.parent >> v.seqno;
	return v;
}
