﻿#include "PgNamespaceObject.h"
#include "PgDatabaseCatalog.h"
#include "PgNamespace.h"
#include "PgNamespaceContainer.h"
#include "SqlFormattingUtils.h"

PgNamespaceObject::PgNamespaceObject(PgDatabaseCatalog& cat, Oid oid, const QString &name, Oid schema_oid)
	: PgDatabaseObject(cat, oid, name)
	, m_schemaOid(schema_oid)
{}

Oid PgNamespaceObject::nsOid() const
{
	return m_schemaOid;
}

//void PgSchemaObject::setSchemaOid(Oid oid)
//{
//	m_schemaOid = oid;
//}

QString PgNamespaceObject::nsName() const
{
	return ns().objectName();
}

QString PgNamespaceObject::quotedNsName() const
{
	return quoteIdent(nsName());
}

QString PgNamespaceObject::fullyQualifiedQuotedObjectName() const
{
	return quotedNsName() + "." + quotedObjectName();
}

const PgNamespace& PgNamespaceObject::ns() const
{
	return *catalog().namespaces()->getByKey(m_schemaOid);
}
