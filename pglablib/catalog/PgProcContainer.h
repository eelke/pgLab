﻿#ifndef PGPROCCONTAINER_H
#define PGPROCCONTAINER_H

#include "PgContainer.h"
#include "PgProc.h"
#include <vector>

namespace Pgsql {

	class Result;

}

class PgProcContainer: public PgContainer<PgProc> {
public:
	using PgContainer<PgProc>::PgContainer;

	virtual std::string getLoadQuery() const override;

	//std::vector<PgProc> getTriggersForRelation(Oid cls) const;
protected:
	PgProc loadElem(const Pgsql::Row &row) override;
private:
};

#endif // PGPROCCONTAINER_H
