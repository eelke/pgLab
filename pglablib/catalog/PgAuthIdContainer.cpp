﻿#include "PgAuthIdContainer.h"
#include "Pgsql_Col.h"

std::string PgAuthIdContainer::getLoadQuery() const
{
	std::string result =
		"SELECT oid, rolname, rolsuper, rolinherit, rolcreaterole, rolcreatedb, "
		"  rolcanlogin, rolreplication, rolconnlimit, rolvaliduntil";
	if (minimumVersion(90500))
		result += ", rolbypassrls";

	result += "\n"
		   "FROM pg_roles";
	return result;
}

PgAuthId PgAuthIdContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);
	Oid oid = col.nextValue();
	QString name = col.nextValue();
	PgAuthId v(m_catalog, oid, name);
	col >> v.super >> v.inherit >> v.createRole >> v.createDB
			>> v.canlogin >> v.replication >> v.connLimit >> v.validUntil;
	if (minimumVersion(90500))
		col >> v.bypassRls;

	return v;
}
