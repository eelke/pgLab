﻿#include "PgCollationContainer.h"
#include "Pgsql_Connection.h"
#include "Pgsql_Col.h"
#include "PgDatabaseCatalog.h"
#include <iterator>

std::string PgCollationContainer::getLoadQuery() const
{
	return "SELECT oid, collname, collnamespace, collowner, collencoding, \n"
		   "   collcollate, collctype \n"
		   "FROM pg_collation";
}

PgCollation PgCollationContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);
	Oid	class_oid = col.nextValue();
	QString name  = col.nextValue();
	Oid ns_oid = col.nextValue();

	PgCollation v(m_catalog, class_oid, name, ns_oid);
	Oid owner ;

	col >> owner >> v.collencoding >> v.collcollate >> v.collctype;
	v.setOwnerOid(owner);

	return v;
}
