﻿#ifndef PGOBJECT_H
#define PGOBJECT_H

#include <libpq-fe.h>
#include <QString>

class PgDatabaseCatalog;

class PgObject {
public:
	explicit PgObject(PgDatabaseCatalog& cat, Oid oid, const QString &name);
	virtual ~PgObject();

	Oid oid() const;
	const QString& objectName() const;
    void setObjectName(const QString &name);
	/// Default implementation uses objectName and add quotes when needed.
	virtual QString quotedObjectName() const;
	/// By default same as quotedObjectName however for objects that reside in namespaces
	/// the namespace name is added to the name.
	virtual QString fullyQualifiedQuotedObjectName() const;
	virtual QString typeName() const = 0;

	bool operator==(Oid _oid) const { return m_oid == _oid; }
	bool operator==(const QString &n) const { return m_name == n; }
	bool operator<(Oid _oid) const { return m_oid < _oid; }
	bool operator<(const PgObject &rhs) const { return m_oid < rhs.m_oid; }
protected:
	const PgDatabaseCatalog& catalog() const;

private:
    PgDatabaseCatalog* m_catalog;
    Oid m_oid;
	QString m_name;
};

#endif // PGOBJECT_H
