﻿#ifndef PGINHERITSCONTAINER_H
#define PGINHERITSCONTAINER_H


#include "PgContainer.h"
#include "PgInherits.h"

class IFindParents
{
public:
    virtual std::vector<Oid> getParentsOf(Oid oid) const = 0;
};

class PgInheritsContainer
    : public PgContainer<PgInherits, PgInherits::Key>
    , public IFindParents
{
public:
	using PgContainer<PgInherits, PgInherits::Key>::PgContainer;

	virtual std::string getLoadQuery() const override;

	/// Returns the parents in the correct order
    std::vector<Oid> getParentsOf(Oid oid) const override;
protected:
	PgInherits loadElem(const Pgsql::Row &row) override;
};

#endif // PGINHERITSCONTAINER_H
