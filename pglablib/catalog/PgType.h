﻿#ifndef PGTYPE_H
#define PGTYPE_H

#include "PgNamespaceObject.h"
#include <QString>
#include <libpq-fe.h>
#include "Pgsql_Value.h"

enum class TypCategory {
	Array,
	Boolean,
	Composite,
	DateTime,
	Enum,
	Geometric,
	NetworkAddress,
	Numeric,
	Pseudo,
	Range,
	String,
	Timespan,
	UserDefined,
	BitString,
	Unknown
};

void operator<<(TypCategory &s, const Pgsql::Value &v);


class PgType: public PgNamespaceObject {
public:
	QString typname; //"name";"NO"
	short len = -1;//"smallint";"NO"
	bool byval = false;//"boolean";"NO"
	QString type;//""char"";"NO"
	TypCategory category;//""char"";"NO"
	bool ispreferred = false;//"boolean";"NO"
	bool isdefined = false;//"boolean";"NO"
	QString delim;//""char"";"NO"
	Oid relid = InvalidOid;//"oid";"NO"
	Oid elem = InvalidOid;//"oid";"NO"
	Oid array = InvalidOid;//"oid";"NO"
	QString input;//regproc";"NO"
	QString output;//"regproc";"NO"
	QString receive;//"regproc";"NO"
	QString send;//"regproc";"NO"
	QString modin;//"regproc";"NO"
	QString modout;//"regproc";"NO"
	QString analyze;//"regproc";"NO"
	QString align;//""char"";"NO"
	QString storage;//""char"";"NO"
	bool notnull = false;//"boolean";"NO"
	Oid basetype = InvalidOid;//"oid";"NO"
	int typmod = -1;//"integer";"NO"
	int ndims = 0;//"integer";"NO"
	Oid collation = InvalidOid;//"oid";"NO"
	QString defaultbin;//"pg_node_tree";"YES"
	QString typdefault;//"text";"YES"

	using PgNamespaceObject::PgNamespaceObject;

	QString typeName() const override;
};

#endif // PGTYPE_H
