﻿#ifndef PGTRIGGER_H
#define PGTRIGGER_H

#include "PgNamespaceObject.h"
#include "Pgsql_Value.h"
#include <QString>
#include <libpq-fe.h>

class PgDatabaseCatalog;

class PgTrigger: public PgNamespaceObject {
public:
//	Oid oid = InvalidOid;
	Oid relid;
//	QString name;
	Oid foid;
	int16_t type;
	char enabled;
	bool isinternal;
	Oid constrrelid;
	Oid constrindid;
	Oid constraint;
	bool deferrable;
	bool initdeferred;
	int16_t nargs;
	QString attr;
	QString args;
	QString whenclause;
	QString oldtable; // >= 10.0
	QString newtable; // >= 10.0

	using PgNamespaceObject::PgNamespaceObject;

//	virtual QString objectName() const override;


//	bool operator==(Oid _oid) const { return oid == _oid; }
//	bool operator==(const QString &n) const { return name == n; }
//	bool operator<(Oid _oid) const { return oid < _oid; }
//	bool operator<(const PgTrigger &rhs) const { return oid < rhs.oid; }

	static constexpr int TriggerTypeRow      = (1 << 0);
	static constexpr int TriggerTypeBefore   = (1 << 1);
	static constexpr int TriggerTypeInsert   = (1 << 2);
	static constexpr int TriggerTypeDelete   = (1 << 3);
	static constexpr int TriggerTypeUpdate   = (1 << 4);
	static constexpr int TriggerTypeTruncate = (1 << 5);
	static constexpr int TriggerTypeInstead  = (1 << 6);

	QString dropSql() const override;
	QString createSql() const override;
	bool isRow() const { return type & TriggerTypeRow; }
	bool isBefore() const { return type & TriggerTypeBefore; }
	QString typeFireWhen() const;
	QString eventAbbr() const;
	QString event() const;
	QString forEach() const;
	QString procedure() const;

	//wxString pgTrigger::GetForEach() const
	//{
	//	return (triggerType & TRIGGER_TYPE_ROW) ? wxT("ROW") : wxT("STATEMENT");
	//}

	QString arguments() const;

	QString typeName() const override;
private:
	mutable QString m_dropSql; // cache
	mutable QString m_createSql; // cache
};

#endif // PGTRIGGER_H
