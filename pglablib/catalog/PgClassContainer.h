﻿#ifndef PGCLASSCONTAINER_H
#define PGCLASSCONTAINER_H

#include "PgContainer.h"
#include "PgClass.h"

namespace Pgsql {

	class Result;

}


class PgClassContainer: public PgContainer<PgClass> {
public:
	using PgContainer<PgClass>::PgContainer;

	virtual std::string getLoadQuery() const override;
protected:
	PgClass loadElem(const Pgsql::Row &row) override;
private:
};

#endif // PGCLASSCONTAINER_H
