﻿#ifndef PGTABLESPACECONTAINER_H
#define PGTABLESPACECONTAINER_H

#include "PgContainer.h"
#include "PgTablespace.h"

namespace Pgsql {

	class Result;

}


class PgTablespaceContainer: public PgContainer<PgTablespace> {
public:
	using PgContainer<PgTablespace>::PgContainer;

	virtual std::string getLoadQuery() const override;
protected:
	PgTablespace loadElem(const Pgsql::Row &row) override;
private:
};

#endif // PGTABLESPACECONTAINER_H
