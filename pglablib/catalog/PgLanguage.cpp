﻿#include "PgLanguage.h"

QString PgLanguage::createSql() const
{
	throw std::exception("PgLanguage::createSql() not implemented");
}

QString PgLanguage::dropSql() const
{
	throw std::exception("PgLanguage::dropSql() not implemented");
}

QString PgLanguage::typeName() const
{
	return "LANGUAGE";
}
