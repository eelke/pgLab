﻿#ifndef PGTABLESPACE_H
#define PGTABLESPACE_H

#include "PgServerObject.h"
#include <QString>
#include <libpq-fe.h>
#include <vector>

class PgTablespace: public PgServerObject {
public:
	std::vector<QString> options;

	using PgServerObject::PgServerObject;
	QString typeName() const override;
};

#endif // PGTABLESPACE_H
