﻿#ifndef PGTRIGGERCONTAINER_H
#define PGTRIGGERCONTAINER_H

#include "PgContainer.h"
#include "PgTrigger.h"
#include <vector>

namespace Pgsql {

	class Result;

}


class PgTriggerContainer: public PgContainer<PgTrigger> {
public:
	using PgContainer<PgTrigger>::PgContainer;

	virtual std::string getLoadQuery() const override;

	std::vector<PgTrigger> getTriggersForRelation(Oid cls) const;
protected:
	PgTrigger loadElem(const Pgsql::Row &row) override;
private:
};


#endif // PGTRIGGERCONTAINER_H
