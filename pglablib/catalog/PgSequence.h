﻿#ifndef PGSEQUENCE_H
#define PGSEQUENCE_H

#include "catalog/PgClass.h"

class PgSequence: public PgClass {
public:
    enum Direction {
        Ascending,
        Descending
    };

    // values that define the trigger
    // in recent versions from the catalog
	Oid typid = InvalidOid;
	int64_t start = 0;
	int64_t increment = 0;
	int64_t max = 0;
	int64_t min = 0;
	int64_t cache = 0;
	bool cycled = false;

	bool priv_usage = false;
	bool priv_select = false;
	bool priv_update = false;

    // Sequence state data, retrieved by SELECTing the sequence
    int64_t last = 0;
    int64_t log = 0; // shows how many fetches are left before new wal log
    bool called = false;

	using PgClass::PgClass;
	QString createSql() const;

    Direction direction() const
    {
        return increment < 0 ? Descending : Ascending;
    }
    int64_t defaultMin() const;
    int64_t defaultMax() const;
    int64_t defaultStart() const;
    int64_t defaultIncrement() const;
    int64_t defaultCache() const;
    // other option defaults are 1
    QString nonDefaultOptionsString() const;
};

#endif // PGSEQUENCE_H
