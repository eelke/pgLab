﻿#include "PgTypeContainer.h"
#include "Pgsql_Connection.h"
#include "Pgsql_Col.h"
#include <algorithm>


std::string PgTypeContainer::getLoadQuery() const
{
	return
		"SELECT oid, format_type(oid, NULL) AS name, typnamespace, typowner, typname, typlen, typbyval, typtype, typcategory, \n"
		"  typispreferred, typisdefined, typdelim, typrelid, typelem, typarray, typinput, typoutput, \n"
		"  typreceive, typsend, typmodin, typmodout, typanalyze, typalign, typstorage, typnotnull, \n"
		"  typbasetype, typtypmod, typndims, typcollation, typdefaultbin, typdefault, typacl \n"
		"FROM pg_type";
}

PgType PgTypeContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);
	Oid type_oid = col.nextValue();
	QString name = col.nextValue();
	Oid ns_oid   = col.nextValue();
	Oid owner    = col.nextValue();
	PgType v(m_catalog, type_oid, name, ns_oid);
	AclList acl_list;
	col >> v.typname >> v.len >> v.byval >> v.type >> v.category
		>> v.ispreferred >> v.isdefined >> v.delim >> v.relid >> v.elem >> v.array >> v.input >> v.output
		>> v.receive >> v.send >> v.modin >> v.modout >> v.analyze >> v.align >> v.storage >> v.notnull
		>> v.basetype >> v.typmod >> v.ndims >> v.collation >> v.defaultbin >> v.typdefault >> acl_list;
	v.setOwnerOid(owner);
	v.setAcls(std::move(acl_list));
	return v;
}
