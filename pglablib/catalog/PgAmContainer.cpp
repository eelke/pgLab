﻿#include "PgAmContainer.h"
#include "Pgsql_Col.h"

std::string PgAmContainer::getLoadQuery() const
{
	std::string q = "SELECT oid, amname FROM pg_am";
	return q;
}

PgAm PgAmContainer::loadElem(const Pgsql::Row &row)
{
	Pgsql::Col col(row);
	PgAm v;
	col >> v.oid >> v.name;

	return v;
}
