﻿#ifndef PGINHERITS_H
#define PGINHERITS_H

#include "Pgsql_declare.h"
#include <QString>
#include <vector>
#include <tuple>

class PgInherits {
public:
	using Key = std::tuple<Oid, int32_t>;


	Oid relid = InvalidOid; // oid
	Oid parent = InvalidOid; // oid
	int32_t seqno = 0; // integer

	PgInherits();

	bool operator==(Key _k) const { return relid == std::get<0>(_k) && seqno == std::get<1>(_k); }
	bool operator<(Key _k) const { return relid < std::get<0>(_k) || (relid == std::get<0>(_k) && seqno < std::get<1>(_k)); }
	bool operator<(const PgInherits &rhs) const { return relid < rhs.relid || (relid == rhs.relid && seqno < rhs.seqno); }
};

#endif // PGINHERITS_H
