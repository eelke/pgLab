﻿#ifndef PGSEQUENCECONTAINER_H
#define PGSEQUENCECONTAINER_H

#include "PgContainer.h"
#include "PgSequence.h"

namespace Pgsql {

	class Result;

}

class PgSequenceContainer: public PgContainer<PgSequence> {
public:
	using PgContainer<PgSequence>::PgContainer;

	std::string getLoadQuery() const override;

	void loadAll(Pgsql::Connection &conn) override;
protected:
	PgSequence loadElem(const Pgsql::Row &row) override;

};

#endif // PGSEQUENCECONTAINER_H
