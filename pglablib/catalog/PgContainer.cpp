﻿#include "PgContainer.h"
#include "PgDatabaseCatalog.h"
#include "Pgsql_Connection.h"

IPgContainer::IPgContainer(PgDatabaseCatalog& cat)
	: m_catalog(cat)
{}

bool IPgContainer::minimumVersion(int required_version) const
{
	return m_catalog.serverVersion() >= required_version;
}

bool IPgContainer::lessThenVersion(int required_version) const
{
	return m_catalog.serverVersion() < required_version;
}

void IPgContainer::loadAll(Pgsql::Connection &conn)
{
	std::string q = getLoadQuery();
	Pgsql::Result result = conn.query(q.c_str());
	if (result && result.resultStatus() == PGRES_TUPLES_OK) {
		load(result);
	}
	else {
		auto details = result.diagDetails();
		if (details.state == "42501") { // permission denied
			// ignore this for now
		}
		else {
			throw std::runtime_error("Query failed\n" + details.errorMessage);
		}
	}
}
