﻿#ifndef PGCATALOGTYPES_H
#define PGCATALOGTYPES_H

#include <libpq-fe.h>
#include <vector>
#include <boost/container/small_vector.hpp>

using AttNumVec	= std::vector<int16_t>;
template<int size>
using SmallAttNumVec = boost::container::small_vector<int16_t, size>;
using OidVec = std::vector<Oid>;

#endif // PGCATALOGTYPES_H
