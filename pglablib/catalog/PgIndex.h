﻿#ifndef PGINDEX_H
#define PGINDEX_H

#include "PgNamespaceObject.h"
#include "Pgsql_declare.h"
#include <QString>
#include <vector>

class PgIndex : public PgNamespaceObject {
public:

	Oid relid = InvalidOid; // oid of table (pg_class) where this is an index on
	int16_t natts = 0;
	bool isunique = false;
	bool isprimary = false;
	bool isexclusion = false;
	bool immediate = false;
	bool isclustered = false;
	bool isvalid = false;
	bool checkxmin = false;
	bool isready = false;
	bool islive = false;
	bool isreplident = false;
	std::vector<int16_t> key;
	std::vector<Oid> collation;
	std::vector<Oid> indclass;
	std::vector<int16_t> option;
	QString exprs;
	QString pred;
	QString definition;
    int64_t sizeBytes;

	using PgNamespaceObject::PgNamespaceObject;
	QString getAm() const;

    QString createSql() const override;
    QString dropSql() const override;

	QString typeName() const override;

    /// Returns true when this index has been created as part of the creation
    /// of a constaint.
    bool isSupportingIndex() const;
};

#endif // PGINDEX_H
