﻿#ifndef PGTYPECONTAINER_H
#define PGTYPECONTAINER_H

#include "PgType.h"
#include "PgContainer.h"

namespace Pgsql {

	class Result;

}

class PgTypeContainer: public PgContainer<PgType> {
public:
	using PgContainer<PgType>::PgContainer;

	virtual std::string getLoadQuery() const override;

protected:
	virtual PgType loadElem(const Pgsql::Row &row) override;
private:
};

#endif // PGTYPECONTAINER_H
