﻿#ifndef PGCONSTRAINT_H
#define PGCONSTRAINT_H

#include "PgNamespaceObject.h"
#include "Pgsql_Value.h"
#include "PgCatalogTypes.h"
#include <QString>
#include <libpq-fe.h>

#include <cstdint>
#include <vector>

enum class ConstraintType {
	PrimaryKey, // p
	ForeignKey, // f
	Unique, // u
	Check,  // c
	ConstraintTrigger, // t
	ExclusionConstraint, // x
};

void operator<<(ConstraintType &s, const Pgsql::Value &v);

QString ShortNameForConstraintType(ConstraintType ct);
QString LongNameForConstraintType(ConstraintType ct);


enum class ForeignKeyAction {
	NoAction, // a
	Restrict, // r
	Cascade, // c
	SetNull, // n
	SetDefault // d
};

void operator<<(ForeignKeyAction &s, const Pgsql::Value &v);

QString ForeignKeyActionToString(ForeignKeyAction fka);

enum class ForeignKeyMatch {
	Full, // f
	Partial, // p
	Simple // s
};

void operator<<(ForeignKeyMatch &s, const Pgsql::Value &v);

QString ForeignKeyMatchToString(ForeignKeyMatch fkm);

class PgConstraint: public PgNamespaceObject {
public:
	ConstraintType type;
	bool deferrable;
	bool deferred;
	bool validated;
	Oid relid = InvalidOid;  ///< the table this constraint is on
	Oid typid = InvalidOid;
	Oid indid = InvalidOid;  ///< index supporting the constraint
	Oid frelid = InvalidOid; ///< only for FK, referenced table pg_class
	ForeignKeyAction fupdtype; // on update
	ForeignKeyAction fdeltype; // on delete
	ForeignKeyMatch fmatchtype; // match type
	bool islocal;
	int32_t inhcount;
	bool noinherit;
	SmallAttNumVec<5> key; // list of constraint columns attnum
	SmallAttNumVec<5> fkey; // fkey list of referenced columns
	OidVec pfeqop;
	OidVec ppeqop;
	OidVec ffeqop;
	OidVec exclop;
	QString bin;

	QString definition;

	using PgNamespaceObject::PgNamespaceObject;

	QString typeName() const override;

	QString dropSql() const override;
	QString createSql() const override;
    bool isInherited() const
    {
        return inhcount > 0;
    }
};


#endif // PGCONSTRAINT_H
