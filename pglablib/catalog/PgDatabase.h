﻿#ifndef PGDATABASE_H
#define PGDATABASE_H

#include "PgServerObject.h"
#include <QString>
#include <libpq-fe.h>

class PgDatabase: public PgServerObject {
public:

    int encoding = 0;
    QString encodingString;
	QString collate;
	QString ctype;
    QString dbTemplate; ///< Not stored in the catalog but can be present in CREATE DATABASE statement
    bool isTemplate = false;
    bool allowConn = true;
    int connLimit = -1;
    Oid tablespace = InvalidOid;

	using PgServerObject::PgServerObject;

	bool isValid() const { return oid() != InvalidOid; }

	QString typeName() const override;
    virtual QString aclAllPattern() const override;
    virtual QString dropSql() const override;
    virtual QString createSql() const override;
};

#endif // PGDATABASE_H
