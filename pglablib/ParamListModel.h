﻿#ifndef PARAMLISTMODEL_H
#define PARAMLISTMODEL_H

#include <QAbstractTableModel>
#include <vector>
#include "Pgsql_declare.h"

class Param {
public:
	QString value; ///< the value of the parameter (currently this is passed directly)
	QString type; ///< the type of the parameter

	Param() = default;
	Param(const QString &v, const QString t)
		: value(v), type(t)
	{}
};
using t_ParamList = std::vector<Param>;


class ParamListModel : public QAbstractTableModel {
	Q_OBJECT
public:

	enum e_Column {
		ColValue = 0,
		ColType,
		ColumnCount // Keep last not a column just the count
	};

	explicit ParamListModel(QObject *parent = 0);

	// Header:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

//	bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	// Editable:
	bool setData(const QModelIndex &index, const QVariant &value,
				 int role = Qt::EditRole) override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;
	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
	bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

//	auto begin() const { return m_paramList.begin(); }
//	auto end() const { return m_paramList.end(); }
	const t_ParamList& GetParams() const;
	void SetParams(t_ParamList params);
private:

	t_ParamList m_paramList;
};

#endif // PARAMLISTMODEL_H
