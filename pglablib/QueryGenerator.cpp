﻿#include "QueryGenerator.h"
#include "catalog/PgDatabaseCatalog.h"
#include "catalog/PgClass.h"
#include "catalog/PgNamespace.h"
#include "catalog/PgNamespaceContainer.h"

using namespace Querygen;

QueryGeneratorFactory::QueryGeneratorFactory(std::shared_ptr<PgDatabaseCatalog> catalog)
	: m_catalog(catalog)
{}

UpdatePtr QueryGeneratorFactory::update(QString ns, QString table_name, QString alias)
{
	return std::make_shared<Update>(ns, table_name, alias);
}

UpdatePtr QueryGeneratorFactory::update(const PgClass &table_class, QString alias)
{
	//QString nsname = m_catalog->namespaces()->getByKey(table_class.relnamespace_name)
	return  update(table_class.nsName(), table_class.objectName(), alias);
}
