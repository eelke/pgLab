﻿#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <QString>
#include <QTableView>

QString msfloatToHumanReadableString(float ms);
void copySelectionToClipboard(const QTableView *view);
QString ConvertToMultiLineCString(const QString &in);
QString ConvertToMultiLineRawCppString(const QString &in);
QString ConvertLangToSqlString(const QString &in);
void exportTable(const QTableView *view, QTextStream &out);

inline QString stdStrToQ(const std::string &s)
{
	return QString::fromUtf8(s.c_str());
}

inline std::string qStrToStd(const QString &s)
{
	auto ba = s.toUtf8();
	return std::string(ba.data(), ba.size());
}

inline std::string qvarToStdStr(const QVariant &c)
{
	return qStrToStd(c.toString());
}



namespace std {

//  template <>
//  struct hash<QString>
//  {
//	std::size_t operator()(const QString& s) const
//	{
//		return qHash(s);
//	}
//  };

}

#endif // UTIL_H
