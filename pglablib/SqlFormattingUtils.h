﻿#ifndef SQLFORMATTINGUTILS_H
#define SQLFORMATTINGUTILS_H

#include <QString>

class PgClass;
class PgConstraint;
class PgDatabaseCatalog;
class PgIndex;

QString escapeIdent(const QString &input);
QString escapeLiteral(const QString &input);

bool identNeedsQuotes(QString ident);
QString quoteIdent(QString ident);
/// Puts a string in dollar quote for passing in a query
/// the standard quotes are $BODY$ if the tag already exists in the string it will start numbering
QString dollarQuoteString(const QString &value);


//QString genFQTableName(const PgDatabaseCatalog &catalog, const PgClass &cls);
QString getDropConstraintDefinition(const PgDatabaseCatalog &catalog, const PgConstraint &constraint);
QString getAlterTableConstraintDefinition(const PgDatabaseCatalog &catalog, const PgConstraint &constraint);
QString getConstraintDefinition(const PgDatabaseCatalog &catalog, const PgConstraint &constraint, const QString &indent);
/// Returns the foreignKey specific part of the constraint definition
QString getForeignKeyConstraintDefinition(const PgDatabaseCatalog &catalog, const PgConstraint &constraint, const QString &indent);
/// Returns the REFERENCES construct as used directly after a column in the create table statement
QString getForeignKeyConstraintReferences(const PgDatabaseCatalog &catalog, const PgConstraint &constraint);
/// Same as above but shortened as much as possible by leaving out defaults
QString getForeignKeyConstraintReferencesShort(const PgDatabaseCatalog &catalog, const PgConstraint &constraint);

#endif // SQLFORMATTINGUTILS_H
