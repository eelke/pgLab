﻿#ifndef WAITHANDLELIST_H
#define WAITHANDLELIST_H

#include <windows.h>
#include <vector>

class Win32Event;

using WaitResultValue = DWORD;

class WaitHandleList {
public:
	WaitHandleList();
	~WaitHandleList();

	WaitResultValue add(HANDLE h);
	WaitResultValue add(Win32Event &e);
	DWORD count() const;
	void clear();
	operator const HANDLE*() const;

private:
	std::vector<HANDLE> m_waitHandles;
};

#endif // WAITHANDLELIST_H
