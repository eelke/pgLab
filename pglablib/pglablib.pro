#-------------------------------------------------
#
# Project created by QtCreator 2017-12-13T17:36:43
#
#-------------------------------------------------

QT       += widgets core

TARGET = pglablib
TEMPLATE = lib
CONFIG += staticlib

! include( ../common.pri ) {
error( "Couldn't find the common.pri file!" )
}

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        Pglablib.cpp \
	ASyncDBConnection.cpp \
	ConnectionConfig.cpp \
    TestConnection.cpp \
    WaitHandleList.cpp \
	catalog/PgType.cpp \
	catalog/PgTypeContainer.cpp \
    catalog/PgNamespace.cpp \
    catalog/PgClass.cpp \
    catalog/PgDatabase.cpp \
    catalog/PgDatabaseContainer.cpp \
    catalog/PgAuthId.cpp \
    catalog/PgAuthIdContainer.cpp \
    catalog/PgClassContainer.cpp \
    catalog/PgDatabaseCatalog.cpp \
    catalog/PgNamespaceContainer.cpp \
    catalog/PgAttribute.cpp \
    catalog/PgContainer.cpp \
    catalog/PgAttributeContainer.cpp \
    catalog/PgIndex.cpp \
    catalog/PgIndexContainer.cpp \
    catalog/PgConstraint.cpp \
    catalog/PgConstraintContainer.cpp \
    ParamListJson.cpp \
    ParamListModel.cpp \
    ui/catalog/tables/TableNode.cpp \
    ui/catalog/tables/TableSize.cpp \
    ui/catalog/tables/TableTreeBuilder.cpp \
    util.cpp \
    SqlFormattingUtils.cpp \
    catalog/PgKeywordList.cpp \
    QueryGenerator.cpp \
    catalog/PgObject.cpp \
    catalog/PgTablespace.cpp \
    catalog/PgTablespaceContainer.cpp \
    codebuilder/LanguageConfig.cpp \
    codebuilder/CodeBuilder.cpp \
    codebuilder/NameManglingRules.cpp \
    codebuilder/DefaultConfigs.cpp \
    codebuilder/TypeMappings.cpp \
    codebuilder/IndentationConfig.cpp \
    codebuilder/StructureTemplate.cpp \
    FormatToStream.cpp \
    codebuilder/StringLiteralRules.cpp \
    codebuilder/StringEscapeRule.cpp \
    catalog/PgTrigger.cpp \
    catalog/PgTriggerContainer.cpp \
    catalog/PgProc.cpp \
    catalog/PgProcContainer.cpp \
    catalog/PgDatabaseObject.cpp \
    catalog/PgServerObject.cpp \
    catalog/PgNamespaceObject.cpp \
    catalog/PgCollation.cpp \
    catalog/PgCollationContainer.cpp \
    catalog/PgInherits.cpp \
    catalog/PgInheritsContainer.cpp \
    SelectionEditorFactory.cpp \
    catalog/PgAm.cpp \
    model/TypeModelFactory.cpp \
    model/TypeSelectionItemModel.cpp \
    catalog/PgAmContainer.cpp \
    model/CollationModel.cpp \
    model/CollationModelFactory.cpp \
    catalog/PgLanguageContainer.cpp \
    catalog/PgLanguage.cpp \
    catalog/PgAcl.cpp \
    catalog/PgSequence.cpp \
    catalog/PgSequenceContainer.cpp \
    utils/HumanReadableBytes.cpp

HEADERS += \
        Pglablib.h \
	ASyncDBConnection.h \
	ConnectionConfig.h \
    TestConnection.h \
    WaitHandleList.h \
    Win32Event.h \
	catalog/PgType.h \
	catalog/PgTypeContainer.h \
    catalog/PgNamespace.h \
    catalog/PgClass.h \
    catalog/PgDatabase.h \
    catalog/PgDatabaseContainer.h \
    catalog/PgContainer.h \
    catalog/PgAuthId.h \
    catalog/PgAuthIdContainer.h \
    catalog/PgClassContainer.h \
    catalog/PgDatabaseCatalog.h \
    catalog/PgNamespaceContainer.h \
    catalog/PgAttribute.h \
    catalog/PgAttributeContainer.h \
    catalog/PgIndex.h \
    catalog/PgIndexContainer.h \
    catalog/PgConstraint.h \
    catalog/PgConstraintContainer.h \
    ParamListJson.h \
    ParamListModel.h \
    ui/catalog/tables/TableNode.h \
    ui/catalog/tables/TableSize.h \
    ui/catalog/tables/TableTreeBuilder.h \
    util.h \
    SqlFormattingUtils.h \
    catalog/PgCatalogTypes.h \
    catalog/PgKeywordList.h \
    QueryGenerator.h \
    catalog/PgObject.h \
    catalog/PgTablespace.h \
    catalog/PgTablespaceContainer.h \
    codebuilder/LanguageConfig.h \
    codebuilder/CodeBuilder.h \
    codebuilder/NameManglingRules.h \
    codebuilder/DefaultConfigs.h \
    codebuilder/TypeMappings.h \
    codebuilder/IndentationConfig.h \
    codebuilder/StructureTemplate.h \
    FormatToStream.h \
    codebuilder/ResultLoopTemplate.h \
    codebuilder/StringEscapeRule.h \
    codebuilder/StringLiteralRules.h \
    catalog/PgTrigger.h \
    catalog/PgTriggerContainer.h \
    catalog/PgProc.h \
    catalog/PgProcContainer.h \
    catalog/PgDatabaseObject.h \
    catalog/PgServerObject.h \
    catalog/PgNamespaceObject.h \
    catalog/PgCollation.h \
    catalog/PgCollationContainer.h \
    AbstractModelFactory.h \
    AbstractEditorFactory.h \
    catalog/PgInherits.h \
    catalog/PgInheritsContainer.h \
    SelectionEditorFactory.h \
    model/TypeSelectionItemModel.h \
    model/TypeModelFactory.h \
    catalog/PgAm.h \
    catalog/PgAmContainer.h \
    model/CollationModel.h \
    model/CollationModelFactory.h \
    catalog/PgLanguageContainer.h \
    catalog/PgLanguage.h \
    catalog/PgAcl.h \
    catalog/PgSequence.h \
    catalog/PgSequenceContainer.h \
    utils/HumanReadableBytes.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../core/release/ -lcore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../core/debug/ -lcore
else:unix:!macx: LIBS += -L$$OUT_PWD/../core/ -lcore

INCLUDEPATH += $$PWD/../core
DEPENDPATH += $$PWD/../core

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/libcore.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/libcore.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/core.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/core.lib
else:unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../core/libcore.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../pgsql/release/ -lpgsql
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../pgsql/debug/ -lpgsql
else:unix:!macx: LIBS += -L$$OUT_PWD/../pgsql/ -lpgsql

INCLUDEPATH += $$PWD/../pgsql
DEPENDPATH += $$PWD/../pgsql

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pgsql/release/libpgsql.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pgsql/debug/libpgsql.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pgsql/release/pgsql.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pgsql/debug/pgsql.lib
else:unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../pgsql/libpgsql.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../core/release/ -lcore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../core/debug/ -lcore
else:unix: LIBS += -L$$OUT_PWD/../core/ -lcore

INCLUDEPATH += $$PWD/../core
DEPENDPATH += $$PWD/../core

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/libcore.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/libcore.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/core.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/core.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../core/libcore.a
