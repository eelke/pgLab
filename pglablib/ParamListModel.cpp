﻿#include "ParamListModel.h"

ParamListModel::ParamListModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

QVariant ParamListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant result;
	if (orientation == Qt::Horizontal) {
		if (role == Qt::DisplayRole) {
			switch (section) {
			case ColValue:
				result = tr("Value");
				break;
			case ColType:
				result = tr("Type");
				break;
			}
		}
	}
	else if (orientation == Qt::Vertical) {
		if (role == Qt::DisplayRole) {
			result = tr("$%1").arg(section + 1);
		}
	}
	return result;
}

//bool ParamListModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
//{
//	if (value != headerData(section, orientation, role)) {
//		// FIXME: Implement me!
//		emit headerDataChanged(orientation, section, section);
//		return true;
//	}
//	return false;
//}


int ParamListModel::rowCount(const QModelIndex &) const
{
	return m_paramList.size();
}

int ParamListModel::columnCount(const QModelIndex &) const
{
	return ColumnCount;
}

QVariant ParamListModel::data(const QModelIndex &index, int role) const
{
	QVariant result;
	if (index.isValid()) {
		int row = index.row();
		int col = index.column();
		if (role == Qt::DisplayRole || role == Qt::EditRole) {
			const auto& record = m_paramList[row];
			switch (col) {
			case ColValue: // value column
				result = record.value; // tr("val, %1").arg(row);
				break;
			case ColType: //  type column
				result = record.type; // tr("type, %1").arg(row);
				break;
			}
		}
	}

	return result;
}

bool ParamListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (data(index, role) != value) {
		if (role == Qt::EditRole) {
			int row = index.row();
			int col = index.column();
			auto& record = m_paramList[row];
			switch (col) {
			case ColValue:
				record.value = value.toString();
				break;
			case ColType:
				record.type = value.toString();
				break;
			}

            Q_EMIT dataChanged(index, index, QVector<int>() << role);
			return true;
		}
	}
	return false;
}

Qt::ItemFlags ParamListModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	return Qt::ItemIsEnabled | Qt::ItemIsEditable;
}

bool ParamListModel::insertRows(int row, int count, const QModelIndex &parent)
{
	beginInsertRows(parent, row, row + count - 1);
	// FIXME: Implement me!
	auto iter = m_paramList.begin() + row;
	m_paramList.insert(iter, count, Param());
	endInsertRows();
	return true;
}

//bool ParamListModel::insertColumns(int column, int count, const QModelIndex &parent)
//{
//	beginInsertColumns(parent, column, column + count - 1);
//	// FIXME: Implement me!
//	endInsertColumns();
//}

bool ParamListModel::removeRows(int row, int count, const QModelIndex &parent)
{
	beginRemoveRows(parent, row, row + count - 1);
	auto iter = m_paramList.begin() + row;
	m_paramList.erase(iter, iter + count);
	endRemoveRows();
	return true;
}

//bool ParamListModel::removeColumns(int column, int count, const QModelIndex &parent)
//{
//	beginRemoveColumns(parent, column, column + count - 1);
//	// FIXME: Implement me!
//	endRemoveColumns();
//}

const t_ParamList& ParamListModel::GetParams() const
{
	return m_paramList;
}

void ParamListModel::SetParams(t_ParamList params)
{
	m_paramList = std::move(params);
}
