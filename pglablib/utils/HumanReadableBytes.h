#pragma once

#include <string>

std::string HumanReadableBytes(uint64_t bytes);
