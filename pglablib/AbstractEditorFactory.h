﻿#ifndef ABSTRACTEDITORFACTORY_H
#define ABSTRACTEDITORFACTORY_H

#include <QObject>

class QWidget;
class QStyleOptionViewItem;
class QModelIndex;
class QAbstractItemModel;

/// Base class for factory classes that create item editors used in views.
///
/// Specifically the PgLabItemDelegate supports the use of EditorFactories.
class AbstractEditorFactory: public QObject {
	Q_OBJECT
public:
	using QObject::QObject;

	virtual QWidget *createEditor (QWidget *parent, const QStyleOptionViewItem &option,
								   const QModelIndex &index) const = 0;
	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const = 0;
	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const = 0;
};

#endif // ABSTRACTEDITORFACTORY_H
