﻿#ifndef COLLATIONMODELFACTORY_H
#define COLLATIONMODELFACTORY_H

#include "AbstractModelFactory.h"

class PgCollationContainer;

class CollationModelFactory: public AbstractModelFactory {
	Q_OBJECT
public:
	CollationModelFactory(QObject *parent, std::shared_ptr<const PgCollationContainer> collations);

	virtual QAbstractItemModel* createModel(QObject *parent = nullptr) const override;

private:
	std::shared_ptr<const PgCollationContainer> m_collations;
};

#endif // COLLATIONMODELFACTORY_H
