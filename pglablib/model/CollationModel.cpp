﻿#include "CollationModel.h"

#include "catalog/PgCollationContainer.h"
#include <algorithm>

CollationModel::CollationModel(QObject *parent)
	: QAbstractListModel(parent)
{
}

int CollationModel::rowCount(const QModelIndex &/*parent*/) const
{
	if (m_collations)
		return static_cast<int>(m_collations->count());

	return 0;
}

int CollationModel::columnCount(const QModelIndex &/*parent*/) const
{
	return colCount;
}

QVariant CollationModel::data(const QModelIndex &index, int role) const
{
	if (index.isValid()) {
		int row = index.row();
		int column = index.column();
		if (role == Qt::DisplayRole) {
			auto elem = m_collations->getByIdx(row);
			switch (column) {
			case OidCol: return elem.oid();
			case NameCol: return elem.objectName();
			}
		}
	}
	return QVariant();
}

void CollationModel::setCollationList(std::shared_ptr<const PgCollationContainer> collations)
{
	beginResetModel();
	m_collations = collations;
	endResetModel();
}
