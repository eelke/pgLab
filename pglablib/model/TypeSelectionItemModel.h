﻿#ifndef TYPESELECTIONITEMMODEL_H
#define TYPESELECTIONITEMMODEL_H

#include <QAbstractListModel>
#include <memory>
#include <vector>
#include "catalog/PgType.h"

class PgTypeContainer;

class TypeSelectionItemModel : public QAbstractListModel {
	Q_OBJECT
public:
	explicit TypeSelectionItemModel(QObject *parent = 0);

	void setTypeList(std::shared_ptr<const PgTypeContainer> types);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
	std::vector<QString> m_types;
};

class TypeModel : public QAbstractListModel {
	Q_OBJECT
public:
	enum e_Columns : int {
		OidCol,
		NameCol, //
        NamespaceCol,
        OwnerCol,
        CategoryCol,
		colCount
	};

	explicit TypeModel(QObject *parent = 0);

	void setTypeList(std::shared_ptr<const PgTypeContainer> types);
    PgType typ(int row) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
	std::shared_ptr<const PgTypeContainer> m_types;

    static QString TypCategoryString(TypCategory tc);
    Oid getType(int column) const;
};


#endif // TYPESELECTIONITEMMODEL_H
