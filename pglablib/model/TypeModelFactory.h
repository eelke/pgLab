﻿#ifndef TYPESELECTIONITEMMODELFACTORY_H
#define TYPESELECTIONITEMMODELFACTORY_H

#include "AbstractModelFactory.h"

class PgTypeContainer;

class TypeModelFactory: public AbstractModelFactory {
	Q_OBJECT
public:
	TypeModelFactory(QObject *parent, std::shared_ptr<const PgTypeContainer> types);

	virtual QAbstractItemModel* createModel(QObject *parent = nullptr) const override;

private:
	std::shared_ptr<const PgTypeContainer> m_types;
};

#endif // TYPESELECTIONITEMMODELFACTORY_H
