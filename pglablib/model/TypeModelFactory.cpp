﻿#include "TypeModelFactory.h"
#include "TypeSelectionItemModel.h"

TypeModelFactory::TypeModelFactory(QObject *parent, std::shared_ptr<const PgTypeContainer> types)
	: AbstractModelFactory(parent)
	, m_types(types)
{}


QAbstractItemModel* TypeModelFactory::createModel(QObject *parent) const
{
	auto model = new TypeModel(parent);
	model->setTypeList(m_types);
	return model;
}
