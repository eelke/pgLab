﻿#ifndef COLLATIONMODEL_H
#define COLLATIONMODEL_H

#include <QAbstractListModel>
#include <memory>
#include <vector>

class PgCollationContainer;

class CollationModel : public QAbstractListModel {
	Q_OBJECT
public:
	enum e_Columns : int {
		OidCol,
		NameCol, //
		colCount
	};

	explicit CollationModel(QObject *parent = nullptr);

	void setCollationList(std::shared_ptr<const PgCollationContainer> collations);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
	std::shared_ptr<const PgCollationContainer> m_collations;
};


#endif // COLLATIONMODEL_H
