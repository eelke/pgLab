﻿#include "CollationModelFactory.h"
#include "CollationModel.h"

CollationModelFactory::CollationModelFactory(QObject *parent, std::shared_ptr<const PgCollationContainer> collations)
	: AbstractModelFactory(parent)
	, m_collations(collations)
{}


QAbstractItemModel* CollationModelFactory::createModel(QObject *parent) const
{
	auto model = new CollationModel(parent);
	model->setCollationList(m_collations);
	return model;
}

