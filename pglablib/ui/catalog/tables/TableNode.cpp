#include "TableNode.h"
#include "catalog/PgDatabaseCatalog.h"
namespace {
    PgDatabaseCatalog dummyCatalog;
}

TableNode::TableNode()
: _class(dummyCatalog, InvalidOid, "", InvalidOid)
{

}

TableNode::TableNode(const PgClass &cls)
: _class(cls)
{}

const TableNode *TableNode::getChildPtr(int index) const
{
    return children.at(index).get();
}
