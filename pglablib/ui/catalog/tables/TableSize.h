#pragma once

#include <cstdint>

class TableSize {
public:
    int oid;
    int64_t totalBytes = -1;
    int64_t indexBytes = -1;
    int64_t toastBytes = -1;

    TableSize();
};
