#pragma once

#include "TableSize.h"
#include "catalog/PgClass.h"

class TableNode;
using t_Tables = std::vector<std::shared_ptr<TableNode>>;




class TableNode {
public:
    PgClass _class;
    TableSize sizes;

    int myIndex;
    std::weak_ptr<TableNode> parent; // hope we do not need it (must be weak to prevent circular reference)
    t_Tables children;

    TableNode();
    TableNode(const PgClass &cls);

    const TableNode* getChildPtr(int index) const;
};
