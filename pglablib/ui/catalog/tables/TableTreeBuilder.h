#pragma once

#include "Pgsql_oids.h"
#include <map>
#include <memory>

class PgClass;
class TableNode;
class IFindParents;

class TableTreeBuilder
{
public:
    TableTreeBuilder(const std::map<Oid, PgClass> &source, const IFindParents &inheritance);

    std::tuple<std::shared_ptr<TableNode>, std::map<Oid, std::shared_ptr<TableNode>>> Build();

private:
    const std::map<Oid, PgClass> &source;
    const IFindParents &inheritance;
    std::map<Oid, std::shared_ptr<TableNode>> processedNodes;
    std::shared_ptr<TableNode> rootNode;

    bool hasParent(const PgClass &cls) const;
    std::shared_ptr<TableNode>  addNode(const PgClass &cls);
    void addToToplevel(std::shared_ptr<TableNode> node);
    void addToParents(std::shared_ptr<TableNode> node, const std::vector<Oid> &parents);
    std::shared_ptr<TableNode> getParent(Oid oid);
    void AssignNodeIndexesAndParents(std::shared_ptr<TableNode> parent);
};

