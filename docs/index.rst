.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pgLab User Manual
=================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   preface
   releasenotes
   installation
   connectionmanager/index
   databasewindow/index
   internals

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

