#######
Preface
#######

*************
What is pgLab
*************

pgLab is a PostgreSQL database query tool that also has some views for inspecting
the database schema. It does not provide dialogs for creating new database objects
you can ofcourse execute DDL to create anything you like.
(Note adding dialogs for creating objects is a lot of work. I am not against it
but it has currently no priority for me.)

The strong point of pgLab are:

* separation of different databases into separate windows.
* inspecting of database with subtabs in a single main tab so the list of toplevel tabs stays short.
* very fast processing of query results millions of rows are no problem.

**************
What is it not
**************

If you are unfamiliar with the SQL and need dialogs for doing anything this
is not the tool for you and you are much better of using pgAdmin.
