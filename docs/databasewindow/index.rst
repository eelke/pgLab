###############
Database Window
###############

When opening a database connection the program will open a database window.
Within this window you can only work in a single database and with global
server objects like for instance roles.

The window start with a single query tab.

*************
The Query tab
*************

In the query tab you can enter your queries and execute them with :kbd:`F5` or
:menuselection:`Query --> Execute query`. You can use postgresql's native query
parameters that consist of a `$` followed by a number. The values for these
parameters can be entered in the list to the right of the query editor.
Note that when using parameters postgresql will not accept multiple queries.

A new query tab can be opened with :kbd:`Ctrl+N` or :menuselection:`File --> New query`.

Queries can also be saved with :kbd:`Ctrl+S` or :menuselection:`File --> Save query`
and opened with :kbd:`Ctrl+O` or :menuselection:`File --> Open query`. You can also
drag and drop a bunch of files onto the window and they will be opened in
seperate query tabs.

Explain
=======

You can get a user friendly version of explain output with :kbd:`F7` or :menuselection:`Query --> Explain`.
If you want explain analyze use :kbd:`Shift+F7` or :menuselection:`Query --> Explain analyze`.

Colors are used to highlight the explain nodes that take the most time. Also
keep an eye on the estimation error a high error can indicate stale statistics.


************
Schema views
************

Schema views van be opened from the :menuselection:`View` menu. These views all work
in the same way and get their data from the postgres catalog. The only difference
is the filter used.

User schema
    Shows all objects that are not part of a system namespace like pg_catalog or
    information schema.

pg_catalog
    This is the native postgresql catalog.

information_schema
    This is the SQL compatible catalog.

The grids within the schema tabs should all be sortable. Click on the header of
a column to sort by the contents of that column. Click a second time to reverse
the sort order.

Tables
======

Double clicking a table will open a crud tab for that tabel

Note the sizes are retrieved in the background and can take some time to appear
on large databases.

Columns
-------

On the columns tab you can see ofcourse the columns of the table. Note that
this table has as its last columns a column for each index on the table. The
cells contain the position of the table column represented by that row in the
index. Hover over the header to see the name of the index.

When a row is selected SQL for dropping or creating that column and also
to just fix the default or NULL constraint this is useful when inspecting
one database to correct another. When multiple rows are selected SQL for all
those rows is shown.

*********
Crud tabs
*********

If the table had no primary key the data will be readonly. Generated always
columns will ofcourse also be readonly. Use :kbd:`tab` to go from one field to
the next accept your changes with :kbd:`Enter`. To enter a value for a boolean
column use :kbd:`t` for true or :kbd:`f` for false.

An empty value will be seen as a NULL to enter an empty string use two single quotes ''.

When data is saved the program will generate INSERT and UPDATE queries that
return the data that was actually put in the database by the server and update
the data in the grid to match. This includes data for generated columns.

Shortcuts:

* :kbd:`Ctrl+D` delete selected rows.
* :kbd:`F5` reload data
* :kbd:`Ctrl+W` close tab
