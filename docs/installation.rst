============
Installation
============

Currently only binaries for Windows 64-bit are provided.

-------
Windows
-------

Downloads can be found `here
<https://eelkeklein.stackstorage.com/s/E9xkMGQDFjHv5XN3>`_.


Installation
============

Unpack the contents of the 7zip archive to a folder of your choosing for
instance `%ProgramFiles%\\pgLab`. You can run the pgLab.exe from there. If
it is complaining about missing files this is probably because the required
Visual C++ Runtime has not yet been installed on your machine you can get it
from `microsoft <https://aka.ms/vs/17/release/vc_redist.x64.exe>`_.

If you want a shortcut you can make one by copying the exe and then choosing
:menuselection:`Paste shortcut` from the context menu in the location where you
want the shortcut.

Upgrade
=======

Note this section assumes you have not saved any of your own data to
the folder in which you unpacked pgLab. If you did move it to another location.

Download the new archive, replace the contents of the folder with the contents
of the new archive. Any shortcuts or pinned taskbar buttons should still work.

Uninstall
=========

Just remove the folder you unpacked pgLab in. If you also want to remove all user
data you should also remove `%AppData%\\pglab` for each user that has used the
program.
