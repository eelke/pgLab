===================
Editing connections
===================

--------------
New connection
--------------

With the :guilabel:`Add connection` button you can create a new connection
configuration.

Entering a password is optional at this stage. You may need it ofcourse if you
want to save the password or test the connection. When you :guilabel:`test` the
connection and the the connection works a list of databases is populated to
select from for the database field.

-----------------
Edit a connection
-----------------

To edit a configuration you need to click the :guilabel:`Configure connection`.

Note the password field will always be empty even if a password was saved. As
long as you do not edit this field the saved password will stay unchanged. As
soon as you change this field or when you clear the :guilabel:`Save password`
checkbox the password will be updated or cleared when you accept your changes
by clicking :guilabel:`OK`.

-----------
Edit a copy
-----------

When you select :guilabel:`Configure copy` you will basically start editing
a new connection with the details of the connection that was selected. Note
the password will not be copied.
