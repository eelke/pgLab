﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
//#include "PasswordManager.h"
#include "PrintTo_Qt.h"

using namespace testing;


#include <botan/pwdhash.h>

//TEST(Botan, recreate)
//{
//	auto phf = Botan::PasswordHashFamily::create("Scrypt");
//	size_t N = 65536, r = 10, p = 2;
//	auto ph = phf->from_params(N, r, p);
//	auto sc = dynamic_cast<Botan::Scrypt*>(ph.get());

//	ASSERT_EQ(N, sc->N());
//	ASSERT_EQ(r, sc->r());
//	ASSERT_EQ(p, sc->p());

//	auto phf2 = phf->create(ph->to_string());
//	phf2->default_params()

//}

//TEST(PasswordManager, initial_changeMasterPassword_returns_true)
//{
//	PasswordManager pwm(10);

//	auto res = pwm.changeMasterPassword("", "my test passphrase");
//	ASSERT_NO_THROW( res.get() );
//	ASSERT_THAT( res.get(), Eq(true) );
//}

//TEST(PasswordManager, unlock_succeeds)
//{
//	PasswordManager pwm(10);

//	std::string passphrase = "my test passphrase";

//	auto res = pwm.changeMasterPassword("", passphrase);
//	ASSERT_NO_THROW( res.get() );
//	ASSERT_THAT( res.get(), Eq(true) );

//	auto res2 = pwm.unlock(passphrase);
//	ASSERT_NO_THROW( res2.get() );
//	ASSERT_THAT( res2.get(), Eq(true) );
//}

//TEST(PasswordManager, unlock_fails)
//{
//	PasswordManager pwm(10);

//	std::string passphrase = "my test passphrase";

//	auto res = pwm.changeMasterPassword("", passphrase);
//	ASSERT_NO_THROW( res.get() );
//	ASSERT_THAT( res.get(), Eq(true) );

//	auto res2 = pwm.unlock(passphrase + "2");
//	ASSERT_NO_THROW( res2.get() );
//	ASSERT_THAT( res2.get(), Eq(false) );
//}

//TEST(PasswordManager, test_save_get)
//{
//	PasswordManager pwm(10);

//	std::string passphrase = "my test passphrase";

//	auto res = pwm.changeMasterPassword("", passphrase);
//	ASSERT_NO_THROW( res.get() );
//	ASSERT_THAT( res.get(), Eq(true) );

////	auto res2 = pwm.unlock(passphrase + "2");
////	ASSERT_NO_THROW( res2.get() );
////	ASSERT_THAT( res2.get(), Eq(false) );

//	const std::string password = "password123";
//	const std::string key = "abc";

//	auto res2 = pwm.savePassword(key, password);
//	ASSERT_THAT( res2.valid(), Eq(true) );

//	std::string result;
//	auto res3 = pwm.getPassword(key, result);
//	ASSERT_THAT( res3.valid(), Eq(true) );
//	ASSERT_THAT( res3.get(), Eq(true) );
//	ASSERT_THAT( result, Eq(password) );

//}
