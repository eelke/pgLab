﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "CsvWriter.h"
#include <QTextStream>
#include <QByteArray>
#include "PrintTo_Qt.h"

using namespace testing;


TEST(CsvWriter, one_row_two_numbers)
{
	QString result;
	QTextStream stream(&result);
	CsvWriter writer(&stream);
	writer.setQuote('"');
	writer.setSeperator(',');
	writer.writeField("1");
	writer.writeField("2");
	writer.nextRow();

	QString expected = QString::fromUtf8("1,2\n");
	ASSERT_THAT(result, Eq(expected));
}

TEST(CsvWriter, one_row_one_number_one_unquoted_string)
{
	QString result;
	QTextStream stream(&result);
	CsvWriter writer(&stream);
	writer.setQuote('"');
	writer.setSeperator(',');
	writer.writeField("1");
	writer.writeField("hello");
	writer.nextRow();

	QString expected = QString::fromUtf8("1,hello\n");
	ASSERT_THAT(result, Eq(expected));
}

TEST(CsvWriter, one_row_one_number_one_quoted_string)
{
	QString result;
	QTextStream stream(&result);
	CsvWriter writer(&stream);
	writer.setQuote('"');
	writer.setSeperator(',');
	writer.writeField("1");
	writer.writeField("hel,lo");
	writer.nextRow();

	QString expected = QString::fromUtf8("1,\"hel,lo\"\n");
	ASSERT_THAT(result, Eq(expected));
}

TEST(CsvWriter, newline_in_field)
{
	QString result;
	QTextStream stream(&result);
	CsvWriter writer(&stream);
	writer.setQuote('"');
	writer.setSeperator(',');
	writer.writeField("1");
	writer.writeField("hel\nlo");
	writer.nextRow();

	QString expected = QString::fromUtf8("1,\"hel\nlo\"\n");
	ASSERT_THAT(result, Eq(expected));
}

TEST(CsvWriter, escape_quote)
{
	QString result;
	QTextStream stream(&result);
	CsvWriter writer(&stream);
	writer.setQuote('"');
	writer.setSeperator(',');
	writer.writeField("1");
	writer.writeField("hel\"lo");
	writer.nextRow();

	QString expected = QString::fromUtf8("1,\"hel\"\"lo\"\n");
	ASSERT_THAT(result, Eq(expected));
}

TEST(CsvWriter, non_default_seperator)
{
	QString result;
	QTextStream stream(&result);
	CsvWriter writer(&stream);
	writer.setQuote('"');
	writer.setSeperator('\t');
	writer.writeField("1");
	writer.writeField("hel,lo");
	writer.nextRow();

	QString expected = QString::fromUtf8("1\thel,lo\n");
	ASSERT_THAT(result, Eq(expected));
}

TEST(CsvWriter, non_default_quote)
{
	QString result;
	QTextStream stream(&result);
	CsvWriter writer(&stream);
	writer.setQuote('*');
	writer.setSeperator('\t');
	writer.writeField("1");
	writer.writeField("hel\tlo");
	writer.nextRow();

	QString expected = QString::fromUtf8("1\t*hel\tlo*\n");
	ASSERT_THAT(result, Eq(expected));
}
