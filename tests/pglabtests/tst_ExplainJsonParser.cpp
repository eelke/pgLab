﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "ExplainTreeModelItem.h"
#include "json/json.h"
#include "PrintTo_Qt.h"

using namespace testing;




TEST(ExplainParser, verbose)
{
	std::string input = R"__json__("[
			  {
				"Plan": {
				  "Node Type": "Limit",
				  "Parallel Aware": false,
				  "Startup Cost": 0.42,
				  "Total Cost": 0.42,
				  "Plan Rows": 10,
				  "Plan Width": 830,
				  "Output": ["fr1.callstack_crc_2", "fr1.id", "fr1.program", "fr1.version", "fr1.lic_bedrijf", "fr1.lic_plaats", "fr1.lic_number", "fr1.callstack_crc_1", "fr1.callstack_crc_3", "fr1.exception_class", "fr1.exception_message", "fr1.full_report", "fr1.screenshot_filename", "fr1.ts", "fr1.duplicate_of", "fr1.actief", "fr1.commentaar", "fr1.bugzilla", "fr1.attachment_filename", "fr1.version_according_to_datetime", "fr1.program_according_to_full_report", "fr1.date", "fr2.id", "fr2.program", "fr2.version", "fr2.lic_bedrijf", "fr2.lic_plaats", "fr2.lic_number", "fr2.callstack_crc_1", "fr2.callstack_crc_3", "fr2.exception_class", "fr2.exception_message", "fr2.full_report", "fr2.screenshot_filename", "fr2.ts", "fr2.duplicate_of", "fr2.actief", "fr2.commentaar", "fr2.bugzilla", "fr2.attachment_filename", "fr2.version_according_to_datetime", "fr2.program_according_to_full_report", "fr2.date"],
				  "Plans": [
					{
					  "Node Type": "Nested Loop",
					  "Parent Relationship": "Outer",
					  "Parallel Aware": false,
					  "Join Type": "Inner",
					  "Startup Cost": 0.42,
					  "Total Cost": 103148.97,
					  "Plan Rows": 493512899,
					  "Plan Width": 830,
					  "Output": ["fr1.callstack_crc_2", "fr1.id", "fr1.program", "fr1.version", "fr1.lic_bedrijf", "fr1.lic_plaats", "fr1.lic_number", "fr1.callstack_crc_1", "fr1.callstack_crc_3", "fr1.exception_class", "fr1.exception_message", "fr1.full_report", "fr1.screenshot_filename", "fr1.ts", "fr1.duplicate_of", "fr1.actief", "fr1.commentaar", "fr1.bugzilla", "fr1.attachment_filename", "fr1.version_according_to_datetime", "fr1.program_according_to_full_report", "fr1.date", "fr2.id", "fr2.program", "fr2.version", "fr2.lic_bedrijf", "fr2.lic_plaats", "fr2.lic_number", "fr2.callstack_crc_1", "fr2.callstack_crc_3", "fr2.exception_class", "fr2.exception_message", "fr2.full_report", "fr2.screenshot_filename", "fr2.ts", "fr2.duplicate_of", "fr2.actief", "fr2.commentaar", "fr2.bugzilla", "fr2.attachment_filename", "fr2.version_according_to_datetime", "fr2.program_according_to_full_report", "fr2.date"],
					  "Plans": [
						{
						  "Node Type": "Seq Scan",
						  "Parent Relationship": "Outer",
						  "Parallel Aware": false,
						  "Relation Name": "foutrapport",
						  "Schema": "public",
						  "Alias": "fr1",
						  "Startup Cost": 0.00,
						  "Total Cost": 6048.89,
						  "Plan Rows": 105489,
						  "Plan Width": 419,
						  "Output": ["fr1.id", "fr1.program", "fr1.version", "fr1.lic_bedrijf", "fr1.lic_plaats", "fr1.lic_number", "fr1.callstack_crc_1", "fr1.callstack_crc_2", "fr1.callstack_crc_3", "fr1.exception_class", "fr1.exception_message", "fr1.full_report", "fr1.screenshot_filename", "fr1.ts", "fr1.duplicate_of", "fr1.actief", "fr1.commentaar", "fr1.bugzilla", "fr1.attachment_filename", "fr1.version_according_to_datetime", "fr1.program_according_to_full_report", "fr1.date"]
						},
						{
						  "Node Type": "Index Scan",
						  "Parent Relationship": "Inner",
						  "Parallel Aware": false,
						  "Scan Direction": "Forward",
						  "Index Name": "foutrapport_callstack_crc_2_idx",
						  "Relation Name": "foutrapport",
						  "Schema": "public",
						  "Alias": "fr2",
						  "Startup Cost": 0.42,
						  "Total Cost": 0.81,
						  "Plan Rows": 11,
						  "Plan Width": 419,
						  "Output": ["fr2.id", "fr2.program", "fr2.version", "fr2.lic_bedrijf", "fr2.lic_plaats", "fr2.lic_number", "fr2.callstack_crc_1", "fr2.callstack_crc_2", "fr2.callstack_crc_3", "fr2.exception_class", "fr2.exception_message", "fr2.full_report", "fr2.screenshot_filename", "fr2.ts", "fr2.duplicate_of", "fr2.actief", "fr2.commentaar", "fr2.bugzilla", "fr2.attachment_filename", "fr2.version_according_to_datetime", "fr2.program_according_to_full_report", "fr2.date"],
						  "Index Cond": "(fr2.callstack_crc_2 = fr1.callstack_crc_2)"
						}
					  ]
					}
				  ]
				}
			  }
			]")__json__";



	Json::Value root;   // will contains the root value after parsing.
	Json::Reader reader;
	bool parsingSuccessful = reader.parse(input, root);
	ASSERT_TRUE(parsingSuccessful);
	auto explain = ExplainRoot::createFromJson(root);

	ASSERT_TRUE(explain != nullptr);
}
