﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "ConnectionConfig.h"
#include "PrintTo_Qt.h"

using namespace testing;


TEST(escapeConnectionStringValue, emptyValue)
{
	auto input = QStringLiteral("");
	auto expected =	QStringLiteral("''");

	auto result = ConnectionConfig::escapeConnectionStringValue(input);

	ASSERT_EQ(result, expected);
}

TEST(escapeConnectionStringValue, simpleValue)
{
	auto input = QStringLiteral("abc");
	auto expected =	QStringLiteral("abc");

	auto result = ConnectionConfig::escapeConnectionStringValue(input);

	ASSERT_EQ(result, expected);
}

TEST(escapeConnectionStringValue, valueWithSpace)
{
	auto input = QStringLiteral("ab c");
	auto expected =	QStringLiteral("'ab c'");

	auto result = ConnectionConfig::escapeConnectionStringValue(input);

	ASSERT_EQ(result, expected);
}

TEST(escapeConnectionStringValue, valueWithQuote)
{
	auto input = QStringLiteral("ab'c");
	auto expected =	QStringLiteral("'ab\\'c'");

	auto result = ConnectionConfig::escapeConnectionStringValue(input);

	ASSERT_EQ(result, expected);
}

TEST(escapeConnectionStringValue, valueBackslash)
{
	auto input = QStringLiteral("ab\\c");
	auto expected =	QStringLiteral("'ab\\\\c'");

	auto result = ConnectionConfig::escapeConnectionStringValue(input);

	ASSERT_EQ(result, expected);
}
