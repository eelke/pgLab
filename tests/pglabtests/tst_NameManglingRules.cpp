﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "PrintTo_Qt.h"
#include "codebuilder/NameManglingRules.h"
#include "Pgsql_oids.h"

using namespace testing;

class NameManglingRulesTest : public ::testing::Test, public ::testing::WithParamInterface<
		std::tuple<QString, NameManglingRules::CaseConversion, bool> > {
protected:

	virtual void SetUp() override
	{
		NameManglingRules r;

//		auto p = GetParam();
//		//ReplaceRules replaceRules;
//	//		{ {"[ -_]", QRegularExpression::OptimizeOnFirstUsageOption }, "", true }
//	//	r.replaceRules;

//		//CaseConversion caseConversion = CaseConversion::AsIs; ///< overall case conversion rule
//		r.caseConversion = std::get<1>(p);
//		//CaseConversion caseFirstChar = CaseConversion::AsIs; ///< case of the first char
//		//r.camelCase = std::get<2>(p); ///< removes underscores and make first char after underscore uppercase
//		rules = r;
	}
	NameManglingRules rules;
};

// First a few test to test some basic things


TEST_F(NameManglingRulesTest, caseConversionAsIs)
{
	QString input = "Ab";
	QString expected = input;

	NameManglingRules rules;
	rules.caseConversion = NameManglingRules::CaseConversion::AsIs;
	QString result = rules.transform(input);

	ASSERT_EQ(result, expected);
}

TEST_F(NameManglingRulesTest, caseConversionLower)
{
	QString input = "Ab";
	QString expected = "ab";

	NameManglingRules rules;
	rules.caseConversion = NameManglingRules::CaseConversion::Lower;
	QString result = rules.transform(input);

	ASSERT_EQ(result, expected);
}

TEST_F(NameManglingRulesTest, caseConversionUpper)
{
	QString input = "Ab";
	QString expected = "AB";

	NameManglingRules rules;
	rules.caseConversion = NameManglingRules::CaseConversion::Upper;
	QString result = rules.transform(input);

	ASSERT_EQ(result, expected);
}

TEST_F(NameManglingRulesTest, replace)
{
	QString input = "abc-def ghi";
	QString expected = "abc_Def_Ghi";

	NameManglingRules rules;
	rules.replaceRules.push_back({QRegularExpression("[- ]"), "_", true});
	QString result = rules.transform(input);

	ASSERT_EQ(result, expected);
}

