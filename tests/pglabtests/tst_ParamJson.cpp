﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "ParamListModel.h"
#include "ParamListJson.h"
#include "PrintTo_Qt.h"

using namespace testing;


TEST(ParamListToJson, test1)
{
	t_ParamList params;
	params.emplace_back("valuestr", "typestr");

	Json::Value root = ParamListToJson(params);   // will contains the root value after parsing.
	ASSERT_TRUE(root.isArray());
	ASSERT_EQ(root.size(), 1);

	Json::Value e = root[0];
	ASSERT_TRUE(e.isMember("type"));
	ASSERT_TRUE(e.isMember("value"));
	ASSERT_EQ(e["type"].asString(), "typestr");
	ASSERT_EQ(e["value"].asString(), "valuestr");
//	Json::Reader reader;
//	bool parsingSuccessful = reader.parse(input, root);
//	auto explain = ExplainRoot::createFromJson(root);

//	ASSERT_TRUE(explain != nullptr);
}

