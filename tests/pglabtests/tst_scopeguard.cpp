﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "ScopeGuard.h"
#include "PrintTo_Qt.h"

using namespace testing;


TEST(ScopeGuard, normal_run_fun_on_destruction_1)
{
	bool result = false;
	auto sg = scopeGuard([&result]() { result = true; });
	ASSERT_THAT(result, Eq(false));
}

TEST(ScopeGuard, normal_run_fun_on_destruction_2)
{
	bool result = false;
	{
		auto sg = scopeGuard([&result]() { result = true; });
	}

	ASSERT_EQ(result, true);
}

TEST(ScopeGuard, dismiss)
{
	bool result = false;
	{
		auto sg = scopeGuard([&result]() { result = true; });
		sg.dismiss();
	}

	ASSERT_THAT(result, Eq(false));
}

TEST(ScopeGuard, SCOPE_EXIT_macro_1)
{
	bool result = false;
	{
		SCOPE_EXIT { result = true; };
		ASSERT_THAT(result, Eq(false)); // prove previous statement hasn't run yet
	}

}

TEST(ScopeGuard, SCOPE_EXIT_macro_2)
{
	bool result = false;
	{
		SCOPE_EXIT { result = true; };
	}

	ASSERT_THAT(result, Eq(true));
}
