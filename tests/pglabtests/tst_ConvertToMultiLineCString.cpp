﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "util.h"
#include "PrintTo_Qt.h"

using namespace testing;


TEST(ConvertToMultiLineCString, singleLine)
{
	QString in(R"__(SELECT 1)__");
	QString expected(R"__("SELECT 1")__");

	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertToMultiLineCString, singleLineTrimWhiteSpace)
{
	QString in(R"__(SELECT 1  )__");
	QString expected(R"__("SELECT 1")__");

	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertToMultiLineCString, singleLineWithComment)
{
	QString in(R"__(SELECT 1 -- hello)__");
	QString expected(R"__("SELECT 1" // hello)__");

	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertToMultiLineCString, singleLineWithCommentTrimWhiteSpace)
{
	// Check whitespace at end is removed but in between is kept
	QString in(R"__(SELECT 1   -- hello  )__");
	QString expected(R"__("SELECT 1"   // hello)__");

	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertToMultiLineCString, multiLine)
{
	QString in(
R"__(SELECT kol
FROM table)__");
	QString expected(
R"__("SELECT kol\n"
"FROM table")__");
	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertToMultiLineCString, multiLineWithComment)
{
	QString in(
R"__(SELECT kol    -- eerste
FROM table    -- tweede)__");
	QString expected(
R"__("SELECT kol\n"    // eerste
"FROM table"    // tweede)__");
	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

// Test case for a discovered bug
TEST(ConvertToMultiLineCString, multiLineWithCommentNoErronousRepeat)
{
	QString in(
R"__(SELECT kol    -- eerste
FROM table)__");
	QString expected(
R"__("SELECT kol\n"    // eerste
"FROM table")__");
	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertToMultiLineCString, trimExtraEmptyLines)
{
	QString in(R"__(
SELECT 1
)__");
	QString expected(R"__("SELECT 1")__");

	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertToMultiLineCString, trimExtraEmptyLines2)
{
	QString in(R"__(
SELECT 1

FROM tab
)__");
	QString expected(R"__("SELECT 1\n"
"\n"
"FROM tab")__");

	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertToMultiLineCString, escapeDoubleQuote)
{
	QString in(R"__(SELECT * FROM "table")__");
	QString expected("\"SELECT * FROM \\\"table\\\"\"");

	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertToMultiLineCString, escapeBackslash)
{
	QString in(R"__(SELECT '\test')__");
	QString expected(R"__("SELECT '\\test'")__");

	auto output = ConvertToMultiLineCString(in);
	ASSERT_EQ(output, expected);
}
