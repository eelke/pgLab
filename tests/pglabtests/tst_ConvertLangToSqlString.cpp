﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "util.h"
#include "PrintTo_Qt.h"

using namespace testing;


TEST(ConvertLangToSqlString, simple)
{
	QString in(R"__( "SELECT" )__");
	QString expected(R"__(SELECT)__");

	auto output = ConvertLangToSqlString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertLangToSqlString, testEscapedQuote)
{
	QString in(R"__( "SELECT\"" )__");
	QString expected(R"__(SELECT")__");

	auto output = ConvertLangToSqlString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertLangToSqlString, testEscapedNewLine)
{
	QString in(R"__( "SELECT\nFROM" )__");
	QString expected(R"__(SELECT
FROM)__");

	auto output = ConvertLangToSqlString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertLangToSqlString, testConcatPlus)
{
	QString in(R"__( "SELECT" + " FROM" )__");
	QString expected(R"__(SELECT FROM)__");

	auto output = ConvertLangToSqlString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertLangToSqlString, testConcatDot)
{
	QString in(R"__( "SELECT"." FROM" )__");
	QString expected(R"__(SELECT FROM)__");

	auto output = ConvertLangToSqlString(in);
	ASSERT_EQ(output, expected);
}

TEST(ConvertLangToSqlString, testSemiColon)
{
	QString in(R"__( "SELECT"." FROM"; )__");
	QString expected(R"__(SELECT FROM)__");

	auto output = ConvertLangToSqlString(in);
	ASSERT_EQ(output, expected);
}

//TEST(ConvertLangToSqlString, testComment)
//{
//    QString in(R"__(  "SELECT * " // comment
//  "FROM t"; )__");
//    QString expected(R"__(SELECT *
//FROM t)__");

//    auto output = ConvertLangToSqlString(in);
//    ASSERT_EQ(output, expected);
//}
