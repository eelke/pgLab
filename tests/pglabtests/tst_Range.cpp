﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "PrintTo_Qt.h"


template <typename T>
class Range {
public:
	Range(T start, T end, bool incl_start = true, bool incl_end = false)
		: m_start(start), m_end(end), m_inclStart(incl_start), m_inclEnd(incl_end)
	{}

	bool includes(T v) const
	{
		// assume chance is small that outcome hangs on the edge cases
		// so we start with testing for definitly outside or definitly inside
		if (v < m_start || v > m_end)
			return false;
		if (v > m_start && v < m_end)
			return true;
		// Now the edge cases
		if (m_inclStart && v == m_start)
			return true;
		if (m_inclEnd && v == m_end)
			return true;

		return false;
	}

	bool overlap(Range<T> r) const
	{
        if ( (r.m_start < m_end || (m_inclStart))
			&& r.m_end > m_start)
			return true;

		return false;
	}
private:
	T m_start;
	T m_end;
	bool m_inclStart;
	bool m_inclEnd;
};


TEST(RangeTest, test_int_includes_before)
{
	bool expected = false;
	Range r(2, 5, true, false);
	bool result = r.includes(1);

	ASSERT_EQ(result, expected);
}

TEST(RangeTest, test_int_includes_start_excl)
{
	bool expected = false;
	Range r(2, 5, false, false);
	bool result = r.includes(2);

	ASSERT_EQ(result, expected);
}

TEST(RangeTest, test_int_includes_start_incl)
{
	bool expected = true;
	Range r(2, 5, true, false);
	bool result = r.includes(2);

	ASSERT_EQ(result, expected);
}

TEST(RangeTest, test_int_includes_mid)
{
	bool expected = true;
	Range r(2, 5, true, false);
	bool result = r.includes(3);

	ASSERT_EQ(result, expected);
}

TEST(RangeTest, test_int_includes_end_incl)
{
	bool expected = true;
	Range r(2, 5, true, true);
	bool result = r.includes(5);

	ASSERT_EQ(result, expected);
}

TEST(RangeTest, test_int_includes_end_excl)
{
	bool expected = false;
	Range r(2, 5, true, false);
	bool result = r.includes(5);

	ASSERT_EQ(result, expected);
}

TEST(RangeTest, test_int_includes_after)
{
	bool expected = false;
	Range r(2, 5, true, false);
	bool result = r.includes(6);

	ASSERT_EQ(result, expected);
}
