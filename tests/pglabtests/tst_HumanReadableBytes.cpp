#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "PrintTo_Qt.h"

#include "utils/HumanReadableBytes.h"

TEST(HumanReadableBytesTest, bytes7)
{
    auto s = HumanReadableBytes(7);
    ASSERT_EQ("7 B", s);
}

TEST(HumanReadableBytesTest, bytes1023)
{
    auto s = HumanReadableBytes(1023);
    ASSERT_EQ("1023 B", s);
}

TEST(HumanReadableBytesTest, bytes1k)
{
    auto s = HumanReadableBytes(1024);
    ASSERT_EQ("1.00 kiB", s);
}
