﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "Expected.h"
#include "PrintTo_Qt.h"

using namespace testing;

Expected<int> getAnswerToEverything() { return 42; }

TEST(expected, valid_when_valid_returns_true)
{
	Expected<int> v = getAnswerToEverything();
	ASSERT_THAT(v.valid(), Eq(true));
}

TEST(expected, get_when_valid_returns_value)
{
	Expected<int> v = getAnswerToEverything();
	ASSERT_THAT(v.get(), Eq(42));
}

TEST(expected, hasException_when_valid_returns_false)
{
	Expected<int> v = getAnswerToEverything();
	ASSERT_THAT(v.hasException<std::exception>(), Eq(false));
}

TEST(expected, T_fromException_is_not_valid)
{
	auto e = Expected<int>::fromException(std::runtime_error("hello"));
	ASSERT_THAT(e.valid(), Eq(false));
}

TEST(expected, T_fromException_get_thows)
{
	auto e = Expected<int>::fromException(std::runtime_error("hello"));
	ASSERT_THROW (e.get(), std::runtime_error);
}

TEST(expected, T_fromException_has_exception_true)
{
	auto e = Expected<int>::fromException(std::runtime_error("hello"));
	ASSERT_THAT(e.hasException<std::runtime_error>(), Eq(true));
}

TEST(expected, T_fromException_has_exception_false)
{
	auto e = Expected<int>::fromException(std::runtime_error("hello"));
	ASSERT_THAT(e.hasException<std::logic_error>(), Eq(false));
}

TEST(expected, T_fromException_has_derived_exception)
{
	auto e = Expected<int>::fromException(std::runtime_error("hello"));
	ASSERT_THAT(e.hasException<std::exception>(), Eq(true));
}

TEST(expected, T_fromCode_is_valid)
{
	auto e = Expected<int>::fromCode([]() -> int { return 42; });
	ASSERT_THAT(e.valid(), Eq(true));
}

TEST(expected, T_fromCode_get)
{
	auto e = Expected<int>::fromCode([]() -> int { return 42; });
	ASSERT_THAT(e.get(), Eq(42));
}


TEST(expected, T_fromCode_E_is_not_valid)
{
	auto e = Expected<int>::fromCode([]() -> int { throw std::runtime_error("hello"); });
	ASSERT_THAT(e.valid(), Eq(false));
}

TEST(expected, T_fromCode_E_get_thows)
{
	auto e = Expected<int>::fromCode([]() -> int { throw std::runtime_error("hello"); });
	ASSERT_THROW (e.get(), std::runtime_error);
}

TEST(expected, T_fromCode_E_has_exception_true)
{
	auto e = Expected<int>::fromCode([]() -> int { throw std::runtime_error("hello"); });
	ASSERT_THAT(e.hasException<std::runtime_error>(), Eq(true));
}

TEST(expected, T_fromCode_E_has_exception_false)
{
	auto e = Expected<int>::fromCode([]() -> int { throw std::runtime_error("hello"); });
	ASSERT_THAT(e.hasException<std::logic_error>(), Eq(false));
}

TEST(expected, T_fromCode_E_has_derived_exception)
{
	auto e = Expected<int>::fromCode([]() -> int { throw std::runtime_error("hello"); });
	ASSERT_THAT(e.hasException<std::exception>(), Eq(true));
}

//Expected<int> getIntWithStdRuntimeError() { return Expected<void>(); }

Expected<void> getNothing() { return Expected<void>(); }


TEST(expected_void, valid_when_valid_returns_true)
{
	Expected<void> v = getNothing();
	ASSERT_THAT(v.valid(), Eq(true));
}

TEST(expected_void, get_when_valid_returns_value)
{
	Expected<void> v = getNothing();
	ASSERT_NO_THROW(v.get());
}

TEST(expected_void, hasException_when_valid_returns_false)
{
	Expected<void> v = getNothing();
	ASSERT_THAT(v.hasException<std::exception>(), Eq(false));
}






TEST(expected_void, void_fromException_is_not_valid)
{
	auto e = Expected<void>::fromException(std::runtime_error("hello"));
	ASSERT_THAT(e.valid(), Eq(false));
}

TEST(expected_void, void_fromException_get_thows)
{
	auto e = Expected<void>::fromException(std::runtime_error("hello"));
	ASSERT_THROW (e.get(), std::runtime_error);
}

TEST(expected_void, void_fromException_has_exception_true)
{
	auto e = Expected<void>::fromException(std::runtime_error("hello"));
	ASSERT_THAT(e.hasException<std::runtime_error>(), Eq(true));
}

TEST(expected_void, void_fromException_has_exception_false)
{
	auto e = Expected<void>::fromException(std::runtime_error("hello"));
	ASSERT_THAT(e.hasException<std::logic_error>(), Eq(false));
}

TEST(expected_void, void_fromException_has_derived_exception)
{
	auto e = Expected<void>::fromException(std::runtime_error("hello"));
	ASSERT_THAT(e.hasException<std::exception>(), Eq(true));
}

TEST(expected_void, void_fromCode_is_valid)
{
	auto e = Expected<void>::fromCode([]() -> void {  });
	ASSERT_THAT(e.valid(), Eq(true));
}

TEST(expected_void, void_fromCode_get)
{
	auto e = Expected<void>::fromCode([]() -> void {  });
	ASSERT_NO_THROW(e.get());
}

void expected_void_throws_func()
{
	throw std::runtime_error("hello");
}

TEST(expected_void, void_fromCode_E_is_not_valid)
{
	auto e = Expected<void>::fromCode(expected_void_throws_func);
	ASSERT_THAT(e.valid(), Eq(false));
}

TEST(expected_void, void_fromCode_E_get_thows)
{
	auto e = Expected<void>::fromCode(expected_void_throws_func);
	ASSERT_THROW (e.get(), std::runtime_error);
}

TEST(expected_void, void_fromCode_E_has_exception_true)
{
	auto e = Expected<void>::fromCode(expected_void_throws_func);
	ASSERT_THAT(e.hasException<std::runtime_error>(), Eq(true));
}

TEST(expected_void, void_fromCode_E_has_exception_false)
{
	auto e = Expected<void>::fromCode(expected_void_throws_func);
	ASSERT_THAT(e.hasException<std::logic_error>(), Eq(false));
}

TEST(expected_void, void_fromCode_E_has_derived_exception)
{
	auto e = Expected<void>::fromCode(expected_void_throws_func);
	ASSERT_THAT(e.hasException<std::exception>(), Eq(true));
}
