﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
//#include <QRegularExpression>
#include "catalog/PgType.h"
#include "catalog/PgTypeContainer.h"
//#include "PrintTo_Qt.h"
//#include "codebuilder/IndentationConfig.h"
#include "codebuilder/TypeMappings.h"
#include "Pgsql_oids.h"

using namespace testing;


class TypeMappingsTest : public ::testing::Test {
protected:
  void SetUp() override
  {
	  def_str_type = "std::string";
	  int4str = "int";
	  int8str = "long long";
	  tm = TypeMappings(nullptr, {
		  { Pgsql::int4_oid, int4str },
		  { Pgsql::int8_oid, int8str }
	  });
	  tm.setDefaultStringType(def_str_type);
	  tm.setDefaultContainerType("std::vector<%1>");
  }

  // void TearDown() override {}

  QString def_str_type;
  QString int4str;
  QString int8str;
  TypeMappings tm;
};

TEST_F(TypeMappingsTest, defStringType)
{
	QString result = tm.getTypeForOid(Pgsql::float4_oid).codeType();
	ASSERT_EQ(result, def_str_type);
}

TEST_F(TypeMappingsTest, int4Type)
{
	QString result = tm.getTypeForOid(Pgsql::int4_oid).codeType();
	ASSERT_EQ(result, int4str);
}

TEST_F(TypeMappingsTest, int4overideType)
{
	tm.set(Pgsql::int4_oid, "QString");
	QString result = tm.getTypeForOid(Pgsql::int4_oid).codeType();
	ASSERT_EQ(result, "QString");
}

// Need catalogue for the next test
// Maybe we should mock this !?
//TEST_F(TypeMappingsTest, int4arrayType)
//{
//	auto types= std::make_shared<PgTypeContainer>();
//	PgType int4arr;
//	int4arr.oid = Pgsql::int4_array_oid;
//	int4arr.elem = Pgsql::int4_oid;
//	types->add(int4arr);

//	tm.setTypes(types);

//	QString result = tm.getTypeForOid(Pgsql::int4_array_oid).codeType();
//	ASSERT_EQ(result, "std::vector<int>");
//}

