﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "Pgsql_oids.h"
#include "PrintTo_Qt.h"

using namespace testing;
using namespace Pgsql;

TEST(Pgsql_Oids, ElemOidFromArrayOid)
{
	Oid elem = ElemOidFromArrayOid(varchar_array_oid);
	ASSERT_EQ(elem, varchar_oid);
}

TEST(Pgsql_Oids, ArrayOidFromElemOid)
{
	Oid elem = ArrayOidFromElemOid(timestamptz_oid);
	ASSERT_EQ(elem, timestamptz_array_oid);
}

