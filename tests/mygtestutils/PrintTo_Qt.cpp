﻿#include "PrintTo_Qt.h"

void PrintTo(const QDateTime &dt, ::std::ostream* os) {
  *os << dt.toString(Qt::ISODateWithMs).toUtf8().toStdString();
}

void PrintTo(const QString &s, ::std::ostream* os) {
  *os << s.toUtf8().toStdString();
}
