﻿#ifndef PRINTTO_QT_H
#define PRINTTO_QT_H

#include <ostream>
#include <QDateTime>
#include <QString>

void PrintTo(const QDateTime &dt, ::std::ostream* os);
void PrintTo(const QString &s, ::std::ostream* os);

#endif // PRINTTO_QT_H
