﻿#include "Pgsql_Row.h"
#include "Pgsql_Result.h"

using namespace Pgsql;

Row::Row(const IResult &result, int row)
	: m_result(result)
	, m_row(row)
{}

bool Row::next()
{
	if (m_row < m_result.rows()) {
		++m_row;
		return true;
	}
	return false;
}

bool Row::operator==(const Row& rhs) const
{
	return &m_result == &rhs.m_result
			&& m_row == rhs.m_row;
}


Value Row::get(int col) const
{
	return m_result.get(col, m_row);
}

Row::const_iterator Row::end() const
{
	return const_iterator(*this, m_result.cols());
}
