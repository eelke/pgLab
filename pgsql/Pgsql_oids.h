﻿#ifndef PGSQL_OIDS_H
#define PGSQL_OIDS_H

#include <libpq-fe.h>
#include <cstdint>
#include <QDateTime>

namespace Pgsql {
	constexpr Oid bool_oid = 16;
	constexpr Oid bytea_oid = 17;
	constexpr Oid char_oid = 18;
	constexpr Oid name_oid = 19;
	constexpr Oid int8_oid = 20;
	constexpr Oid int2_oid = 21;
	constexpr Oid int4_oid = 23;
	constexpr Oid regproc_oid = 24;
	constexpr Oid text_oid = 25;
	constexpr Oid oid_oid = 26;
	constexpr Oid tid_oid = 27;
	constexpr Oid xid_oid = 28;
	constexpr Oid cid_oid = 29;
	constexpr Oid json_oid = 114;
	constexpr Oid xml_oid = 142;
	constexpr Oid point_oid = 600;
	constexpr Oid lseg_oid = 601;
	constexpr Oid path_oid = 602;
	constexpr Oid box_oid = 603;
	constexpr Oid polygon_oid = 604;
	constexpr Oid line_oid = 628;
	constexpr Oid cidr_oid = 650;
	constexpr Oid float4_oid = 700;
	constexpr Oid float8_oid = 701;
	constexpr Oid abstime_oid = 702;
	constexpr Oid reltime_oid = 703;
	constexpr Oid tinterval_oid = 704;
	constexpr Oid circle_oid = 718;
	constexpr Oid money_oid = 790;
	constexpr Oid macaddr_oid = 829;
	constexpr Oid inet_oid = 869;
	constexpr Oid aclitem_oid = 1033;
	constexpr Oid bpchar_oid = 1042;
	constexpr Oid varchar_oid = 1043;
	constexpr Oid date_oid = 1082;
	constexpr Oid time_oid = 1083;
	constexpr Oid timestamp_oid = 1114;
	constexpr Oid timestamptz_oid = 1184;
	constexpr Oid interval_oid = 1186;
	constexpr Oid timetz_oid = 1266;
	constexpr Oid bit_oid = 1560;
	constexpr Oid varbit_oid = 1562;
	constexpr Oid numeric_oid = 1700;
	constexpr Oid refcursor_oid = 1790;
	constexpr Oid regprocedure_oid = 2202;
	constexpr Oid regoper_oid = 2203;
	constexpr Oid regoperator_oid = 2204;
	constexpr Oid regclass_oid = 2205;
	constexpr Oid regtype_oid = 2206;
	constexpr Oid any_oid = 2276;
	constexpr Oid uuid_oid = 2950;
	constexpr Oid txid_snapshot_oid = 2970;
	constexpr Oid pg_lsn_oid = 3220;
	constexpr Oid tsvector_oid = 3614;
	constexpr Oid tsquery_oid = 3615;
	constexpr Oid gtsvector_oid = 3642;
	constexpr Oid regconfig_oid = 3734;
	constexpr Oid regdictionary_oid = 3769;
	constexpr Oid jsonb_oid = 3802;
	constexpr Oid int4range_oid = 3904;
	constexpr Oid numrange_oid = 3906;
	constexpr Oid tsrange_oid = 3908;
	constexpr Oid tstzrange_oid = 3910;
	constexpr Oid daterange_oid = 3912;
	constexpr Oid int8range_oid = 3926;
	constexpr Oid regnamespace_oid = 4089;
	constexpr Oid regrole_oid = 4096;

	constexpr Oid bool_array_oid = 1000;
	constexpr Oid bytea_array_oid = 1001;
	constexpr Oid char_array_oid = 1002;
	constexpr Oid name_array_oid = 1003;
	constexpr Oid int8_array_oid = 1016;
	constexpr Oid int2_array_oid = 1005;
	constexpr Oid int4_array_oid = 1007;
	constexpr Oid regproc_array_oid = 1008;
	constexpr Oid text_array_oid = 1009;
	constexpr Oid oid_array_oid = 1028;
	constexpr Oid tid_array_oid = 1010;
	constexpr Oid xid_array_oid = 1011;
	constexpr Oid cid_array_oid = 1012;
	constexpr Oid json_array_oid = 199;
	constexpr Oid xml_array_oid = 143;
	constexpr Oid point_array_oid = 1017;
	constexpr Oid lseg_array_oid = 1018;
	constexpr Oid path_array_oid = 1019;
	constexpr Oid box_array_oid = 1020;
	constexpr Oid polygon_array_oid = 1027;
	constexpr Oid line_array_oid = 629;
	constexpr Oid cidr_array_oid = 651;
	constexpr Oid float4_array_oid = 1021;
	constexpr Oid float8_array_oid = 1022;
	constexpr Oid abstime_array_oid = 1023;
	constexpr Oid reltime_array_oid = 1024;
	constexpr Oid tinterval_array_oid = 1025;
	constexpr Oid circle_array_oid = 719;
	constexpr Oid money_array_oid = 791;
	constexpr Oid macaddr_array_oid = 1040;
	constexpr Oid inet_array_oid = 1041;
	constexpr Oid aclitem_array_oid = 1034;
	constexpr Oid bpchar_array_oid = 1014;
	constexpr Oid varchar_array_oid = 1015;
	constexpr Oid date_array_oid = 1182;
	constexpr Oid time_array_oid = 1183;
	constexpr Oid timestamp_array_oid = 1115;
	constexpr Oid timestamptz_array_oid = 1185;
	constexpr Oid interval_array_oid = 1187;
	constexpr Oid timetz_array_oid = 1270;
	constexpr Oid bit_array_oid = 1561;
	constexpr Oid varbit_array_oid = 1563;
	constexpr Oid numeric_array_oid = 1231;
	constexpr Oid refcursor_array_oid = 2201;
	constexpr Oid regprocedure_array_oid = 2207;
	constexpr Oid regoper_array_oid = 2208;
	constexpr Oid regoperator_array_oid = 2209;
	constexpr Oid regclass_array_oid = 2210;
	constexpr Oid regtype_array_oid = 2211;
	constexpr Oid uuid_array_oid = 2951;
	constexpr Oid txid_snapshot_array_oid = 2949;
	constexpr Oid pg_lsn_array_oid = 3221;
	constexpr Oid tsvector_array_oid = 3643;
	constexpr Oid tsquery_array_oid = 3645;
	constexpr Oid gtsvector_array_oid = 3644;
	constexpr Oid regconfig_array_oid = 3735;
	constexpr Oid regdictionary_array_oid = 3770;
	constexpr Oid jsonb_array_oid = 3807;
	constexpr Oid int4range_array_oid = 3905;
	constexpr Oid numrange_array_oid = 3907;
	constexpr Oid tsrange_array_oid = 3909;
	constexpr Oid tstzrange_array_oid = 3911;
	constexpr Oid daterange_array_oid = 3913;
	constexpr Oid int8range_array_oid = 3927;
	constexpr Oid regnamespace_array_oid = 4090;
	constexpr Oid regrole_array_oid = 4097;

	/** If oid is an array oid then it returns the corresponding element oid.
	 *  It oid is not an array the functions returns InvalidOid.
	 * This function only works for the predefined postgresql types.
	 * If you need it to work for user defined types then use the system catalogue.
	 */
	Oid ElemOidFromArrayOid(Oid oid) noexcept;
	/** If oid is a non array oid then it returns the corresponding array oid.
	 *  It oid is a array oid or oid is unknown it returns InvalidOid.
	 * This function only works for the predefined postgresql types.
	 * If you need it to work for user defined types then use the system catalogue.
	 *
	 */
	Oid ArrayOidFromElemOid(Oid oid) noexcept;

	template <typename T>
	class OidFor {
	public:
		static Oid elem();
		static Oid array();
	};

	template <>
	class OidFor<char> {
	public:
		static Oid elem() { return char_oid; }
		static Oid array() { return char_array_oid; }
	};

	template <>
	class OidFor<int16_t> {
	public:
		static Oid elem() { return int2_oid; }
		static Oid array() { return int2_array_oid; }
	};

	template <>
	class OidFor<int32_t> {
	public:
		static Oid elem() { return int4_oid; }
		static Oid array() { return int4_array_oid; }
	};

	template <>
	class OidFor<unsigned int> {
	public:
		static Oid elem() { return oid_oid; }
		static Oid array() { return oid_array_oid; }
	};

	template <>
	class OidFor<QDateTime> {
	public:
		static Oid elem() { return timestamptz_oid; }
		static Oid array() { return timestamptz_array_oid; }
	};

	template <>
	class OidFor<QString> {
	public:
		static Oid elem() { return text_oid; }
		static Oid array() { return text_array_oid; }
	};
//	template <>
//	class OidFor<> {
//	public:
//		static Oid elem() { return _oid; }
//		static Oid array() { return _array_oid; }
//	};

} // end of namespace Pgsql

#endif // PGSQL_OIDS_H
