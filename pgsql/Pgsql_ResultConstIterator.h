#pragma once

#include "Pgsql_Row.h"

namespace Pgsql {

    class IResult;

    class ResultConstIterator {
    public:
        ResultConstIterator(const IResult &r, int rw)
            : m_row(r, rw)
        {}

        ResultConstIterator operator++()
        {
            ResultConstIterator t(*this);
            m_row.next();
            return t;
        }

        ResultConstIterator& operator++(int)
        {
            m_row.next();
            return *this;
        }

        bool operator==(const ResultConstIterator &rhs) const
        {
            return m_row == rhs.m_row;
        }

        bool operator!=(const ResultConstIterator &rhs) const
        {
            return !operator==(rhs);
        }

        const Row& operator*() const
        {
            return m_row;
        }
        const Row& operator->() const
        {
            return m_row;
        }

    private:
        Row m_row;
    };

}
