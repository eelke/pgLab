﻿#include "Pgsql_ErrorDetails.h"

namespace  {

	void set_stdstring_with_charptr(std::string &s, const char *p)
	{
		if (p) {
			s = p;
		}
		else {
			s.clear();
		}
	}

} // einde unnamed namespace

namespace Pgsql {

	ErrorDetails ErrorDetails::createErrorDetailsFromPGresult(const PGresult *result)
	{

		ErrorDetails r;
		set_stdstring_with_charptr(r.errorMessage,      PQresultErrorMessage(result));
		set_stdstring_with_charptr(r.state,             PQresultErrorField(result, PG_DIAG_SQLSTATE)); ///< PG_DIAG_SQLSTATE Error code as listed in	https://www.postgresql.org/docs/9.5/static/errcodes-appendix.html
		set_stdstring_with_charptr(r.severity,          PQresultErrorField(result, PG_DIAG_SEVERITY));
		set_stdstring_with_charptr(r.messagePrimary,    PQresultErrorField(result, PG_DIAG_MESSAGE_PRIMARY));
		set_stdstring_with_charptr(r.messageDetail,     PQresultErrorField(result, PG_DIAG_MESSAGE_DETAIL));
		set_stdstring_with_charptr(r.messageHint,       PQresultErrorField(result, PG_DIAG_MESSAGE_HINT));
		const char * p = PQresultErrorField(result, PG_DIAG_STATEMENT_POSITION);
		r.statementPosition = p != nullptr ? atoi(p) : -1; ///< First character is one, measured in characters not bytes!
		p = PQresultErrorField(result, PG_DIAG_INTERNAL_POSITION);
		r.internalPosition  = p != nullptr ? atoi(p) : -1;
		set_stdstring_with_charptr(r.internalQuery,     PQresultErrorField(result, PG_DIAG_INTERNAL_QUERY));
		set_stdstring_with_charptr(r.context,           PQresultErrorField(result, PG_DIAG_CONTEXT));
		set_stdstring_with_charptr(r.schemaName,        PQresultErrorField(result, PG_DIAG_SCHEMA_NAME));
		set_stdstring_with_charptr(r.tableName,         PQresultErrorField(result, PG_DIAG_TABLE_NAME));
		set_stdstring_with_charptr(r.columnName,        PQresultErrorField(result, PG_DIAG_COLUMN_NAME));
		set_stdstring_with_charptr(r.datatypeName,      PQresultErrorField(result, PG_DIAG_DATATYPE_NAME));
		set_stdstring_with_charptr(r.constraintName,    PQresultErrorField(result, PG_DIAG_CONSTRAINT_NAME));
		set_stdstring_with_charptr(r.sourceFile,        PQresultErrorField(result, PG_DIAG_SOURCE_FILE));
		set_stdstring_with_charptr(r.sourceLine,        PQresultErrorField(result, PG_DIAG_SOURCE_LINE));
		set_stdstring_with_charptr(r.sourceFunction,    PQresultErrorField(result, PG_DIAG_SOURCE_FUNCTION));
		return r;
	}

}
