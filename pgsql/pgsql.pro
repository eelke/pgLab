#-------------------------------------------------
#
# Project created by QtCreator 2016-11-11T09:17:13
#
#-------------------------------------------------

CONFIG   += staticlib
QT       += core

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets sql

TARGET = pgsql
TEMPLATE = lib

! include( ../common.pri ) {
error( "Couldn't find the common.pri file!" )
}

LIBS += -lws2_32 -llibpq

SOURCES += Pgsql_Connection.cpp \
	Pgsql_Params.cpp \
	Pgsql_PgException.cpp \
    Pgsql_Result.cpp \
    Pgsql_Row.cpp \
    Pgsql_Value.cpp \ 
    ArrayParser.cpp \
    Pgsql_oids.cpp \
    Pgsql_Transaction.cpp \
    Pgsql_ErrorDetails.cpp \
    Pgsql_Canceller.cpp

HEADERS  += Pgsql_Connection.h \
    Pgsql_IResult.h \
	Pgsql_Params.h \
	Pgsql_PgException.h \
    Pgsql_Result.h \
    Pgsql_ResultConstIterator.h \
    Pgsql_Row.h \
    Pgsql_Value.h \
	Pgsql_declare.h \
    Pgsql_Col.h \
    ArrayParser.h \
    Pgsql_oids.h \
    SqlGenerator.h \
    Pgsql_Transaction.h \
    Pgsql_ErrorDetails.h \
    Pgsql_Canceller.h


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../core/release/ -lcore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../core/debug/ -lcore

INCLUDEPATH += $$PWD/../core
DEPENDPATH += $$PWD/../core

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/libcore.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/libcore.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/core.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/core.lib
