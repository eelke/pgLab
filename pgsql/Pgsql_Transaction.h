﻿#ifndef PGSQL_TRANSACTION_H
#define PGSQL_TRANSACTION_H

#include <QString>
#include <string>
#include "Pgsql_Canceller.h"
#include <memory>

namespace Pgsql {

	class Result;
	class Params;
	class Connection;


	class Transaction {
	public:
		static Transaction startTransaction(Connection &conn);
		~Transaction();
		void rollback();
		void commit();
		Canceller getCancel();
		std::string getErrorMessage() const;
		Result query(const char * command);
		Result query(const QString &command);
		Result queryParam(const char * command, const Params &params);
		Result queryParam(const QString &command, const Params &params);
        void sendQuery(const char * query);
        void sendQuery(const std::string &command);
        void sendQuery(const QString &command);
        void sendQueryParams(const char * command, const Params &params);
		std::shared_ptr<Result> getResult();
		bool consumeInput();
		bool isBusy();
		std::string escapeLiteral(const std::string_view &literal);
		QString escapeLiteral(const QString &literal);
		std::string escapeIdentifier(const std::string_view &ident);
		QString escapeIdentifier(const QString &ident);
		QString getDBName() const;

	private:
		Connection *m_conn = nullptr;
		bool m_committed = false;
		bool m_rolledback = false;

		Transaction(Connection *conn)
			: m_conn(conn)
		{
		}
	};

}

#endif // PGSQL_TRANSACTION_H
