﻿#include "Pgsql_PgException.h"

namespace Pgsql {

	ResultCode::ResultCode(std::string result_code)
		: m_resultCode(std::move(result_code))
	{
		assert(m_resultCode.length() == 5);
	}

	std::string ResultCode::getClass() const
	{
		return m_resultCode.substr(1,2);
	}

	/** Helper to easily check the class of the error
	 *
	 */
	bool ResultCode::isClass(const std::string_view cls)
	{
		return m_resultCode.compare(1, 2, cls);
	}

	const std::string& ResultCode::getSpecific() const
	{
		return m_resultCode;
	}


	PgException::PgException(const char *msg)
		: std::runtime_error(msg)
	{}

	PgException::PgException(std::string msg)
		: std::runtime_error(msg)
	{}


	PgResultError::PgResultError(const Pgsql::ErrorDetails &details)
		: PgException(details.errorMessage)
		, m_details(details)
	{}

	ResultCode PgResultError::getResultCode() const
	{
		return ResultCode(m_details.state);
	}

	const Pgsql::ErrorDetails& PgResultError::details() const
	{
		return m_details;
	}

}
