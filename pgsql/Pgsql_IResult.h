#pragma once

#include "Pgsql_ResultConstIterator.h"

namespace Pgsql {

    class IResult {
    public:

        virtual operator bool() const = 0;

        virtual int tuplesAffected() const = 0;
        virtual int rows() const = 0;
        virtual int cols() const = 0;

        virtual const char* getColName(int idx) const = 0;

        virtual const char* val(int col, int row) const = 0;
        virtual Value get(int col, int row) const = 0;
        virtual Oid type(int col) const = 0;
        virtual bool null(int col, int row) const = 0;

        ResultConstIterator begin() const
        {
            return ResultConstIterator(*this, 0);
        }

        ResultConstIterator end() const
        {
            return ResultConstIterator(*this, rows());
        }
    };

}
