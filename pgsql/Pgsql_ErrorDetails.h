﻿#ifndef PGSQL_ERRORDETAILS_H
#define PGSQL_ERRORDETAILS_H

#include <string>
#include <libpq-fe.h>

namespace Pgsql {

	class ErrorDetails {
	public:
		static ErrorDetails createErrorDetailsFromPGresult(const PGresult *res);

		std::string errorMessage;
		std::string state; ///< PG_DIAG_SQLSTATE Error code as listed in	https://www.postgresql.org/docs/9.5/static/errcodes-appendix.html
		std::string severity;
		std::string messagePrimary;
		std::string messageDetail;
		std::string messageHint;
		int statementPosition; ///< First character is one, measured in characters not bytes!
		int internalPosition;
		std::string internalQuery;
		std::string context;
		std::string schemaName;
		std::string tableName;
		std::string columnName;
		std::string datatypeName;
		std::string constraintName;
		std::string sourceFile;
		std::string sourceLine;
		std::string sourceFunction;
	};

}

#endif // PGSQL_ERRORDETAILS_H
