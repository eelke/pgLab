﻿#ifndef PGEXCEPTION_H
#define PGEXCEPTION_H

#include "Pgsql_ErrorDetails.h"
#include <cassert>
#include <stdexcept>
#include <utility>

namespace Pgsql {

	class ResultCode {
	public:
		explicit ResultCode(std::string result_code);
		std::string getClass() const;
		bool isClass(const std::string_view cls);
		const std::string& getSpecific() const;
	private:
		std::string m_resultCode;
	};

	class PgException: public std::runtime_error {
    public:
		PgException(const char *msg);
		PgException(std::string msg);
    private:
    };
    
    class PgResultError: public PgException {
    public:
		PgResultError(const Pgsql::ErrorDetails &details);
		ResultCode getResultCode() const;
		const Pgsql::ErrorDetails& details() const;
	private:
		Pgsql::ErrorDetails m_details;
    };
    
	class PgConnectionError: public PgException {
	public:
		PgConnectionError(const std::string &msg)
			: PgException(msg)
		{}
	private:
        
	};
    
    
 
/* postgresq error classes
 *
 * success
 * warning
 * no data
 *
 * 03 sql statement not yet  complete
 * 08 connection exception -> could be resolved by reconnecting / resetting connection
 * 09 triggered action exception
 * 0A feature not supported
 * 0B invalid transaction initiation
 * 0F locator exception
 * 0L invalid grantor
 * 0P invalid role
 * 0Z diagnostic exception
 * 20 case not fount
 * 21 cardinality violation
 * 22 data exception
 * 23 integrity constraint error
 * 24 invalid cursor statement
 * 25 invalid transaction state
 * 26 invalid sql statement name
 * 27 triggered data change violation
 * 28 invalid authorization specification
 * 2B Dependent Privilege Descriptors Still Exist
 * 2D Invalid Transaction Termination
 * 2F SQL Routine Exception
 * 34 Invalid Cursor Name
 * 38 External Routine Exception
 * 39 External Routine Invocation Exception
 * 3B Savepoint Exception
 * 3D Invalid Catalog Name 
 * 3F Invalid Schema Name
 * 40 Transaction Rollback
 * 42 Syntax Error or Access Rule Violation
 * 44 WITH CHECK OPTION Violation
 * 53 Insufficient Resources
 * 54 Program Limit Exceeded
 * 55 Object Not In Prerequisite State
 * 57 Operator Intervention
 * 58 System Error (errors external to PostgreSQL itself)
 * 72 Snapshot Failure
 * F0 Configuration File Error
 * HV Foreign Data Wrapper Error (SQL/MED)
 * P0 PL/pgSQL Error
 * XX Internal Error
 * 
 * At what levels can the communication with postgresql fail
 * - network -> reset/reconnecting
 * - 
 */
}

#endif // PGEXCEPTION_H
