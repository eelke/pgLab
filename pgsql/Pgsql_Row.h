﻿#ifndef PGSQL_ROW_H
#define PGSQL_ROW_H

#include "Pgsql_Value.h"

namespace Pgsql {

    class IResult;

	/** \brief A reference to a specific row from a result.
	 *
	 * As it is a reference its contents won't be valid after its associated result has
	 * been destroyed.
	 */
	class Row {
	public:
		class const_iterator {
		public:
			const_iterator(const Row &r, int col)
				: m_row(r)
				, m_col(col)
			{}

			const_iterator operator++()
			{
				const_iterator t(*this);
				++m_col;
				return t;
			}

			const_iterator& operator++(int)
			{
				++m_col;
				return *this;
			}

            bool operator==(const const_iterator &rhs) const
			{
				return m_row == rhs.m_row && m_col == rhs.m_col;
			}

            bool operator!=(const const_iterator &rhs) const
			{
				return !operator==(rhs);
			}

			const Value operator*()
			{
				return m_row.get(m_col);
			}

			const Value operator->()
			{
				return m_row.get(m_col);
			}

		private:
			const Row &m_row;
			int m_col;
		};

        Row(const IResult &result, int row);
		bool next();

		bool operator==(const Row& rhs) const;
		Value get(int col) const;
		//Value get(const char *colname) const;
		//bool get(int col, QString &s);

		const_iterator begin() const
		{
			return const_iterator(*this, 0);
		}

		const_iterator end() const;

	private:
        const IResult& m_result;
		int m_row;
	};

} // end namespace Pgsql

#endif // PGSQL_ROW_H
