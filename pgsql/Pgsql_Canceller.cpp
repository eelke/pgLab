﻿#include "Pgsql_Canceller.h"

namespace Pgsql {

	Canceller::Canceller(PGcancel *c)
		: m_cancel(c)
	{}

	Canceller::Canceller(Canceller&& rhs)
		: m_cancel(rhs.m_cancel)
	{
		rhs.m_cancel = nullptr;
	}

	Canceller& Canceller::operator=(Canceller&& rhs)
	{
        if (m_cancel)
			PQfreeCancel(m_cancel);

        m_cancel = rhs.m_cancel;
		rhs.m_cancel = nullptr;
		return *this;
	}

	Canceller::~Canceller()
	{
        if (m_cancel)
			PQfreeCancel(m_cancel);
	}

	bool Canceller::cancel(std::string *error)
	{
		const int errbuf_size = 256;
		char errbuf[errbuf_size];
		bool res = PQcancel(m_cancel, errbuf, errbuf_size);
        if (!res && error)
			*error = errbuf;
		return res;
	}

}
