﻿#ifndef PGSQL_CANCELLER_H
#define PGSQL_CANCELLER_H

#include <string>
#include <libpq-fe.h>

namespace Pgsql {

	/** \brief Wrapper for a cancel object from libpq.
	 */
	class Canceller {
	public:
		Canceller() = default;
		Canceller(PGcancel *c);
		Canceller(const Canceller&) = delete;
		Canceller& operator=(const Canceller&) = delete;
		Canceller(Canceller&& rhs);
		Canceller& operator=(Canceller&& rhs);
		~Canceller();

		bool cancel(std::string *error);
	private:
		PGcancel *m_cancel = nullptr;
	};

}

#endif // PGSQL_CANCELLER_H
