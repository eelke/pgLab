﻿#ifndef PGSQL_DECLARE_H
#define PGSQL_DECLARE_H

#include <libpq-fe.h>

namespace Pgsql {

//	const Oid BOOLOID			= 16;
//	const Oid BYTEAOID		= 17;
//	const Oid CHAROID			= 18;
//	const Oid NAMEOID			= 19;
//	const Oid INT8OID			= 20;
//	const Oid INT2OID			= 21;
//	const Oid INT2VECTOROID	= 22;
//	const Oid INT4OID			= 23;
//	const Oid REGPROCOID		= 24;
//	const Oid TEXTOID			= 25;
//	const Oid OIDOID			= 26;
//	const Oid TIDOID		= 27;
//	const Oid XIDOID = 28;
//	const Oid CIDOID = 29;
//	const Oid OIDVECTOROID	= 30;
//	const Oid JSONOID = 114;
//	const Oid XMLOID = 142;
//	const Oid PGNODETREEOID	= 194;
//	const Oid PGDDLCOMMANDOID = 32;
//	const Oid POINTOID		= 600;
//	const Oid LSEGOID			= 601;
//	const Oid PATHOID			= 602;
//	const Oid BOXOID			= 603;
//	const Oid POLYGONOID		= 604;
//	const Oid LINEOID			= 628;
//	const Oid FLOAT4OID = 700;
//	const Oid FLOAT8OID = 701;
//	const Oid ABSTIMEOID		= 702;
//	const Oid RELTIMEOID		= 703;
//	const Oid TINTERVALOID	= 704;
//	const Oid UNKNOWNOID		= 705;
//	const Oid CIRCLEOID		= 718;
//	const Oid CASHOID = 790;
//	const Oid MACADDROID = 829;
//	const Oid INETOID = 869;
//	const Oid CIDROID = 650;
//	const Oid INT2ARRAYOID		= 1005;
//	const Oid INT4ARRAYOID		= 1007;
//	const Oid TEXTARRAYOID		= 1009;
//	const Oid OIDARRAYOID			= 1028;
//	const Oid FLOAT4ARRAYOID = 1021;
//	const Oid ACLITEMOID		= 1033;
//	const Oid CSTRINGARRAYOID		= 1263;
//	const Oid BPCHAROID		= 1042;
//	const Oid VARCHAROID		= 1043;
//	const Oid DATEOID			= 1082;
//	const Oid TIMEOID			= 1083;
//	const Oid TIMESTAMPOID	= 1114;
//	const Oid TIMESTAMPTZOID	= 1184;
//	const Oid INTERVALOID		= 1186;
//	const Oid TIMETZOID		= 1266;
//	const Oid BITOID	 = 1560;
//	const Oid VARBITOID	  = 1562;
//	const Oid NUMERICOID		= 1700;
//	const Oid REFCURSOROID	= 1790;
//	const Oid REGPROCEDUREOID = 2202;
//	const Oid REGOPEROID		= 2203;
//	const Oid REGOPERATOROID	= 2204;
//	const Oid REGCLASSOID		= 2205;
//	const Oid REGTYPEOID		= 2206;
//	const Oid REGROLEOID		= 4096;
//	const Oid REGNAMESPACEOID		= 4089;
//	const Oid REGTYPEARRAYOID = 2211;
//	const Oid UUIDOID = 2950;
//	const Oid LSNOID			= 3220;
//	const Oid TSVECTOROID		= 3614;
//	const Oid GTSVECTOROID	= 3642;
//	const Oid TSQUERYOID		= 3615;
//	const Oid REGCONFIGOID	= 3734;
//	const Oid REGDICTIONARYOID	= 3769;
//	const Oid JSONBOID = 3802;
//	const Oid INT4RANGEOID		= 3904;
//	const Oid RECORDOID		= 2249;
//	const Oid RECORDARRAYOID	= 2287;
//	const Oid CSTRINGOID		= 2275;
//	const Oid ANYOID			= 2276;
//	const Oid ANYARRAYOID		= 2277;
//	const Oid VOIDOID			= 2278;
//	const Oid TRIGGEROID		= 2279;
//	const Oid EVTTRIGGEROID		= 3838;
//	const Oid LANGUAGE_HANDLEROID		= 2280;
//	const Oid INTERNALOID		= 2281;
//	const Oid OPAQUEOID		= 2282;
//	const Oid ANYELEMENTOID	= 2283;
//	const Oid ANYNONARRAYOID	= 2776;
//	const Oid ANYENUMOID		= 3500;
//	const Oid FDW_HANDLEROID	= 3115;
//	const Oid TSM_HANDLEROID	= 3310;
//	const Oid ANYRANGEOID		= 3831;


	class Params;
	class Connection;
	class Result;
	class Value;
	class Row;

	enum class NullHandling {
		Ignore,
		Throw
	};


} // END namespace Pgsql

#endif // PGSQL_DECLARE_H
