! include( ./local.pri ) {
error( "Use local.pri.sample to create your own local.pri" )
}

LIBS += -lUser32 -lws2_32 -llibpq

CONFIG += c++20

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += BOOST_ENABLE_ASSERT_HANDLER
DEFINES += WIN32_LEAN_AND_MEAN NOMINMAX _WIN32_WINNT=0x0501
