TEMPLATE = subdirs

DEFINES += BOOST_ENABLE_ASSERT_HANDLER

SUBDIRS += core \
    pgsql \
    pglablib \
    pglab 

pglab.depends = core pgsql pglablib
pgsql.depends = core
pglablib.depends = core pgsql
tests.depends = core pgsql pglablib


CONFIG(debug, debug|release) {
SUBDIRS += tests
}
