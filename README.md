The program is build with Qt 6.3 using QtCreator and the Visual Studio 2019 C++ compiler.
Only 64-bits builds are actively maintained.

[Documentation](https://eelke.gitlab.io/pgLab/) is generated automatically when a commit is tagged. It also includes releasenotes.
The release notes use collected from git using [reno](https://docs.openstack.org/reno/latest/index.html).

## Dependencies

- boost
- botan
- fmt
- googletest
- libpq

