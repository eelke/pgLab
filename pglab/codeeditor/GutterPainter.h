#include <QPainter>
#include <QTextBlock>

#pragma once

class CodeEditor;
class QPaintEvent;

class GutterPainter
{
public:
    GutterPainter(CodeEditor *editor, QPaintEvent *event);

    void Paint();
private:
    CodeEditor *editor;
    QPainter painter;
    QPaintEvent *event;
    QFontMetrics fontMetrics;

    struct LoopState {
        CodeEditor *editor;
        QTextBlock block;
        int top;
        int bottom;

        LoopState(CodeEditor *editor);

        int blockNumber() const;

        void advanceToNextLine();
    };

    void drawLineNumber(const LoopState &loopState);

    void drawGutterErrorMarker(const LoopState &loopState);
};
