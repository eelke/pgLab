﻿#include "EditorGutter.h"
#include "CodeEditor.h"
#include "GutterPainter.h"

EditorGutter::EditorGutter(CodeEditor *editor)
	: QWidget(editor)
	, codeEditor(editor)
{}

QSize EditorGutter::sizeHint() const
{
	return QSize(codeEditor->gutterAreaWidth(), 0);
}

void EditorGutter::paintEvent(QPaintEvent *event)
{
    GutterPainter gutterpainter(codeEditor, event);
    gutterpainter.Paint();
}
