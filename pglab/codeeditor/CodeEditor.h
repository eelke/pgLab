﻿#ifndef CODEEDITOR_H
#define CODEEDITOR_H

#include <QPlainTextEdit>
#include <set>

/** This class adds some capabilities to QPlainTextEdit that are useful
 * in code editor scenarios.
 */
class CodeEditor : public QPlainTextEdit
{
	Q_OBJECT
public:
	explicit CodeEditor(QWidget *parent = nullptr);

	int gutterAreaWidth();

	void addErrorMarker(int position, int length);
	void clearErrorMarkers();
	void setFont(const QFont &);
	void setTabSize(int chars);
protected:
	void resizeEvent(QResizeEvent *event) override;
	void keyPressEvent(QKeyEvent *e) override;

signals:

public slots:

private slots:
	void updateGutterAreaWidth(int newBlockCount);
	void highlightCurrentLine();
	void updateGutterArea(const QRect &, int);
	void onTextChanged();


private:
	QWidget *gutterArea;

	QTextEdit::ExtraSelection currentLine;
	QList<QTextEdit::ExtraSelection> errorMarkers;

	std::set<int> errorLines;

	// Settings
	int m_tabSize = 0;  // tabSize in characters
	bool m_useTab = false;

    bool lineHasError(int blockNumber) const;
    void updateExtraSelections();
	bool indentSelection(bool indent);
    void insertIndentation(QTextCursor &cursor);
    void removeIndentation(QTextCursor &cursor);
    void makeSelection(QTextCursor &cursor, int first_pos, int end_block);

    friend class GutterPainter;
};

#endif // CODEEDITOR_H
