﻿#ifndef EDITORGUTTER_H
#define EDITORGUTTER_H

#include <QWidget>

class CodeEditor;

class EditorGutter : public QWidget
{
	Q_OBJECT
public:
	explicit EditorGutter(CodeEditor *editor);

	QSize sizeHint() const override;

protected:

	void paintEvent(QPaintEvent *event) override;

private:
	CodeEditor *codeEditor;
signals:

public slots:
};

#endif // EDITORGUTTER_H
