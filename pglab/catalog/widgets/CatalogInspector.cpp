﻿#include "CatalogInspector.h"
#include "OpenDatabase.h"
#include "UserConfiguration.h"
#include "catalog/widgets/CatalogFunctionsPage.h"
#include "catalog/widgets/CatalogNamespacePage.h"
#include "catalog/widgets/CatalogSequencesPage.h"
#include "catalog/widgets/CatalogTablesPage.h"
#include "catalog/widgets/CatalogTypesPage.h"

#include <QApplication>
#include <QTabWidget>
#include <QStringBuilder>
#include <QVBoxLayout>
#include <unordered_set>

CatalogInspector::CatalogInspector(std::shared_ptr<OpenDatabase> open_database, QWidget *parent)
	: QWidget(parent)
{
	m_tabWidget = new QTabWidget(this);
//	m_namespacePage = new CatalogNamespacePage(this);
    m_tablesPage = new CatalogTablesPage(open_database, this);
	m_functionsPage = new CatalogFunctionsPage(this);
	m_sequencesPage = new CatalogSequencesPage(this);
    m_typesPage = new CatalogTypesPage(this);

	auto layout = new QVBoxLayout(this);
	setLayout(layout);
	layout->addWidget(m_tabWidget);
//	m_tabWidget->addTab(m_namespacePage, "");
	m_tabWidget->addTab(m_tablesPage, "");
	m_tabWidget->addTab(m_functionsPage, "");
	m_tabWidget->addTab(m_sequencesPage, "");
    m_tabWidget->addTab(m_typesPage, "");

	setCatalog(open_database->catalog());
	retranslateUi(false);
}

void CatalogInspector::retranslateUi(bool all)
{
	m_tablesPage->retranslateUi(all);

//	m_tabWidget->setTabText(m_tabWidget->indexOf(m_namespacePage),
//		QApplication::translate("CatalogInspector", "Schemas", nullptr));
	m_tabWidget->setTabText(m_tabWidget->indexOf(m_tablesPage),
		QApplication::translate("CatalogInspector", "Tables", nullptr));
	m_tabWidget->setTabText(m_tabWidget->indexOf(m_functionsPage),
		QApplication::translate("CatalogInspector", "Functions", nullptr));
	m_tabWidget->setTabText(m_tabWidget->indexOf(m_sequencesPage),
		QApplication::translate("CatalogInspector", "Sequences", nullptr));
    m_tabWidget->setTabText(m_tabWidget->indexOf(m_typesPage),
        QApplication::translate("CatalogInspector", "Types", nullptr));
}

CatalogInspector::~CatalogInspector()
{
}

void CatalogInspector::setCatalog(std::shared_ptr<PgDatabaseCatalog> cat)
{
	m_catalog = cat;
//	m_namespacePage->setCatalog(cat);
	m_tablesPage->setCatalog(cat);
	m_functionsPage->setCatalog(cat);
	m_sequencesPage->setCatalog(cat);
    m_typesPage->setCatalog(cat);
}

void CatalogInspector::setNamespaceFilter(NamespaceFilter filter)
{
	m_tablesPage->setNamespaceFilter(filter);
	m_functionsPage->setNamespaceFilter(filter);
	m_sequencesPage->setNamespaceFilter(filter);
	QString hint = "Catalog instpector";
	QString caption = "Inspector";
	switch (filter) {
		case NamespaceFilter::PgCatalog:
			hint += " - pg_catalog";
			caption = "pg_catalog";
			break;
		case NamespaceFilter::InformationSchema:
			hint += " - information_schema";
			caption = "information_schema";
			break;
		default:
			break;
	}
//	context()->setCaption(this, caption, hint);
}

CatalogTablesPage *CatalogInspector::tablesPage()
{
	return m_tablesPage;
}
