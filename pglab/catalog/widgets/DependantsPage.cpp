﻿#include "catalog/widgets/DependantsPage.h"

#include "ResultTableModelUtil.h"
#include "UserConfiguration.h"
#include "catalog/PgClass.h"
#include "catalog/models/DependantsTableModel.h"
#include "CustomFilterSortModel.h"
#include "CustomDataRole.h"
#include "util/PgLabTableView.h"
#include <QStringBuilder>
#include <unordered_set>

DependantsPage::DependantsPage(QWidget *parent)
	: CatalogPageBase(parent)
{
	m_model = new DependantsTableModel(this);
	m_sortFilterProxy->setSourceModel(m_model);

//	connect(m_tableView->selectionModel(), &QItemSelectionModel::selectionChanged,
//			this, &DependentsPage::tableView_selectionChanged);
//	connect(m_model, &DependentsTableModel::modelReset, m_definitionView, &SqlCodePreview::clear);
}


void DependantsPage::catalogSet()
{
	m_model->setCatalog(m_catalog);
}


void DependantsPage::setFilter(const std::optional<PgClass> &cls)
{
	m_model->loadForTable(cls ? cls->oid() : InvalidOid);
	m_tableView->resizeColumnsToContents();
}


//void DependentsPage::tableView_selectionChanged(const QItemSelection &/*selected*/, const QItemSelection &/*deselected*/)
//{
//	auto rijen = selectedRows();

//	QString drops;
//	QString creates;
//	for (auto rij : rijen) {
//		auto&& t = m_model->trigger(rij);
//		drops += t.dropSql() % "\n";
//		creates += t.createSql() % "\n";

//		const PgProc *proc = m_catalog->procs()->getByKey(t.foid);
//		if (proc) {
//			creates += "\n" % proc->createSql() % "\n";
//		}
//	}
//	m_definitionView->setPlainText(drops % "\n" % creates);
//}
