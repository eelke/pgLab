﻿#ifndef DEPENDENTSPAGE_H
#define DEPENDENTSPAGE_H

#include "catalog/widgets/CatalogPageBase.h"

class PgClass;
class DependantsTableModel;
class QItemSelection;

class DependantsPage : public CatalogPageBase {
	Q_OBJECT
public:
	explicit DependantsPage(QWidget *parent = nullptr);

	void setFilter(const std::optional<PgClass> &cls);
signals:

public slots:

protected:
	void catalogSet() override;
private:
	DependantsTableModel *m_model = nullptr;

private slots:
//	void tableView_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
};


#endif // DEPENDENTSPAGE_H
