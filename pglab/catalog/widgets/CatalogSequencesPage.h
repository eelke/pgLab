﻿#ifndef SEQUENCESPAGES_H
#define SEQUENCESPAGES_H

#include "NamespaceFilter.h"
#include <QSplitter>
#include <memory>
#include <optional>

class PgLabTableView;
class PgDatabaseCatalog;
class SequenceModel;
class CustomFilterSortModel;
class SqlCodePreview;
class PgSequence;

class CatalogSequencesPage : public QSplitter {
	Q_OBJECT
public:
	CatalogSequencesPage(QWidget *parent = nullptr);

	void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);
	void setNamespaceFilter(NamespaceFilter filter);
public slots:

	void sequenceTable_currentRowChanged(const QModelIndex &current, const QModelIndex &previous);

private:
	PgLabTableView *m_sequenceTable = nullptr;
	//QTabWidget *m_detailTabs = nullptr;
	SqlCodePreview *m_definitionView = nullptr;
	SequenceModel *m_model = nullptr;
	CustomFilterSortModel *m_sortFilterProxy = nullptr;
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;

	void retranslateUi();
	void selectedSequenceChanged(const std::optional<PgSequence> &seq);
	void updateSqlTab(const std::optional<PgSequence> &seq);
};

#endif // SEQUENCESPAGES_H
