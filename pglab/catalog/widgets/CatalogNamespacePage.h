﻿#ifndef CATALOGNAMESPACEPAGE_H
#define CATALOGNAMESPACEPAGE_H

#include "catalog/widgets/CatalogPageBase.h"

class QTreeView;
class NamespaceItemModel;
class PgDatabaseCatalog;
class SqlCodePreview;


class CatalogNamespacePage : public QSplitter
{
	Q_OBJECT
public:
	explicit CatalogNamespacePage(QWidget *parent = nullptr);

	void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);
signals:

public slots:

protected:

private:
	QTreeView *m_namespaceTree = nullptr;
	SqlCodePreview *m_definitionView = nullptr;
	NamespaceItemModel *m_model = nullptr;
	CustomFilterSortModel *m_sortFilterProxy = nullptr;
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;

	void retranslateUi();

};

#endif // CATALOGNAMESPACEPAGE_H
