﻿#ifndef FUNCTIONSPAGE_H
#define FUNCTIONSPAGE_H

#include <QSplitter>
#include <memory>
#include <optional>
#include "NamespaceFilter.h"

class PgLabTableView;
class PgDatabaseCatalog;
class ProcTableModel;
class CustomFilterSortModel;
class QTabWidget;
class SqlCodePreview;
class PgProc;

class CatalogFunctionsPage : public QSplitter {
	Q_OBJECT
public:
	explicit CatalogFunctionsPage(QWidget *parent = nullptr);

	void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);
	void setNamespaceFilter(NamespaceFilter filter);
signals:

public slots:

	void functionTable_currentRowChanged(const QModelIndex &current, const QModelIndex &previous);
private:
	PgLabTableView *m_functionTable = nullptr;
	QTabWidget *m_detailTabs = nullptr;
	SqlCodePreview *m_definitionView = nullptr;
	ProcTableModel *m_model = nullptr;
	CustomFilterSortModel *m_sortFilterProxy = nullptr;
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;

	void retranslateUi();

	void selectedProcChanged(const std::optional<PgProc> &proc);
	void updateSqlTab(const std::optional<PgProc> &proc);
};

#endif // FUNCTIONSPAGE_H
