﻿#ifndef TABLESPAGE_H
#define TABLESPAGE_H

#include <QWidget>
#include <memory>
#include "NamespaceFilter.h"

class CatalogFunctionsPage;
class CatalogSequencesPage;
class CatalogTablesPage;
class CatalogNamespacePage;
class CatalogTypesPage;
class OpenDatabase;
class PgDatabaseCatalog;
class QTabWidget;

class CatalogInspector : public QWidget {
	Q_OBJECT
public:
	explicit CatalogInspector(std::shared_ptr<OpenDatabase> open_database, QWidget *parent = nullptr);
	~CatalogInspector();

	void setCatalog(std::shared_ptr<PgDatabaseCatalog> cat);
	void setNamespaceFilter(NamespaceFilter filter);

	CatalogTablesPage *tablesPage();
private:
	QTabWidget *m_tabWidget = nullptr;
//	CatalogNamespacePage *m_namespacePage = nullptr;
	CatalogTablesPage *m_tablesPage = nullptr;
	CatalogFunctionsPage *m_functionsPage = nullptr;
	CatalogSequencesPage *m_sequencesPage = nullptr;
    CatalogTypesPage *m_typesPage = nullptr;
	std::shared_ptr<PgDatabaseCatalog>  m_catalog;

	void retranslateUi(bool all = true);

private slots:
};

#endif // TABLESPAGE_H
