﻿#ifndef CATALOGTABLESPAGE_H
#define CATALOGTABLESPAGE_H

#include "NamespaceFilter.h"
#include "catalog/tables/TablesTableModel.h"
#include "util/PgLabTableViewHelper.h"
#include <QSplitter>
#include <memory>
#include <optional>
#include <QAbstractItemModel>


class CatalogConstraintPage;
class CatalogIndexPage;
class ColumnPage;
class ColumnTableModel;
class ConstraintModel;
class DependantsPage;
class PgClass;
class PgDatabaseCatalog;
class PgLabTableView;
class PropertiesPage;
class QTabWidget;
class SqlCodePreview;
class QSortFilterProxyModel;
class TriggerPage;


class CatalogTablesPage: public QSplitter {
	Q_OBJECT
public:
    explicit CatalogTablesPage(std::shared_ptr<OpenDatabase> opendatabase, QWidget * parent = nullptr);

	void setCatalog(std::shared_ptr<PgDatabaseCatalog> cat);
	void setNamespaceFilter(NamespaceFilter filter);

	void retranslateUi(bool all = true);

signals:
	void tableSelected(Oid tableoid);
private:
    PgLabTableViewHelper<TablesTableModel, QTreeView> m_tablesTableView;

	// Details
	QTabWidget *m_detailsTabs = nullptr;
	ColumnPage *m_columnsPage = nullptr;
	CatalogConstraintPage *m_constraintPage = nullptr;
	CatalogIndexPage *m_indexPage = nullptr;
//	PropertiesPage *m_propertiesPage = nullptr;
	TriggerPage *m_triggerPage = nullptr;
	DependantsPage *m_dependentsPage = nullptr;
	SqlCodePreview *m_tableSql = nullptr;

	std::shared_ptr<PgDatabaseCatalog>  m_catalog;

	void selectedTableChanged(const std::optional<PgClass> &table);
	void updateSqlTab(const std::optional<PgClass> &table);
private slots:

	void tableListTable_currentRowChanged(const QModelIndex &current, const QModelIndex &previous);
	void tableListTable_layoutChanged(const QList<QPersistentModelIndex> &parents, QAbstractItemModel::LayoutChangeHint hint);
	void on_tableListTable_doubleClicked(const QModelIndex &index);

};

#endif // CATALOGTABLESPAGE_H
