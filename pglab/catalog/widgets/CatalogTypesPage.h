#pragma once

#include <QSplitter>

class PgLabTableView;
class PgDatabaseCatalog;
class TypeModel;
class CustomFilterSortModel;
class SqlCodePreview;
class PgType;


class CatalogTypesPage : public QSplitter
{
    Q_OBJECT
public:
    CatalogTypesPage(QWidget *parent = nullptr);
    void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);
public slots:

    void typeTable_currentRowChanged(const QModelIndex &current, const QModelIndex &previous);
private:
    PgLabTableView *m_typeTable = nullptr;
    //QTabWidget *m_detailTabs = nullptr;
    SqlCodePreview *m_definitionView = nullptr;
    TypeModel *m_model = nullptr;
    CustomFilterSortModel *m_sortFilterProxy = nullptr;
    std::shared_ptr<const PgDatabaseCatalog> m_catalog;

    void retranslateUi();
    void selectedTypeChanged(const std::optional<PgType> &seq);
    void updateSqlTab(const std::optional<PgType> &seq);
};

