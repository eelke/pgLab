﻿#ifndef CATALOGPAGEBASE_H
#define CATALOGPAGEBASE_H

#include <QSplitter>
#include <memory>
#include <optional>
#include <unordered_set>

class PgDatabaseCatalog;
class PgLabTableView;
class SqlCodePreview;
class CustomFilterSortModel;


class CatalogPageBase : public QSplitter {
	Q_OBJECT
public:
	CatalogPageBase(QWidget *parent = nullptr);

	void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);

protected:
	PgLabTableView *m_tableView = nullptr;
	SqlCodePreview *m_definitionView = nullptr;
	CustomFilterSortModel *m_sortFilterProxy = nullptr;
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;

	virtual void catalogSet() {}
	std::unordered_set<int> selectedRows() const;
};

#endif // CATALOGPAGEBASE_H
