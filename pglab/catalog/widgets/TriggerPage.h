﻿#ifndef TRIGGERPAGE_H
#define TRIGGERPAGE_H

#include "catalog/widgets/CatalogPageBase.h"

class PgClass;
class TriggerTableModel;
class QItemSelection;

class TriggerPage : public CatalogPageBase {
	Q_OBJECT
public:
	explicit TriggerPage(QWidget *parent = nullptr);
//	TriggerPage(QWidget *parent = nullptr);

	void setFilter(const std::optional<PgClass> &cls);
signals:

public slots:

protected:
	void catalogSet() override;
private:
	TriggerTableModel *m_model = nullptr;

private slots:
	void tableView_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
};

#endif // TRIGGERPAGE_H
