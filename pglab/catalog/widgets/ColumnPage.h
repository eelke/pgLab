﻿#ifndef COLUMNPAGE_H
#define COLUMNPAGE_H

#include "catalog/PgClass.h"
#include <QSplitter>
#include <memory>
#include <optional>


class PgLabTableView;
class SqlCodePreview;
class PgDatabaseCatalog;
class ColumnTableModel;
class QSortFilterProxyModel;
class QItemSelection;
class QAbstractItemModel;

class ColumnPage : public QSplitter
{
	Q_OBJECT
public:
	explicit ColumnPage(QWidget *parent = nullptr);

	void setData(std::shared_ptr<const PgDatabaseCatalog> cat, const std::optional<PgClass> &cls);
	//void setFilter(const std::optional<PgClass> &cls);
signals:

public slots:

private:
	PgLabTableView *m_tableView = nullptr;
	SqlCodePreview *m_definitionView = nullptr;
	ColumnTableModel *m_columnModel = nullptr;
	QSortFilterProxyModel *m_sortFilterProxy = nullptr;
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;
	std::optional<PgClass> m_Class;

private slots:
	void tableView_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
};

#endif // COLUMNPAGE_H
