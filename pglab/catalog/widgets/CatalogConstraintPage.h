﻿#ifndef CATALOGCONSTRAINTPAGE_H
#define CATALOGCONSTRAINTPAGE_H

#include "CatalogPageBase.h"

class ConstraintModel;
class PgClass;
class QItemSelection;


class CatalogConstraintPage : public CatalogPageBase {
	Q_OBJECT
public:
	explicit CatalogConstraintPage(QWidget *parent = nullptr);

	void setFilter(const std::optional<PgClass> &cls);

protected:
	void catalogSet() override;

private:

	ConstraintModel *m_constraintModel = nullptr;

    QString generateSql(const std::unordered_set<int> &rows);
private slots:
	void tableView_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
};

#endif // CATALOGCONSTRAINTPAGE_H
