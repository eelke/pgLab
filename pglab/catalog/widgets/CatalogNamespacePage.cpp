﻿#include "CatalogNamespacePage.h"
#include <QTreeView>
#include "NamespaceItemModel.h"
#include "widgets/SqlCodePreview.h"
#include "catalog/PgDatabaseCatalog.h"

CatalogNamespacePage::CatalogNamespacePage(QWidget *parent)
	: QSplitter(Qt::Horizontal, parent)
	, m_namespaceTree(new QTreeView(this))
	, m_model(new NamespaceItemModel(this))
{
	m_namespaceTree->setModel(m_model);
	m_definitionView = new SqlCodePreview(this);

	addWidget(m_namespaceTree);
	addWidget(m_definitionView);

	retranslateUi();
}

void CatalogNamespacePage::setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat)
{
	m_catalog = cat;
	m_model->setEnableCheckboxes(false);
	m_model->init(cat->namespaces());
}

void CatalogNamespacePage::retranslateUi()
{

}

