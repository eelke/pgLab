﻿#include "catalog/widgets/CatalogPageBase.h"
#include "CustomFilterSortModel.h"
#include "util/PgLabTableView.h"
#include "widgets/SqlCodePreview.h"

CatalogPageBase::CatalogPageBase(QWidget *parent)
	: QSplitter(Qt::Vertical, parent)
{
	m_tableView = new PgLabTableView(this);
	m_definitionView = new SqlCodePreview(this);
	addWidget(m_tableView);
	addWidget(m_definitionView);

	m_sortFilterProxy = new CustomFilterSortModel(this);

	m_tableView->setModel(m_sortFilterProxy);
	m_tableView->setSortingEnabled(true);
	m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void CatalogPageBase::setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat)
{
	m_catalog = cat;
	m_definitionView->setCatalog(m_catalog);
	catalogSet();
}

std::unordered_set<int> CatalogPageBase::selectedRows() const
{
	auto&& indexes = m_tableView->selectionModel()->selectedIndexes();
	std::unordered_set<int> rijen;
	for (const auto &e : indexes)
		rijen.insert(m_sortFilterProxy->mapToSource(e).row());
	return rijen;
}
