﻿#ifndef CATALOGINDEXPAGE_H
#define CATALOGINDEXPAGE_H

#include "CatalogPageBase.h"

class IndexModel;
class PgClass;
class QItemSelection;


class CatalogIndexPage : public CatalogPageBase {
	Q_OBJECT
public:
	explicit CatalogIndexPage(QWidget *parent = nullptr);

	void setFilter(const std::optional<PgClass> &cls);

protected:
	void catalogSet() override;

private:

	IndexModel *m_indexModel = nullptr;

private slots:
	void tableView_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
};

#endif // CATALOGINDEXPAGE_H
