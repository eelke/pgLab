﻿#ifndef CONSTRAINTMODEL_H
#define CONSTRAINTMODEL_H

#include "BaseTableModel.h"
#include "catalog/PgClass.h"
#include "catalog/PgConstraint.h"
#include <QAbstractTableModel>
#include <optional>
#include <vector>

class PgDatabaseCatalog;

class ConstraintModel : public BaseTableModel {
	Q_OBJECT
public:

	enum e_Columns : int {
		TypeCol,
		NameCol, ///
		NsCol, ///
		SupportingIndexCol,
        InheritedCol,
//		DefinitionCol,
		colCount };

	explicit ConstraintModel(QObject *parent = nullptr);

	void setData(std::shared_ptr<const PgDatabaseCatalog> cat, const std::optional<PgClass> &table);


	// Header:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	//QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	const PgConstraint& constraint(int row);
protected:

	virtual Oid getType(int column) const override;
	virtual QVariant getData(const QModelIndex &index) const override;

private:
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;
	std::optional<PgClass> m_table;

	using t_Constraints = std::vector<PgConstraint>;
	t_Constraints m_constraints;
	QMetaObject::Connection refreshConnection;

private slots:
	void refresh();
};

#endif // CONSTRAINTMODEL_H
