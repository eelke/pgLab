﻿#ifndef PROCTABLEMODEL_H
#define PROCTABLEMODEL_H

#include "catalog/PgClass.h"
#include "catalog/PgProc.h"
#include "NamespaceFilter.h"
#include <QAbstractTableModel>
#include <memory>

class PgDatabaseCatalog;
class PgProcContainer;

/**
 * @brief The ProcTableModel class
 *
 * Hidden values:
 *
 */
class ProcTableModel: public QAbstractTableModel {
	Q_OBJECT
public:
	//using QAbstractTableModel::QAbstractTableModel;

	enum e_Columns : int {
		NameCol, //
		NamespaceCol, // Schema
		OwnerCol,
		LangCol,
		AclCol,

		colCount
	};

	ProcTableModel(QObject *parent = nullptr);

	void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);
	void setNamespaceFilter(NamespaceFilter filter);

	// Basic functionality:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role) const override;

	PgProc proc(int row) const;

private:
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;
	NamespaceFilter m_namespaceFilter = NamespaceFilter::User;
	std::vector<PgProc> m_procs;
	QMetaObject::Connection refreshConnection;

	Oid getType(int column) const;
	QVariant getData(const QModelIndex &index) const;
private slots:
	void refresh();
};


#endif // PROCTABLEMODEL_H
