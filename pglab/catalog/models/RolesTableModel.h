﻿#ifndef ROLESTABLEMODEL_H
#define ROLESTABLEMODEL_H


#include "BaseTableModel.h"
#include <memory>

class PgAuthId;
class PgAuthIdContainer;

/** Class for displaying the list of roles of a server in a QTableView
 *
 */
class RolesTableModel : public BaseTableModel {
	Q_OBJECT
public:
    using RowItem = PgAuthId;
	enum e_Columns : int { NameCol, SuperCol, InheritCol, CreateRoleCol,
						 CreateDBCol, CanLoginCol, ReplicationCol,
						 BypassRlsCol, ConnlimitCol, ValidUntilCol };



	explicit RolesTableModel(QObject *parent);

	void setRoleList(std::shared_ptr<const PgAuthIdContainer> roles);

	// Header:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	virtual Oid getType(int column) const override;
	virtual QVariant getData(const QModelIndex &index) const override;

//	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
	std::shared_ptr<const PgAuthIdContainer> m_roles;
};


#endif // ROLESTABLEMODEL_H
