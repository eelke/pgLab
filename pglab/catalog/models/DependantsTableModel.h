﻿#ifndef DEPENDENTSTABLEMODEL_H
#define DEPENDENTSTABLEMODEL_H

#include <QAbstractTableModel>
#include <memory>
#include "catalog/PgClass.h"
#include "Pgsql_oids.h"

#include <QVector>

class PgDatabaseCatalog;

class DependantsTableModel: public QAbstractTableModel {
	Q_OBJECT
public:

	enum e_Columns : int {
		NameCol, //
		NamespaceCol, // Schema
		ConstraintCol,

		colCount
	};

	DependantsTableModel(QObject *parent = nullptr);

	void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);
	void loadForTable(Oid table_id);

	// Basic functionality:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role) const override;

private:
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;
	QMetaObject::Connection refreshConnection;
	Oid tableId;

	class Item {
	public:
		Oid tableOid;
		QString tableName;
		QString namespaceName;
		QString constraintName;
	};

	QVector<Item> dependants;

private slots:
	void refresh();
};

#endif // DEPENDENTSTABLEMODEL_H
