﻿#ifndef INDEXMODEL_H
#define INDEXMODEL_H

#include <QAbstractTableModel>
#include "catalog/PgClass.h"
#include "catalog/PgIndex.h"
#include <memory>
#include <optional>
#include <vector>


class PgDatabaseCatalog;

class IndexModel: public QAbstractTableModel {
	Q_OBJECT
public:
	using QAbstractTableModel::QAbstractTableModel;

	enum e_Columns : int {
		TypeCol, /// primary/unique/normal
		NameCol, ///
		AmCol, ///< Access Method
		ColumnsCol, ///
		ConditionCol,
        SizeCol,
        ExplicitIndexCol,
		colCount };

// oid
// tablespace
// operator class
// unique
// primary
// clustered
// valid
// constraint
// system index
// fill factor
// comment

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	void setData(std::shared_ptr<const PgDatabaseCatalog> cat, const std::optional<PgClass> &table);


	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	virtual QVariant data(const QModelIndex &index, int role) const override;

	PgIndex getIndex(int row) const { return m_indexes[row]; }
protected:
	Oid getType(int column) const;
	QVariant getData(const QModelIndex &index) const;

private:
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;
	std::optional<PgClass> m_table;

	using t_Indexes = std::vector<PgIndex>;
	t_Indexes m_indexes;
	QMetaObject::Connection refreshConnection;

private slots:
	void refresh();
};

#endif // INDEXMODEL_H
