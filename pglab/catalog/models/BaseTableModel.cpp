﻿#include "BaseTableModel.h"
#include "CustomDataRole.h"
#include "ResultTableModelUtil.h"
#include <QBrush>
#include "Pgsql_oids.h"

using namespace Pgsql;

QVariant BaseTableModel::data(const QModelIndex &index, int role) const
{
	QVariant v;
	Oid oid = getType(index.column());
	if (role == Qt::DisplayRole) {
		v = getData(index);
	}
    else if (role == CustomDataTypeRole) {
        v = oid;
    }
    else if (role == CustomDataMeaningRole) {
        v = getDataMeaning(index);
    }
    return v;
}

QVariant BaseTableModel::getDataMeaning(const QModelIndex &) const
{
    return static_cast<int>(DataMeaning::Normal);
}
