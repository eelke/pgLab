﻿#include "IndexModel.h"
#include "catalog/PgDatabaseCatalog.h"
#include "catalog/PgIndexContainer.h"
#include "Pgsql_oids.h"
#include "ScopeGuard.h"
#include "CustomDataRole.h"

void IndexModel::setData(std::shared_ptr<const PgDatabaseCatalog> cat, const std::optional<PgClass> &table)
{
	if (cat != m_catalog) {
		m_catalog = cat;
		refreshConnection = connect(m_catalog.get(), &PgDatabaseCatalog::refreshed, this, &IndexModel::refresh);
	}
	m_table = table;
	refresh();
}

void IndexModel::refresh()
{
	beginResetModel();
	SCOPE_EXIT { endResetModel(); };

	if (m_table)
		m_indexes = m_catalog->indexes()->getIndexesForTable(m_table->oid());
	else
		m_indexes.clear();
}

int IndexModel::rowCount(const QModelIndex &/*parent*/) const
{
	return (int)m_indexes.size();
}

int IndexModel::columnCount(const QModelIndex &/*parent*/) const
{
	return colCount;
}

Oid IndexModel::getType(int column) const
{
    switch (column) {
    case SizeCol:
        return Pgsql::int8_oid;
    case ExplicitIndexCol:
        return Pgsql::bool_oid;
    default:
        return Pgsql::varchar_oid;
    }
}

QVariant IndexModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant v;
	if (orientation == Qt::Horizontal) {
		if (role == Qt::DisplayRole) {
			QString c;
			switch (section) {
			case TypeCol:
				c = tr("Type");
				break;
			case NameCol:
				c = tr("Name");
				break;
			case AmCol:
				c = tr("AM");
				break;
			case ColumnsCol:
				c = tr("On");
				break;
			case ConditionCol:
				c = tr("Condition");
				break;
            case SizeCol:
                c = tr("Size");
                break;
            case ExplicitIndexCol:
                c = tr("Explicit");
                break;
//			case DefinitionCol:
//				c = tr("Definition");
//				break;
			}
			v = c;
		}
	}
	return v;

}

QVariant IndexModel::getData(const QModelIndex &index) const
{
	QVariant v;
	int rij = index.row();
	const auto &dat = m_indexes[rij];
	switch (index.column()) {
	case TypeCol:
		if (dat.isprimary) v = ":/icons/constraints/primarykey.png";
		else if (dat.isunique) v = ":/icons/constraints/unique.png";
		else v = ":/icons/constraints/index.png";
		break;

	case NameCol:
		v = dat.objectName(); // getIndexDisplayString(*m_catalog, dat.indexrelid);
		break;

	case AmCol:
		v = dat.getAm();
		break;

	case ColumnsCol:
		break;

	case ConditionCol:
		break;

    case SizeCol:
        v = dat.sizeBytes;
        break;
    case ExplicitIndexCol:
        v = !dat.isSupportingIndex();
        break;
	}
	return v;
}

QVariant IndexModel::data(const QModelIndex &index, int role) const
{
	QVariant v;
	if (role == Qt::DisplayRole)
		v = getData(index);
	else if (role == CustomDataTypeRole)
		v = getType(index.column());
    else if (role == CustomDataMeaningRole) {
        switch (index.column()) {
        case SizeCol:
            return static_cast<int>(DataMeaning::Bytes);
        default:
            return static_cast<int>(DataMeaning::Normal);
        }
    }
    return v;
}
