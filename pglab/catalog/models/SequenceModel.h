﻿#ifndef SEQUENCEMODEL_H
#define SEQUENCEMODEL_H

#include "NamespaceFilter.h"
#include <QAbstractTableModel>
#include "catalog/PgSequence.h"
#include <memory>

class PgDatabaseCatalog;
class PgSequenceContainer;

class SequenceModel: public QAbstractTableModel {
	Q_OBJECT
public:

	enum e_Columns : int {
		NameCol,
		SchemaCol,
		OwnerCol,
		LastCol,
		StartCol,
		MinCol,
		MaxCol,
		IncrementCol,
		CacheCol,
		CycledCol,
		CalledCol,
		AclCol,

		colCount
	};

	SequenceModel(QObject * parent = nullptr);

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);
    void setNamespaceFilter(NamespaceFilter filter);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role) const override;

	PgSequence sequence(int row) const;
private:
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;
	std::vector<PgSequence> m_sequences;
	NamespaceFilter m_namespaceFilter = NamespaceFilter::User;
	QMetaObject::Connection refreshConnection;

	void refresh();
};

#endif // SEQUENCEMODEL_H
