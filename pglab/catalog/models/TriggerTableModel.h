﻿#ifndef TRIGGERTABLEMODEL_H
#define TRIGGERTABLEMODEL_H

#include <QAbstractTableModel>
#include "catalog/PgClass.h"
#include "catalog/PgTrigger.h"
#include <memory>

class PgDatabaseCatalog;
class PgTriggerContainer;

/**
 * @brief The TriggerTableModel class
 *
 * Hidden values:
 * - FirstHiddenValue: oid of table the trigger belongs to
 */
class TriggerTableModel: public QAbstractTableModel {
	Q_OBJECT
public:
	//using QAbstractTableModel::QAbstractTableModel;

	enum e_Columns : int {
		NameCol, //
		EventsCol, // Insert, Update, Delete Truncate
		WhenCol, // before after instead of
		DeferrableCol,
		InitiallyCol,
		ForCol,
		CondCol,
		//ReferencingCol,
		ProcedureCol,
		colCount
	};

	TriggerTableModel(QObject *parent = nullptr);

//	CREATE [ CONSTRAINT ] TRIGGER name { BEFORE | AFTER | INSTEAD OF } { event [ OR ... ] }
//		ON table_name
//		[ FROM referenced_table_name ]
//		[ NOT DEFERRABLE | [ DEFERRABLE ] [ INITIALLY IMMEDIATE | INITIALLY DEFERRED ] ]
//		[ REFERENCING { { OLD | NEW } TABLE [ AS ] transition_relation_name } [ ... ] ]
//		[ FOR [ EACH ] { ROW | STATEMENT } ]
//		[ WHEN ( condition ) ]
//		EXECUTE PROCEDURE function_name ( arguments )

//	where event can be one of:

//		INSERT
//		UPDATE [ OF column_name [, ... ] ]
//		DELETE
//		TRUNCATE

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);


	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	virtual QVariant data(const QModelIndex &index, int role) const override;

	PgTrigger trigger(int row) const;

private:
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;
	std::shared_ptr<const PgTriggerContainer> m_triggers;
	QMetaObject::Connection refreshConnection;

	Oid getType(int column) const;
	QVariant getData(const QModelIndex &index) const;

private slots:
	void refresh();
};


#endif // TRIGGERTABLEMODEL_H
