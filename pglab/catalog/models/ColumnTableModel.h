﻿#ifndef COLUMNTABLEMODEL_H
#define COLUMNTABLEMODEL_H

#include "catalog/models/BaseTableModel.h"
#include "catalog/PgAttribute.h"
#include "catalog/PgDatabaseCatalog.h"
#include "catalog/PgClass.h"
#include "catalog/PgIndex.h"
#include <memory>
#include <optional>
#include <vector>

class PgDatabaseCatalog;
class PgAttribute;

class ColumnTableModel: public BaseTableModel {
	Q_OBJECT
public:
	enum e_Columns : int {
		AttnumCol,
		NameCol, ///
		TypeCol,
		NullCol,
		DefaultCol,
		ForeignKeyCol,
		CollationCol,
        CommentCol,

		colCount };


	using BaseTableModel::BaseTableModel;
	void setData(std::shared_ptr<const PgDatabaseCatalog> cat, const std::optional<PgClass> &table);

	// Header:
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

	// Basic functionality:
	int rowCount(const QModelIndex &parent) const override;
	int columnCount(const QModelIndex &parent) const override;

	QVariant data(const QModelIndex &index, int role) const override;
	const PgAttribute& column(int row) const;
protected:
	virtual Oid getType(int column) const override;
	virtual QVariant getData(const QModelIndex &index) const override;

	using t_Columns = std::vector<PgAttribute>;

	std::shared_ptr<const PgDatabaseCatalog> m_catalog;
	std::optional<PgClass> m_table;
	t_Columns m_columns;
	std::vector<PgIndex> m_indexes;

	QMetaObject::Connection refreshConnection;

	QString getFKey(const PgAttribute &column) const;
    QString getDefaultString(const PgAttribute &column) const;

private slots:
	/// Retrieves required data from catalog, called everytime it might have changed
	void refresh();
};

#endif // COLUMNTABLEMODEL_H
