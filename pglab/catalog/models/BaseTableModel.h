﻿#ifndef BASETABLEMODEL_H
#define BASETABLEMODEL_H

#include <QAbstractTableModel>
#include "Pgsql_declare.h"

class BaseTableModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	using QAbstractTableModel::QAbstractTableModel;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

protected:
	virtual Oid getType(int column) const = 0;
	virtual QVariant getData(const QModelIndex &index) const = 0;
    virtual QVariant getDataMeaning(const QModelIndex &index) const;
};


#endif // BASETABLEMODEL_H
