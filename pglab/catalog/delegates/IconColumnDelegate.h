﻿#ifndef ICONCOLUMNDELEGATE_H
#define ICONCOLUMNDELEGATE_H

#include <QStyledItemDelegate>
#include <map>

/** This class asumes that the string values supplies for the column data
 * can be Resolved by QIcon to an actual icon. Icons are cached and reused.
 *
 */
class IconColumnDelegate: public QStyledItemDelegate {
public:
	IconColumnDelegate(QWidget *parent = nullptr);
	~IconColumnDelegate() override;

	void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	QSize sizeHint(const QStyleOptionViewItem &option,
					   const QModelIndex &index) const override;

	void clearCache();
private:
	using t_IconCache = std::map<QString, QIcon*>;

	mutable t_IconCache m_Icons;

	QIcon* getIcon(const QString &name) const;
};

#endif // ICONCOLUMNDELEGATE_H
