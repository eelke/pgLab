﻿#include "catalog/delegates/IconColumnDelegate.h"
#include <QPainter>

IconColumnDelegate::IconColumnDelegate(QWidget *parent)
	: QStyledItemDelegate(parent)
{
}

IconColumnDelegate::~IconColumnDelegate()
{
	clearCache();
}

void IconColumnDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
						 const QModelIndex &index) const
{
	if (index.data().canConvert<QString>()) {
		auto icon_name = qvariant_cast<QString>(index.data());


		QIcon* icon = IconColumnDelegate::getIcon(icon_name);
		if (icon) {
			if (option.state & QStyle::State_Selected)
				painter->fillRect(option.rect, option.palette.highlight());
			icon->paint(painter, option.rect);
		}
		else
			QStyledItemDelegate::paint(painter, option, index);

	} else {
		QStyledItemDelegate::paint(painter, option, index);
	}
}

QSize IconColumnDelegate::sizeHint(const QStyleOptionViewItem &,
				   const QModelIndex &) const
{
	return QSize(16, 16);
}


void IconColumnDelegate::clearCache()
{
	for (auto &e : m_Icons)
		delete e.second;
	m_Icons.clear();
}

QIcon* IconColumnDelegate::getIcon(const QString &name) const
{
	auto fr = m_Icons.find(name);
	if (fr == m_Icons.end()) {
		// load and insert icon
		auto icon = new QIcon(name);
		fr = m_Icons.emplace(name, icon).first;
	}
	return fr->second;
}
