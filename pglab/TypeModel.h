#ifndef TYPEMODEL_H
#define TYPEMODEL_H

#include <QAbstractTableModel>
#include "catalog/PgType.h"

class TypeModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum e_Columns : int {
        NameCol,
        SchemaCol,
        OwnerCol,

        colCount
    };

    TypeModel(QObject * parent = nullptr);

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    void setCatalog(std::shared_ptr<const PgDatabaseCatalog> cat);
    //void setNamespaceFilter(NamespaceFilter filter);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    PgType typ(int row) const;

private:
    std::shared_ptr<const PgDatabaseCatalog> m_catalog;
    std::vector<PgType> m_types;
    //NamespaceFilter m_namespaceFilter = NamespaceFilter::User;
    QMetaObject::Connection refreshConnection;

    void refresh();
};

#endif
