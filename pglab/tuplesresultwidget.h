﻿#ifndef TUPLESRESULTWIDGET_H
#define TUPLESRESULTWIDGET_H

#include "querytool/QueryResultModel.h"
#include <QWidget>

namespace Ui {
class TuplesResultWidget;
}

class TuplesResultWidget : public QWidget
{
	Q_OBJECT

public:
	explicit TuplesResultWidget(QWidget *parent = 0);
	~TuplesResultWidget();

	void setResult(std::shared_ptr<QueryResultModel> res, float ms);
	void exportData(const QString &file_name) const;

	std::shared_ptr<const Pgsql::Result> GetPgsqlResult() const { return resultModel->GetPgsqlResult(); }
private:
	Ui::TuplesResultWidget *ui;

	std::shared_ptr<QueryResultModel> resultModel;
};

#endif // TUPLESRESULTWIDGET_H
