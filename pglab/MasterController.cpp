﻿#include "MasterController.h"
#include "ConnectionController.h"
#include <QDebug>
#include <QDir>
#include <QStandardPaths>

namespace {

	QString GetUserConfigDatabaseName()
	{
		QString path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
		QDir dir(path);
		if (!dir.exists()) {
			dir.mkpath(".");
		}
		path += "/pglabuser.db";
		return path;
	}

}

MasterController::MasterController(QObject *parent) : QObject(parent)
{}

MasterController::~MasterController()
{}

void MasterController::init()
{
	m_userConfigDatabase = QSqlDatabase::addDatabase("QSQLITE");
	m_userConfigDatabase.setDatabaseName(GetUserConfigDatabaseName());

	if (!m_userConfigDatabase.open()) {
		qDebug() << "Error: connection with database fail";
	}
	else {
		qDebug() << "Database: connection ok";
	}

	m_connectionController = new ConnectionController(this);
	m_connectionController->init();
}

ConnectionController *MasterController::connectionController()
{
	return m_connectionController;
}

QSqlDatabase& MasterController::userConfigDatabase()
{
	return m_userConfigDatabase;
}


