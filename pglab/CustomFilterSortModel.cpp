﻿#include "CustomFilterSortModel.h"

CustomFilterSortModel::CustomFilterSortModel(QObject *parent)
	: QSortFilterProxyModel(parent)
{
}


void CustomFilterSortModel::setOidFilterTable(Oid tbl_oid, int role)
{
	m_filterOid = tbl_oid;
	m_filterRole = role;
	invalidateFilter();
}


bool CustomFilterSortModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
	bool result = true;
	if (m_filterOid != InvalidOid) {
		auto&& source = sourceModel();
		QModelIndex index0 = source->index(sourceRow, 0, sourceParent);
		auto&& v = source->data(index0, m_filterRole);
		result = v.toUInt() == m_filterOid;
	}
	return result && QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);
}
