﻿#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <QTimer>
#include <QElapsedTimer>

class QLabel;

class StopWatch : public QObject {
	Q_OBJECT
public:
	StopWatch();

	void start();
	qint64 elapsed();
	void stop();

	void setOutputLabel(QLabel *label);
private:
	QElapsedTimer m_elapsed; // = nullptr; ///< Keeps time
	QTimer m_timer; ///< triggers updates
	QLabel* m_output = nullptr;
	qint64 m_timeTaken = 0LL;


private slots:
	void updateTimer();
};

#endif // STOPWATCH_H
