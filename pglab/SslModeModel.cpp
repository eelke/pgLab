﻿#include "SslModeModel.h"

SslModeModel::SslModeModel(QObject *parent)
	: QAbstractListModel(parent)
{}

int SslModeModel::rowCount(const QModelIndex &) const
{
	return 6;
}

int SslModeModel::columnCount(const QModelIndex &) const
{
	return ColCount;
}

QVariant SslModeModel::data(const QModelIndex &index, int role) const
{
	QVariant v;
	if (role == Qt::DisplayRole) {
		switch (index.column()) {
		case Name:
			switch(index.row()) {
			case 0:
				v = tr("disable");
				break;
			case 1:
				v = tr("allow");
				break;
			case 2:
				v = tr("prefer");
				break;
			case 3:
				v = tr("require");
				break;
			case 4:
				v = tr("verify_ca");
				break;
			case 5:
				v = tr("verify_full");
				break;
			}
			break;
		case Description:
			switch(index.row()) {
			case 0:
				v = tr("try a non encrypted connection only");
				break;
			case 1:
				v = tr("try no encryption first then try encrypted");
				break;
			case 2:
				v = tr("try encrypted first then not encrypted");
				break;
			case 3:
				v = tr("require an encrypted connection");
				break;
			case 4:
				v = tr("verify encryption certificate has a valid signature");
				break;
			case 5:
				v = tr("verify encryption certificate has a valid signature and matches the host");
				break;
			}
			break;
		}
	}
	return v;
}
