﻿#ifndef CODEGENERATOR_H
#define CODEGENERATOR_H

#include <QWidget>
#include "Pgsql_declare.h"

namespace Ui {
class CodeGenerator;
}

class PgDatabaseCatalog;
class CodeGeneratorViewModel;

class CodeGenerator : public QWidget {
	Q_OBJECT
public:
	CodeGenerator(QWidget *parent = nullptr);
	~CodeGenerator();

	void Init(std::shared_ptr<PgDatabaseCatalog> catalog, QString query, std::shared_ptr<const Pgsql::Result> dbres);
private slots:
	void on_updateCodeButton_clicked();

private:
	Ui::CodeGenerator *ui;

    CodeGeneratorViewModel *Model;

	std::shared_ptr<PgDatabaseCatalog> m_catalog;
	QString m_query;
	std::shared_ptr<const Pgsql::Result> m_dbres;

	void generateCode();
};

#endif // CODEGENERATOR_H
