﻿#ifndef PGDUMPOUTPUTHIGHLIGHTER_H
#define PGDUMPOUTPUTHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextFormat>

class PgDumpOutputHighlighter : public QSyntaxHighlighter
{
	Q_OBJECT
public:
	PgDumpOutputHighlighter(QTextDocument *parent = nullptr);
	~PgDumpOutputHighlighter() override;
protected:
	void highlightBlock(const QString &text) override;

private:
	QTextCharFormat m_errorFormat;
};

#endif // PGDUMPOUTPUTHIGHLIGHTER_H
