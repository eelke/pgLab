﻿#include "PgDumpOutputHighlighter.h"

PgDumpOutputHighlighter::PgDumpOutputHighlighter(QTextDocument *parent)
	: QSyntaxHighlighter(parent)
{
	m_errorFormat.setForeground(QColor(255, 128, 128));
	m_errorFormat.setFontWeight(QFont::Bold);
}

PgDumpOutputHighlighter::~PgDumpOutputHighlighter()
{

}

void PgDumpOutputHighlighter::highlightBlock(const QString &text)
{
	if (text.startsWith("pg_dump: error"))
		setFormat(0, text.length(), m_errorFormat);
}
