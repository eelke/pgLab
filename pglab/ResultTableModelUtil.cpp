﻿#include "ResultTableModelUtil.h"
#include "Pgsql_oids.h"
#include <QTableView>
#include <QHeaderView>

using namespace Pgsql;

Qt::Alignment GetDefaultAlignmentForType(Oid o)
{

    Qt::Alignment r;
	switch (o) {
	case int2_oid:
	case int4_oid:
	case int8_oid:
	case float4_oid:
	case float8_oid:
	case numeric_oid:
	case oid_oid:
		r = GetDefaultNumberAlignment();
		break;
	case bool_oid:
		r = GetDefaultBoolAlignment(); // Qt::AlignCenter;
		break;
	default:
		r = GetDefaultAlignment();
	}
	return r;
}

QColor GetDefaultColorForType(Oid o)
{
	QColor c;
	switch (o) {
	case int2_oid:
	case int4_oid:
	case int8_oid:
		c = GetDefaultIntegerColor();
		break;
	case float4_oid:
	case float8_oid:
		c = GetDefaultFloatColor();
		break;
	case numeric_oid:
		c = GetDefaultNumericColor();
		break;
	case oid_oid:
	case bool_oid:
	default:
		c = Qt::black;
	}
	return c;
}

QString FormatBoolForDisplay(bool v)
{
    static QString t(QChar(0x2713));
    static QString f(QChar(0x2717));

    return v ? t : f;
}


//<widget class="QTableView" name="ResultView">
// <property name="font">
//  <font>
//   <family>Source Sans Pro</family>
//   <pointsize>10</pointsize>
//  </font>
// </property>
// <property name="editTriggers">
//  <set>QAbstractItemView::NoEditTriggers</set>
// </property>
// <property name="verticalScrollMode">
//  <enum>QAbstractItemView::ScrollPerPixel</enum>
// </property>
// <property name="horizontalScrollMode">
//  <enum>QAbstractItemView::ScrollPerPixel</enum>
// </property>
// <property name="wordWrap">
//  <bool>false</bool>
// </property>
// <attribute name="verticalHeaderDefaultSectionSize">
//  <number>20</number>
// </attribute>
// <attribute name="verticalHeaderMinimumSectionSize">
//  <number>16</number>
// </attribute>
//</widget>
void SetTableViewDefault(QTableView *tv)
{
	tv->setAlternatingRowColors(true);
	tv->verticalHeader()->setMinimumSectionSize(16);
	tv->verticalHeader()->setDefaultSectionSize(20);
}
