﻿#ifndef PASSWORDPROMPTDIALOG_H
#define PASSWORDPROMPTDIALOG_H

#include <QDialog>
#include <optional>

class QCheckBox;
class QComboBox;
class QDialogButtonBox;
class QLabel;
class QLineEdit;

class PasswordPromptDialog : public QDialog
{
	Q_OBJECT
public:
	enum Flag {
		ConfirmPassword = 1,
		SaveOption = 2,
		RememberPassword = 4
	};

	Q_DECLARE_FLAGS(Flags, Flag)
	//Q_FLAG(Flags)

	explicit PasswordPromptDialog(Flags flags, QWidget *parent = nullptr);


	PasswordPromptDialog& setCaption(const std::optional<QString> &caption);
	PasswordPromptDialog& setDescription(const QString &description);
	/// Checks the save checkbox when save is true, this call is ignored when the checkbox does not exist
	PasswordPromptDialog& setSaveChecked(bool save);

	QString password() const;
	bool saveChecked() const;
	/// Returns how long the user wants the password remembered
	///
	/// \return time to remember password in minutes, 0= do not remember, -1= until termination
	int remember() const;
private:
	static const char* translateContext;

	Flags m_Flags;
	QLabel *m_descriptionLabel = nullptr;
	QLabel *m_passwordLabel[2] = { nullptr, nullptr };
	QLineEdit *m_passwordInput[2] = { nullptr, nullptr };
	QCheckBox *m_saveCheck = nullptr;
	QLabel *m_rememberLabel = nullptr;
	QComboBox *m_rememberChoices = nullptr;
	QDialogButtonBox *m_DialogButtons = nullptr;

	std::optional<QString> m_customCaption;

	void retranslateUi();
	void updateOkEnabled();
	void UpdateCaption();

private slots:
	void passwordChanged(const QString &text);
};

#endif // PASSWORDPROMPTDIALOG_H
