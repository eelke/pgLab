﻿#ifndef CONNECTIONMANAGERWINDOW_H
#define CONNECTIONMANAGERWINDOW_H

#include <QMainWindow>
#include <optional>

namespace Ui {
class ConnectionManagerWindow;
}

class ConnectionConfig;
class ConnectionController;
class MasterController;
class QStandardItemModel;

/** \brief Class that holds glue code for the ConnectionManager UI.
 *
 */
class ConnectionManagerWindow : public QMainWindow {
	Q_OBJECT
public:
	explicit ConnectionManagerWindow(MasterController *master, QWidget *parent = nullptr);
	~ConnectionManagerWindow() override;

private slots:
	void on_actionAdd_Connection_triggered();
	void on_actionDelete_connection_triggered();
	void on_actionConnect_triggered();
	void on_actionQuit_application_triggered();
	void on_actionBackup_database_triggered();

	void connectionActivated(const QModelIndex &index);

	void on_actionConfigure_connection_triggered();

	void on_actionAdd_group_triggered();

	void on_actionRemove_group_triggered();

    void on_actionConfigureCopy_triggered();

    void on_actionAbout_triggered();

    void on_actionManual_triggered();

    void on_actionReset_password_manager_triggered();

private:
	Ui::ConnectionManagerWindow *ui;
	MasterController *m_masterController;
	ConnectionController *m_connectionController;
};

#endif // CONNECTIONMANAGERWINDOW_H
