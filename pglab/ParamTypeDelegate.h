﻿#ifndef PARAMTYPEDELEGATE_H
#define PARAMTYPEDELEGATE_H

//#include <QStyledItemDelegate>
#include "util/PgLabItemDelegate.h"

class TypeSelectionItemModel;

/** Item delegate for supplying a combobox for selected the parameter type in
 * the parameter list.
 */
class ParamTypeDelegate : public PgLabItemDelegate {
	Q_OBJECT
public:
	ParamTypeDelegate();
	~ParamTypeDelegate() override;

	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
							  const QModelIndex &index) const override;
	void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	void setModelData(QWidget *editor, QAbstractItemModel *model,
					  const QModelIndex &index) const override;

	void setTypeSelectionModel(TypeSelectionItemModel* model);
private:
	TypeSelectionItemModel* m_typeSelectionModel = nullptr;

private slots:
	void commitAndCloseEditor();
};

#endif // PARAMTYPEDELEGATE_H
