﻿#ifndef MASTERCONTROLLER_H
#define MASTERCONTROLLER_H

#include <QObject>
#include <QSqlDatabase>
#include <atomic>
#include <future>
#include <map>
#include <memory>


class ConnectionController;
/** \brief Controller class responsible for all things global.
 */
class MasterController : public QObject {
	Q_OBJECT
public:
	explicit MasterController(QObject *parent = nullptr);
	MasterController(const MasterController&) = delete;
	MasterController &operator=(const MasterController&) = delete;
	~MasterController();

	void init();

	ConnectionController* connectionController();
	QSqlDatabase& userConfigDatabase();
    
signals:

public slots:

private:
	QSqlDatabase m_userConfigDatabase;
	ConnectionController* m_connectionController = nullptr;
};


#endif // MASTERCONTROLLER_H
