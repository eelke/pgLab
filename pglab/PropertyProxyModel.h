﻿#ifndef HORIZONTALPROXYMODEL_H
#define HORIZONTALPROXYMODEL_H

#include <QIdentityProxyModel>

class PropertyProxyModel : public QIdentityProxyModel {
	Q_OBJECT
public:
	PropertyProxyModel(QObject * parent = nullptr);
	QModelIndex mapToSource(const QModelIndex &proxyIndex) const override;
	QModelIndex mapFromSource(const QModelIndex &sourceIndex) const override;
	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
	QModelIndex parent(const QModelIndex &child) const override;
	int rowCount(const QModelIndex &parent) const override;
	int columnCount(const QModelIndex &parent) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
	QVariant data(const QModelIndex &proxyIndex, int role) const override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;

public Q_SLOTS:
	/** Updates the model (and view) to show the values for row
	 *
     * The column part of the index is not used QModelIndex is used to make it easy to connect to
	 * QItemSelectionModel::currentRowChanged
	 */
	void setActiveRow(const QModelIndex &row);

private:
	enum Columns {
		propertyColumn = 0,
		valueColumn
	};
	int activeRow = -1;
};

#endif // HORIZONTALPROXYMODEL_H
