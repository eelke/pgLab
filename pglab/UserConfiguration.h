﻿#ifndef USERCONFIGURATION_H
#define USERCONFIGURATION_H

#include <QSettings>


/** Class for most of the program configuration
 *
 * The settings represented by this class are stored in a settings file
 * that is user specific. Depending on OS setup it might be shared with
 * other machines in the network because for instance on windows it is
 * part of the roaming profile.
 */
class UserConfiguration
{
public:
	static UserConfiguration* instance();

	UserConfiguration();

	QFont codeFont() const;


private:
	QSettings m_settings;
};

#endif // USERCONFIGURATION_H
