﻿#include "ParamTypeDelegate.h"

#include <QComboBox>
#include "model/TypeSelectionItemModel.h"

ParamTypeDelegate::ParamTypeDelegate() = default;

ParamTypeDelegate::~ParamTypeDelegate() = default;

void ParamTypeDelegate::setTypeSelectionModel(TypeSelectionItemModel* model)
{
	m_typeSelectionModel = model;
}

QWidget *ParamTypeDelegate::createEditor(QWidget *parent,
					const QStyleOptionViewItem &/*option*/,
					const QModelIndex &/*index*/) const

{
	QWidget *w = nullptr;

	auto cmbbx = new QComboBox(parent);
	cmbbx->setMaxVisibleItems(32);
	cmbbx->setModel(m_typeSelectionModel);
	w = cmbbx;

	return w;
}

void ParamTypeDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.column() == 1) {
		QComboBox *cmbbx = dynamic_cast<QComboBox*>(editor);
		if (cmbbx) {
			auto data = index.data();
			if (data.canConvert<QString>()) {
				QModelIndexList indexes = m_typeSelectionModel->match(
						m_typeSelectionModel->index(0, 0), Qt::DisplayRole, data, 1, Qt::MatchFlags( Qt::MatchExactly ));
				if (!indexes.empty()) {
					cmbbx->setCurrentIndex(indexes.at(0).row());
				}
				else {
					cmbbx->setCurrentIndex(-1);
				}
			}
		}
	}
	else {
		 QStyledItemDelegate::setEditorData(editor, index);
	}
}

void ParamTypeDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
									const QModelIndex &index) const
{
	if (index.column() == 1) {
		QComboBox *cmbbx = dynamic_cast<QComboBox*>(editor);
		if (cmbbx) {
			auto data = index.data();
			if (data.canConvert<QString>()) {
				QVariant d = m_typeSelectionModel->data(
							m_typeSelectionModel->index(cmbbx->currentIndex(), 0));
				model->setData(index, d);
			}
		}
	} else {
		QStyledItemDelegate::setModelData(editor, model, index);
	}
}



// triggered by editing finished from editor
void ParamTypeDelegate::commitAndCloseEditor()
{
//	StarEditor *editor = qobject_cast<StarEditor *>(sender());
//    emit commitData(editor);
//    emit closeEditor(editor);
}
