#ifndef PROCESSSTDIOWIDGET_H
#define PROCESSSTDIOWIDGET_H

#include <QWidget>

namespace Ui {
class ProcessStdioWidget;
}

class ProcessStdioWidget : public QWidget
{
	Q_OBJECT

public:
	explicit ProcessStdioWidget(QWidget *parent = 0);
	~ProcessStdioWidget();

private:
	Ui::ProcessStdioWidget *ui;
};

#endif // PROCESSSTDIOWIDGET_H
