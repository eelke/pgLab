﻿#pragma once
#include "Pgsql_declare.h"
#include <QAbstractTableModel>
#include <QColor>

Qt::Alignment GetDefaultAlignmentForType(Oid oid);
QColor GetDefaultColorForType(Oid oid);

inline Qt::Alignment GetDefaultAlignment() {	return Qt::AlignLeft | Qt::AlignVCenter; }
inline Qt::Alignment GetDefaultBoolAlignment() { return Qt::AlignCenter | Qt::AlignVCenter; }
inline Qt::Alignment GetDefaultNumberAlignment() { return Qt::AlignRight | Qt::AlignVCenter; }

inline QColor GetDefaultBoolColor(bool v)
{
	return v ? Qt::darkGreen : Qt::darkRed;
}

inline QColor GetDefaultIntegerColor() { return Qt::darkBlue; }
inline QColor GetDefaultFloatColor() { return Qt::darkCyan; }
inline QColor GetDefaultNumericColor() { return Qt::darkGreen; }
inline QColor GetDefaultNullColor() { return Qt::gray; }

QString FormatBoolForDisplay(bool v);

class QTableView;
void SetTableViewDefault(QTableView *tv);
