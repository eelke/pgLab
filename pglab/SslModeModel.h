﻿#ifndef SSLMODEMODEL_H
#define SSLMODEMODEL_H

#include <QAbstractListModel>

class SslModeModel : public QAbstractListModel
{
	Q_OBJECT
public:
	enum Column { Name, Description, ColCount };

	SslModeModel(QObject *parent = nullptr);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

};

#endif // SSLMODEMODEL_H
