﻿#include "PropertiesPage.h"
#include "util/PgLabItemDelegate.h"
#include "PropertyProxyModel.h"
#include "ResultTableModelUtil.h"
#include "widgets/SqlCodePreview.h"
#include "util/PgLabTableView.h"


PropertiesPage::PropertiesPage(QWidget *parent) : QSplitter(parent)
{
	m_tableView = new PgLabTableView(this);
	addWidget(m_tableView);

	SetTableViewDefault(m_tableView);

	m_propertyProxyModel = new PropertyProxyModel(this);
	m_tableView->setModel(m_propertyProxyModel);

	auto item_delegate = new PgLabItemDelegate(this);
	m_tableView->setItemDelegate(item_delegate);
	m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}


void PropertiesPage::setSourceModel(QAbstractItemModel *model)
{
	m_propertyProxyModel->setSourceModel(model);
}


void PropertiesPage::setActiveRow(const QModelIndex &row)
{
	m_propertyProxyModel->setActiveRow(row);
	m_tableView->resizeColumnsToContents();
}
