﻿#include "CreateDatabaseDialog.h"
#include "ui_CreateDatabaseDialog.h"
#include <QStringBuilder>

CreateDatabaseDialog::CreateDatabaseDialog(std::shared_ptr<PgDatabaseCatalog> catalog, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::CreateDatabaseDialog)
    , m_catalog(catalog)
    , definition(*catalog, InvalidOid, "")
{
    ui->setupUi(this);
}

CreateDatabaseDialog::~CreateDatabaseDialog()
{
    delete ui;
}

//CREATE DATABASE name
//    [ [ WITH ] [ OWNER [=] user_name ]
//           [ TEMPLATE [=] template ]
//           [ ENCODING [=] encoding ]
//           [ LC_COLLATE [=] lc_collate ]
//           [ LC_CTYPE [=] lc_ctype ]
//           [ TABLESPACE [=] tablespace_name ]
//           [ ALLOW_CONNECTIONS [=] allowconn ]
//           [ CONNECTION LIMIT [=] connlimit ]
//           [ IS_TEMPLATE [=] istemplate ] ]

void CreateDatabaseDialog::on_tabWidget_currentChanged(int index)
{
    if (index == 0)
        return;

    // update the sql
    definition.setObjectName(ui->name->text());

//    definition.setOwnerOid()
    definition.dbTemplate = ui->dbTemplate->currentText();

    //QString drop_sql = definition.dropSql();
    QString create_sql = definition.createSql();
    ui->sqlPreview->setPlainText(create_sql);

//    create_sql += "\n\n-- set Privileges\n";
//    create_sql += definition.grantSql() % "\n";

//    create_sql += "-- set comment\n";
//    create_sql += definition.commentSql() % "\n";

//    ui->sqlPreview->setPlainText(drop_sql % "\n\n" % create_sql);

}

