﻿#ifndef NAMESPACEFILTER_H
#define NAMESPACEFILTER_H

enum class NamespaceFilter {
	User, PgCatalog, InformationSchema
};

#endif // NAMESPACEFILTER_H
