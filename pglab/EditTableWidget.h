﻿#ifndef EDITTABLEWIDGET_H
#define EDITTABLEWIDGET_H

#include <QObject>
#include <QWidget>
#include <memory>

class OpenDatabase;

class EditTableWidget : public QWidget {
	Q_OBJECT
public:
	explicit EditTableWidget(std::shared_ptr<OpenDatabase> database, QWidget *parent = nullptr);

signals:

public slots:

private:
	std::shared_ptr<OpenDatabase> m_database;
};

#endif // EDITTABLEWIDGET_H
