﻿#ifndef ABSTRACTCOMMAND_H
#define ABSTRACTCOMMAND_H

#include <QString>

class QAction;

class AbstractCommand {
public:
	virtual QAction* getAction() const = 0;
	virtual QString getToolbar() const = 0;
	virtual QString getMenuPath() const = 0;
};


#endif // ABSTRACTCOMMAND_H
