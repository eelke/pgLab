﻿#include "UserConfiguration.h"
#include <QFont>

UserConfiguration* UserConfiguration::instance()
{
	static UserConfiguration config; // in C++ 11 static ensures thread safe initialization at first call of function
	return &config;
}

UserConfiguration::UserConfiguration()
	: m_settings(nullptr)
{
}

QFont UserConfiguration::codeFont() const
{
	QString family = m_settings.value("fonts/code/family", QString("Source Code Pro")).toString();
	int size = m_settings.value("fonts/code/size", 10).toInt();
	QFont font;
	font.setFamily(family);
	font.setFixedPitch(true);
	font.setPointSize(size);
	return font;
}

