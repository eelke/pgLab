﻿#ifndef MANAGEDPAGE_H
#define MANAGEDPAGE_H

#include <QWidget>

/// Base class for widgets that are used as on the tabs in DatabaseWindow
/// if a widget does not inherit from managed pase DatabaseWindow will
/// fallback on default behaviour.
class ManagedPage: public QWidget
{
	Q_OBJECT
public:
	using QWidget::QWidget;

	virtual ~ManagedPage() {}

    /// Tells DatabaseWindow if it is ok to close this tab
	virtual bool CanClose(bool prompt_user) = 0;
};

#endif // MANAGEDPAGE_H
