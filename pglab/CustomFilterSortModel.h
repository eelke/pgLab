﻿#ifndef TRIGGERFILTERSORTMODEL_H
#define TRIGGERFILTERSORTMODEL_H

#include <QSortFilterProxyModel>
#include "Pgsql_oids.h"

class CustomFilterSortModel : public QSortFilterProxyModel {
public:
	CustomFilterSortModel(QObject *parent = nullptr);

	void setOidFilterTable(Oid tbl_oid, int role);

protected:
	bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
private:
	Oid m_filterOid = InvalidOid;
	int m_filterRole = Qt::DisplayRole;
};

#endif // TRIGGERFILTERSORTMODEL_H
