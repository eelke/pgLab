#pragma once

#include <QSplitter>
#include <QWidget>

#include "util/PgLabTableViewHelper.h"
#include "catalog/models/DatabasesTableModel.h"

class PgDatabase;
class PgDatabaseCatalog;
class PgLabTableView;
class QSortFilterProxyModel;
class SqlCodePreview;

class DatabasesPage: public QSplitter {
public:
    explicit DatabasesPage(std::shared_ptr<OpenDatabase> opendatabase, QWidget * parent = nullptr);

    void setCatalog(std::shared_ptr<PgDatabaseCatalog> cat);

    void retranslateUi(bool all = true);

private:
    PgLabTableViewHelper<DatabasesTableModel> m_databasesTableView;
    SqlCodePreview *m_tableSql = nullptr;

    std::shared_ptr<PgDatabaseCatalog>  m_catalog;

    void updateDatabaseDetails(const PgDatabase &db);
    void updateSqlTab(const PgDatabase &db);

private slots:

    void databaseSelectionChanged(const QModelIndex &current, const QModelIndex &previous);
};

