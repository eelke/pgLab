#include "RolesPage.h"
#include "catalog/PgDatabaseCatalog.h"
#include "catalog/models/RolesTableModel.h"

RolesPage::RolesPage(QWidget * parent)
    : QSplitter(Qt::Horizontal, parent)
    , m_rolesTableView(this)
{
    auto tv = m_rolesTableView.itemView();
    tv->setSelectionMode(QAbstractItemView::SingleSelection);

    m_detailsTabs = new QTabWidget(this);

    addWidget(tv);
    addWidget(m_detailsTabs);
}

void RolesPage::setCatalog(std::shared_ptr<PgDatabaseCatalog> cat)
{
    m_catalog = cat;
    m_rolesTableView.dataModel()->setRoleList(cat->authIds());
    m_rolesTableView.itemView()->resizeColumnsToContents();
}
