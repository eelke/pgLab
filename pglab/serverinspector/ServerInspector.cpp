#include "ServerInspector.h"
#include "DatabasesPage.h"
#include "OpenDatabase.h"
#include "RolesPage.h"

#include <QTabWidget>
#include <QVBoxLayout>

ServerInspector::ServerInspector(std::shared_ptr<OpenDatabase> open_database, QWidget *parent)
    : QWidget(parent)
{
    m_databasesPage = new DatabasesPage(open_database, this);
    m_rolesPage = new RolesPage(this);

    m_tabWidget = new QTabWidget(this);
    m_tabWidget->addTab(m_databasesPage, tr("Databases"));
    m_tabWidget->addTab(m_rolesPage, tr("Roles"));

    auto layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->addWidget(m_tabWidget);

    setCatalog(open_database->catalog());
    retranslateUi(false);
}


void ServerInspector::setCatalog(std::shared_ptr<PgDatabaseCatalog> cat)
{
    m_catalog = cat;
    m_databasesPage->setCatalog(cat);
    m_rolesPage->setCatalog(cat);
}

void ServerInspector::retranslateUi(bool )
{
//    m_tabWidget->setTabText(m_tabWidget->indexOf(m_namespacePage),
//		QApplication::translate("CatalogInspector", "Schemas", nullptr));

}
