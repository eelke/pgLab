#pragma once

#include <QWidget>
#include <memory>

class DatabasesPage;
class OpenDatabase;
class PgDatabaseCatalog;
class QTabWidget;
class RolesPage;


class ServerInspector : public QWidget {
    Q_OBJECT
public:
    explicit ServerInspector(std::shared_ptr<OpenDatabase> open_database, QWidget *parent = nullptr);

    void setCatalog(std::shared_ptr<PgDatabaseCatalog> cat);
private:
    QTabWidget *m_tabWidget = nullptr;

    DatabasesPage *m_databasesPage = nullptr;
    RolesPage *m_rolesPage = nullptr;

    std::shared_ptr<PgDatabaseCatalog>  m_catalog;

    void retranslateUi(bool all = true);
};

