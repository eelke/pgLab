#pragma once

#include <QSplitter>
#include <QWidget>

#include "util/PgLabTableViewHelper.h"
#include "catalog/models/RolesTableModel.h"

class PgDatabaseCatalog;
class PgLabTableView;
class QSortFilterProxyModel;


class RolesPage: public QSplitter {
public:
    RolesPage(QWidget * parent = nullptr);

    void setCatalog(std::shared_ptr<PgDatabaseCatalog> cat);
private:
    PgLabTableViewHelper<RolesTableModel> m_rolesTableView;
    QTabWidget *m_detailsTabs = nullptr;

    std::shared_ptr<PgDatabaseCatalog>  m_catalog;

};

