﻿#include "ConnectionManagerWindow.h"
#include "About.h"
#include "ui_ConnectionManagerWindow.h"
#include "MasterController.h"
#include "ConnectionController.h"
#include "ConnectionListModel.h"
#include <QDir>
#include <QMessageBox>
#include <QStandardItemModel>
#include <QStandardPaths>
#include <QUrl>

ConnectionManagerWindow::ConnectionManagerWindow(MasterController *master, QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::ConnectionManagerWindow)
	, m_masterController(master)
	, m_connectionController(master->connectionController())
{
	ui->setupUi(this);
	ui->treeView->setModel(m_connectionController->getConnectionTreeModel());
	ui->treeView->setDragDropMode(QAbstractItemView::InternalMove);

	connect(ui->treeView, &QTreeView::activated, this,
			 &ConnectionManagerWindow::connectionActivated);
}


ConnectionManagerWindow::~ConnectionManagerWindow()
{
	delete ui;
}

void ConnectionManagerWindow::on_actionAdd_Connection_triggered()
{
	m_connectionController->createConnection();
}

void ConnectionManagerWindow::on_actionDelete_connection_triggered()
{
	auto ci = ui->treeView->selectionModel()->currentIndex();
	if (ci.isValid()) {
		auto node = static_cast<ConnectionNode*>(ci.internalPointer());
		auto cc = dynamic_cast<ConnectionConfig*>(node);
		if (cc) {
			auto res = QMessageBox::question(this, "pglab",
				tr("Are you sure you want to remove this connection?"), QMessageBox::Yes, QMessageBox::No);
			if (res == QMessageBox::Yes) {
				auto cm = m_connectionController->getConnectionTreeModel();
				cm->removeRow(ci.row(), ci.parent());
			}
		}
	}
}

void ConnectionManagerWindow::on_actionConnect_triggered()
{
	auto ci = ui->treeView->selectionModel()->currentIndex();
	m_connectionController->openSqlWindowForConnection(ci);
}

void ConnectionManagerWindow::on_actionQuit_application_triggered()
{
	auto res = QMessageBox::question(this, "pglab",
		tr("Close all windows?"), QMessageBox::Yes, QMessageBox::No);
	if (res == QMessageBox::Yes) {
		QApplication::quit();
	}
}

void ConnectionManagerWindow::on_actionBackup_database_triggered()
{
	auto ci = ui->treeView->selectionModel()->currentIndex();
	m_connectionController->openBackupDlgForConnection(ci);
}

void ConnectionManagerWindow::connectionActivated(const QModelIndex &index)
{
	if (index.isValid()) {
		m_connectionController->openSqlWindowForConnection(index);
	}
}

void ConnectionManagerWindow::on_actionConfigure_connection_triggered()
{
	auto ci = ui->treeView->selectionModel()->currentIndex();
	m_connectionController->editConnection(ci);
}

void ConnectionManagerWindow::on_actionAdd_group_triggered()
{
	m_connectionController->addGroup();
}

void ConnectionManagerWindow::on_actionRemove_group_triggered()
{
	auto ci = ui->treeView->selectionModel()->currentIndex();
	m_connectionController->removeGroup(ci);
}

void ConnectionManagerWindow::on_actionConfigureCopy_triggered()
{
    auto ci = ui->treeView->selectionModel()->currentIndex();
    m_connectionController->editCopy(ci);
}

void ConnectionManagerWindow::on_actionAbout_triggered()
{
    ShowAboutDialog(this);
}


void ConnectionManagerWindow::on_actionManual_triggered()
{
    OpenManual();
}


void ConnectionManagerWindow::on_actionReset_password_manager_triggered()
{
    auto warning_response = QMessageBox::warning(this, "pgLab", tr(
        "Warning you are about to reset the password manager master passwords "
        "all stored passwords will be lost! Are you shure you wish to continue?"),
        QMessageBox::Yes | QMessageBox::No);

    if (warning_response == QMessageBox::Yes)
        m_connectionController->resetPasswordManager();
}

