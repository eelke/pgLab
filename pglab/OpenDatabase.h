﻿#ifndef OPENDATABASE_H
#define OPENDATABASE_H

#include "ConnectionConfig.h"
#include <QException>
#include <memory>

class PgDatabaseCatalog;
class TypeSelectionItemModel;

class OpenDatabaseException : public QException {
public:
	OpenDatabaseException(QString message)
		: m_message(std::move(message))
	{}

	QString text() const { return m_message; }

	void raise() const override { throw *this; }
	OpenDatabaseException *clone() const override { return new OpenDatabaseException(*this); }
private:
	QString m_message;
};

/** Instances of this class represent a single database on which atleast one
 * window is opened. This class is used to track details about that database.
 */
class OpenDatabase {
public:
	using OpenDatabaseSPtr = std::shared_ptr<OpenDatabase>;
	static OpenDatabaseSPtr createOpenDatabase(const ConnectionConfig &cfg);

	OpenDatabase(const OpenDatabase &) = delete;
	OpenDatabase& operator=(const OpenDatabase &) = delete;
	~OpenDatabase();

	std::shared_ptr<PgDatabaseCatalog> catalog();
	TypeSelectionItemModel* typeSelectionModel();
	void refresh();

	const ConnectionConfig& config() const { return m_config; }
private:
	ConnectionConfig m_config;
	std::shared_ptr<PgDatabaseCatalog> m_catalog;

	TypeSelectionItemModel *m_typeSelectionModel = nullptr;

	OpenDatabase(const ConnectionConfig& cfg);
	void Init();
};

#endif // OPENDATABASE_H
