#ifndef CREATEDATABASEDIALOG_H
#define CREATEDATABASEDIALOG_H

#include <QDialog>

#include "catalog\PgDatabase.h"

class PgDatabaseCatalog;

namespace Ui {
class CreateDatabaseDialog;
}

class CreateDatabaseDialog : public QDialog
{
    Q_OBJECT

public:
    CreateDatabaseDialog(std::shared_ptr<PgDatabaseCatalog> catalog, QWidget *parent = 0);
    ~CreateDatabaseDialog();

private slots:
    void on_tabWidget_currentChanged(int index);

private:
    Ui::CreateDatabaseDialog *ui;

    std::shared_ptr<PgDatabaseCatalog> m_catalog;
    PgDatabase definition;
};

#endif // CREATEDATABASEDIALOG_H
