﻿#include "PropertyProxyModel.h"
/*
 * Code borrowed from: https://stackoverflow.com/questions/21653253/how-to-change-orientation-of-qt-tableview
 *
 * Originally it was called Horizontal_proxy_model however some adjustments were made to it.
 * Instead of the column headers becoming row headers we now convert them to the first column.
 * The second column show the values of a single row from the source model. Which is determined
 * by the setActiveRow call.
 */


PropertyProxyModel::PropertyProxyModel(QObject *parent)
	: QIdentityProxyModel(parent)
{
}

QModelIndex PropertyProxyModel::mapToSource(const QModelIndex &proxyIndex) const
{
	if (sourceModel()) {
		if (activeRow >= 0) {
			if (proxyIndex.column() == valueColumn) {
				return sourceModel()->index(activeRow, proxyIndex.row());
			}
		}
	}
	return QModelIndex();
}

QModelIndex PropertyProxyModel::mapFromSource(const QModelIndex &sourceIndex) const
{
	if (activeRow >= 0) {
		if (sourceIndex.row() == activeRow) {
			return index(sourceIndex.column(), valueColumn);
		}
	}
	return QModelIndex();
}

QModelIndex PropertyProxyModel::index(int row, int column, const QModelIndex &) const
{
	return createIndex(row, column, nullptr);
}

QModelIndex PropertyProxyModel::parent(const QModelIndex &) const
{
	return QModelIndex();
}

int PropertyProxyModel::rowCount(const QModelIndex &) const
{
	return sourceModel() ? sourceModel()->columnCount() : 0;
}

int PropertyProxyModel::columnCount(const QModelIndex &) const
{
  return 2;
}

QVariant PropertyProxyModel::headerData(
		   int section, Qt::Orientation orientation, int role) const
{
//	if (!sourceModel()) {
//		return QVariant();
//	}
//	Qt::Orientation new_orientation = orientation == Qt::Horizontal ?
//			Qt::Vertical : Qt::Horizontal;
//	return sourceModel()->headerData(section, new_orientation, role);
	if (orientation == Qt::Horizontal) {
		if (role == Qt::DisplayRole) {
			switch (section) {
			case 0:
				return tr("Property");
			case 1:
				return tr("Value");
			}
		}
	}
	return QVariant();
}

QVariant PropertyProxyModel::data(const QModelIndex &proxyIndex, int role) const
{
	auto sm = sourceModel();
	if (sm) {
		switch (proxyIndex.column()) {
		case 0:
			// return source header data
			return sm->headerData(proxyIndex.row(), Qt::Horizontal, role);
		case 1:
			// return value if activeRow is set
			if (activeRow >= 0) {
				return QIdentityProxyModel::data(proxyIndex, role);
			}
		}
	}
	//return d->model->data(mapToSource(proxyIndex), role);
	return QVariant();
}

void PropertyProxyModel::setActiveRow(const QModelIndex &row)
{
	activeRow = row.isValid() ? row.row() : -1;
    emit dataChanged(index(0, valueColumn), index(rowCount(QModelIndex()), valueColumn),
                     QVector<int>() << Qt::DisplayRole);
}

Qt::ItemFlags PropertyProxyModel::flags(const QModelIndex &) const
{
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}
