﻿#ifndef IDATABASEWINDOW_H
#define IDATABASEWINDOW_H

#include <QIcon>
#include <QString>
#include <memory>

namespace Pgsql {
	class Result;
}

class OpenDatabase;
class QWidget;
/** Abstract class definition to make some functions
 * available to other classes without having to know everything about the window.
 */
class IDatabaseWindow {
public:
	virtual void setTitleForWidget(QWidget* widget, QString title, QString hint) = 0;
	virtual void setIconForWidget(QWidget* widget, QIcon icon) = 0;
	virtual std::shared_ptr<OpenDatabase> openDatabase() = 0;
	virtual void showStatusBarMessage(QString message) = 0;
	virtual void newCodeGenPage(QString query, std::shared_ptr<const Pgsql::Result> dbres) = 0;
};

#endif // IDATABASEWINDOW_H
