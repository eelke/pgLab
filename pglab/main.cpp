﻿#include "MasterController.h"
#include <QApplication>
#ifdef _WIN32
# include <winsock2.h>
#endif
#include <memory>

int main(int argc, char *argv[])
{

	/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
#ifdef WIN32
	WORD wVersionRequested = MAKEWORD(2, 2);
	WSADATA wsaData;
	int err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0) {
		/* Tell the user that we could not find a usable */
		/* Winsock DLL.                                  */
		printf("WSAStartup failed with error: %d\n", err);
		return 1;
	}
#endif


	QApplication a(argc, argv);

	QCoreApplication::setOrganizationName("pglab");
	QCoreApplication::setOrganizationDomain("eelkeklein.nl");
	QCoreApplication::setApplicationName("pglab");

    int result = -1;
    {   
        // make sure the io_service is stopped before we wait on the future
        auto master_controller = std::make_unique<MasterController>();
        master_controller->init();
        result = a.exec();
    }
#ifdef WIN32
	WSACleanup();
#endif
	return result;
}

