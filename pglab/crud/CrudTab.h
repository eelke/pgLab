﻿#ifndef CRUDTAB_H
#define CRUDTAB_H

#include <QSortFilterProxyModel>
#include "catalog/PgClass.h"
#include "IDatabaseWindow.h"
#include <QWidget>
#include <memory>
#include <optional>

namespace Ui {
	class CrudTab;
}

class OpenDatabase;
class CrudModel;
class QMenu;

class CrudTab : public QWidget {
	Q_OBJECT
public:
	explicit CrudTab(IDatabaseWindow *context, QWidget *parent = nullptr);
	~CrudTab() override;

	void setConfig(Oid oid);
public slots:
	void refresh();
private:
	Ui::CrudTab *ui;
	IDatabaseWindow *m_context;

	std::shared_ptr<OpenDatabase> m_db;
	std::optional<PgClass> m_table;

	CrudModel *m_crudModel = nullptr;
    QSortFilterProxyModel *m_SortFilterProxy = nullptr;

    QMenu *headerContextMenu = nullptr;

	void initActions();
    void buildHeaderContextMenu();

private slots:
	void on_actionRemove_rows_triggered();
	void headerCustomContextMenu(const QPoint &pos);
    void gotoColumn(QString column);
};


#endif // CRUDTAB_H
