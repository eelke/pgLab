#include "About.h"
#include "Config.h"
#include <QCoreApplication>
#include <QDesktopServices>
#include <QMessageBox>
#include <QString>
#include <QUrl>

void ShowAboutDialog(QWidget *parent)
{
    QString pgLabVersionString = PGLAB_VERSION_STRING;
    QString releaseNotesUrl = "https://eelke.gitlab.io/pgLab/releasenotes.html";
    QString year = QString::fromLatin1(__DATE__, 11).right(4);
    QMessageBox::about(parent, "pgLab", QMessageBox::tr(
              "<h1>Version %3</h1>"
              "<p><a href=\"%2\">Releasenotes</a></p>"
              "<p>Copyrights 2016-%1, Eelke Klein, All Rights Reserved.</p>"
              "<p>The program is provided AS IS with NO WARRANTY OF ANY KIND, "
              "INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS "
              "FOR A PARTICULAR PURPOSE.</p>"
              "<p>This program is dynamically linked with Qt " QT_VERSION_STR " Copyrights "
              "the Qt Company Ltd. https://www.qt.io/licensing/.</p>"
              "<p>Some icons by <a href=\"https://icons8.com\">Icons8</a> and others by <a href=\"http://www.fatcow.com/free-icons\">fatcow</a> provided under Creative Commons "
              "attribution 3.0 license.</p>"
              ).arg(year).arg(releaseNotesUrl).arg(pgLabVersionString));
}

void OpenManual()
{
    QDesktopServices::openUrl(QString("https://eelke.gitlab.io/pgLab/"));
}
