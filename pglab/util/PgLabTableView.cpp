﻿#include "util/PgLabTableView.h"
#include "util/PgLabItemDelegate.h"
#include <QHeaderView>
#include <QSortFilterProxyModel>

PgLabTableView::PgLabTableView(QWidget *parent)
	: QTableView(parent)
{
	setAlternatingRowColors(true);
	setItemDelegate(new PgLabItemDelegate(this));
	setWordWrap(false);

	auto pal = palette();
	pal.setColor(QPalette::Inactive, QPalette::Highlight, pal.color(QPalette::Active, QPalette::Highlight));
	pal.setColor(QPalette::Inactive, QPalette::HighlightedText, pal.color(QPalette::Active, QPalette::HighlightedText));
	setPalette(pal);

	auto vertical_header = verticalHeader();
	vertical_header->setMinimumSectionSize(16);
	vertical_header->setDefaultSectionSize(20);
}

