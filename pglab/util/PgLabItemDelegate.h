﻿#ifndef PGLABITEMDELEGATE_H
#define PGLABITEMDELEGATE_H

#include <QStyledItemDelegate>

class AbstractEditorFactory;

/** Delegate for rendering SQL data types in tableviews
 *
 * This delegate removes the need for the model to provide formatting information
 * which in many cases solely based on the datatype so this delegate can determine
 * on its own what the correct formatting is.
 */
class PgLabItemDelegate : public QStyledItemDelegate
{
public:
	PgLabItemDelegate(QObject *parent = nullptr);

	virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
	void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

	AbstractEditorFactory* editorFactory();
	void setEditorFactory(AbstractEditorFactory *editor_factory);

protected:
	virtual void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const override;

private:
    AbstractEditorFactory *m_editorFactory = nullptr;
    QString HandleBytes(qlonglong s, QColor &forground_color) const;
};

#endif // PGLABITEMDELEGATE_H
