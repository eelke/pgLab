﻿#ifndef SQLSYNTAXHIGHLIGHTER_H
#define SQLSYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextFormat>
#include "catalog/PgKeywordList.h"
#include "util.h"

class PgTypeContainer;

class SqlSyntaxHighlighter : public QSyntaxHighlighter
{
	Q_OBJECT

public:
	SqlSyntaxHighlighter(QTextDocument *parent = nullptr);
	~SqlSyntaxHighlighter() override;

	void setTypes(const PgTypeContainer& types);
protected:
	void highlightBlock(const QString &text) override;

private:
	QTextCharFormat m_keywordFormat;
	QTextCharFormat m_commentFormat;
	QTextCharFormat m_quotedStringFormat;
	QTextCharFormat m_typeFormat;
	QTextCharFormat m_quotedIdentifierFormat;
    QTextCharFormat m_parameterFormat;

	t_SymbolSet m_typeNames;
};

#endif // SQLSYNTAXHIGHLIGHTER_H
