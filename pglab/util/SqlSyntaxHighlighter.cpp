﻿#include "SqlSyntaxHighlighter.h"

#include "catalog/PgTypeContainer.h"
#include "SqlLexer.h"



SqlSyntaxHighlighter::SqlSyntaxHighlighter(QTextDocument *parent)
	: QSyntaxHighlighter(parent)
{
	m_keywordFormat.setForeground(QColor(32, 32, 192));
	m_keywordFormat.setFontWeight(QFont::Bold);

	m_commentFormat.setForeground(QColor(128, 128, 128));

	m_quotedStringFormat.setForeground(QColor(192, 32, 192));

	m_quotedIdentifierFormat.setForeground(QColor(192, 128, 32));

	m_typeFormat.setForeground(QColor(32, 192, 32));
	m_typeFormat.setFontWeight(QFont::Bold);
    
    m_parameterFormat.setForeground(QColor(192, 32, 32));
    m_parameterFormat.setFontWeight(QFont::Bold);
}

SqlSyntaxHighlighter::~SqlSyntaxHighlighter()
{
}

void SqlSyntaxHighlighter::setTypes(const PgTypeContainer& types)
{
	m_typeNames.clear();
	for (auto&& e : types) {
		m_typeNames.insert(e.objectName());
	}
	rehighlight();
}

void SqlSyntaxHighlighter::highlightBlock(const QString &text)
{
	int previous_state = previousBlockState();
	if (previous_state <= 0)
		previous_state = 0;
	SqlLexer lexer(text, (LexerState)previous_state);
	int startpos, length;
	BasicTokenType tokentype;
	QString s;
	while (lexer.nextBasicToken(startpos, length, tokentype, s) && tokentype != BasicTokenType::End) {
		switch (tokentype) {
		case BasicTokenType::None:
		case BasicTokenType::End: // End of input
		case BasicTokenType::DollarQuote:
			break;
		case BasicTokenType::Symbol: { // can be many things, keyword, object name, operator, ..
				auto kw = getPgsqlKeyword(s);
				if (kw != nullptr) {
					setFormat(startpos, length, m_keywordFormat);
				}
				else if (m_typeNames.count(s.toLower()) > 0) {
					setFormat(startpos, length, m_typeFormat);
				}
			}
			break;
		case BasicTokenType::OpenBlockComment:
			setCurrentBlockState((int)lexer.currentState());
		case BasicTokenType::BlockComment:
		case BasicTokenType::Comment:
			setFormat(startpos, length, m_commentFormat);
			break;
		case BasicTokenType::QuotedString:
			setFormat(startpos, length, m_quotedStringFormat);
			break;
		case BasicTokenType::QuotedIdentifier:
			setFormat(startpos, length, m_quotedIdentifierFormat);
			break;
		case BasicTokenType::Parameter:
            setFormat(startpos, length, m_parameterFormat);
            break;
		}
	}
}
