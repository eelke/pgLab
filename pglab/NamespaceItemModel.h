﻿#ifndef NAMESPACEITEMMODEL_H
#define NAMESPACEITEMMODEL_H

#include <QAbstractItemModel>
#include <QVector>
#include "catalog/PgNamespace.h"
//#include <vector>
//#include <iterator>

namespace NamespaceItemModel_impl {

	class Node;
	class LeafNode;
	class GroupNode;

}

class PgNamespaceContainer;

class NamespaceItemModel: public QAbstractItemModel {
public:
	NamespaceItemModel(QObject *parent = 0);

	enum Columns {
		ColNamespaceName,
		ColOwner,
		ColAcl,

		ColCount
	};

	void init(std::shared_ptr<const PgNamespaceContainer> ns);
	void setEnableCheckboxes(bool enable);
	bool isEnableCheckboxes() const;

	virtual QModelIndex index(int row, int column,
		const QModelIndex &parent = QModelIndex()) const override;
	virtual QModelIndex parent(const QModelIndex &index) const override;

	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const override;

private:
	using GroupVec = QVector<std::shared_ptr<NamespaceItemModel_impl::GroupNode>>;

	GroupVec groups;
	bool m_enableCheckboxes = true;

	// QAbstractItemModel interface
public:
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};

#endif // NAMESPACEITEMMODEL_H
