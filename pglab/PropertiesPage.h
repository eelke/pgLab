﻿#ifndef PROPERTIESPAGE_H
#define PROPERTIESPAGE_H

#include <QSplitter>

class SqlCodePreview;
class PgDatabaseCatalog;
class PropertyProxyModel;
class PgLabTableView;
class QAbstractItemModel;

class PropertiesPage : public QSplitter {
	Q_OBJECT
public:
	explicit PropertiesPage(QWidget *parent = nullptr);

	void setSourceModel(QAbstractItemModel *model);
signals:
public slots:
	/** Updates the model (and view) to show the values for row
	 *
	 * The column part of the index is not used QModelIndex is used to make is eacy to connect to
	 * QItemSelectionModel::currentRowChanged
	 */
	void setActiveRow(const QModelIndex &row);

private:
	PgLabTableView *m_tableView = nullptr;
	PropertyProxyModel *m_propertyProxyModel = nullptr;

};

#endif // PROPERTIESPAGE_H
