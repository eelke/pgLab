﻿#ifndef QUERYPARAMLISTCONTROLLER_H
#define QUERYPARAMLISTCONTROLLER_H

#include <QObject>
#include "ParamListModel.h"
#include "ParamTypeDelegate.h"
#include "Pgsql_Params.h"
#include <memory>

class QTableView;
class OpenDatabase;

class QueryParamListController : public QObject {
	Q_OBJECT
public:
	QueryParamListController(QTableView *tv, std::shared_ptr<OpenDatabase> opendb, QWidget *parent);

	Pgsql::Params params() const;
	bool empty() const;
public slots:
	void on_addParam();
	void on_removeParam();
private:
	QTableView *paramTableView;
	std::shared_ptr<OpenDatabase> m_openDatabase;
	ParamListModel m_paramList;
	ParamTypeDelegate m_typeDelegate;
};

#endif // QUERYPARAMLISTCONTROLLER_H
