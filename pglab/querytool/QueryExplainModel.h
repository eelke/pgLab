﻿#pragma once

#include <QAbstractItemModel>
#include <string>
#include "ExplainTreeModelItem.h"

/** \brief Model class for displaying the explain of a query in a tree like format.
 */
class QueryExplainModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	explicit QueryExplainModel(QObject *parent, ExplainRoot::SPtr exp);

	QVariant data(const QModelIndex &index, int role) const override;

//	Qt::ItemFlags flags(const QModelIndex &index) const override;

	QVariant headerData(int section, Qt::Orientation orientation,
		int role = Qt::DisplayRole) const override;

	QModelIndex index(int row, int column,
		const QModelIndex &parent = QModelIndex()) const override;

	QModelIndex parent(const QModelIndex &index) const override;

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;

	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

private:
	ExplainRoot::SPtr explain;
};
