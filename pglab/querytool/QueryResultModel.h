﻿#ifndef QUERYRESULTMODEL_H
#define QUERYRESULTMODEL_H

#include <QAbstractTableModel>
#include "catalog/models/BaseTableModel.h"
#include "Pgsql_Connection.h"

class PgDatabaseCatalog;

class QueryResultModel : public QAbstractTableModel
{
	Q_OBJECT
public:
	QueryResultModel(QObject *parent, std::shared_ptr<Pgsql::Result> r, std::shared_ptr<PgDatabaseCatalog> cat);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const override;

	std::shared_ptr<const Pgsql::Result> GetPgsqlResult() const { return result; }
protected:
//	virtual Oid getType(int column) const override;
//	virtual QVariant getData(const QModelIndex &index) const override;
private:
	std::shared_ptr<Pgsql::Result> result;
	std::shared_ptr<PgDatabaseCatalog> catalog;
};

#endif // QUERYRESULTMODEL_H
