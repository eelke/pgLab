﻿#include "EditTableWidget.h"
#include "EditColumnTableModel.h"
#include "util/PgLabItemDelegate.h"
#include <QVBoxLayout>
#include <QTableView>
#include "OpenDatabase.h"
#include "catalog/PgDatabaseCatalog.h"
#include "SelectionEditorFactory.h"
#include "model/CollationModelFactory.h"
#include "model/TypeModelFactory.h"

EditTableWidget::EditTableWidget(std::shared_ptr<OpenDatabase> database, QWidget *parent)
	: QWidget(parent)
	, m_database(database)
{


	// Table
	auto table = new QTableView(this);
	// Dialogbutton

	auto mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(table);
	setLayout(mainLayout);

	auto model = new EditColumnTableModel(database->catalog(), this);
	table->setModel(model);
	table->setItemDelegate(new PgLabItemDelegate(this));

	auto types = database->catalog()->types();
	auto type_delegate = new PgLabItemDelegate(this);
	type_delegate->setEditorFactory(
			new SelectionEditorFactory(this, new TypeModelFactory(this, types), 0, 1)
				);
	table->setItemDelegateForColumn(EditColumnTableModel::TypeCol, type_delegate);

	auto collations = database->catalog()->collations();
	auto collate_delegate = new PgLabItemDelegate(this);
	collate_delegate->setEditorFactory(
			new SelectionEditorFactory(this, new CollationModelFactory(this, collations), 0, 1)
				);
	table->setItemDelegateForColumn(EditColumnTableModel::CollateCol, collate_delegate);

//	if (opendb) {
//		m_typeDelegate.setTypeSelectionModel(opendb->typeSelectionModel());
//	}

//	paramTableView->setModel(&m_paramList);
//	paramTableView->setItemDelegateForColumn(1, &m_typeDelegate);
}

