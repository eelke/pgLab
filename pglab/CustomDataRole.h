﻿#ifndef CUSTOMDATAROLE_H
#define CUSTOMDATAROLE_H

#include <Qt>

///	Returned by a model when asked for CustomReferencedTypeRole
///
/// Models will probably only be asked this for columns for which they returned
/// Oid_Oid for the CustomDataTypeRole
enum class ReferencedType {
	PgType,
	PgNamespace,
	PgRole
};

///
enum class DataMeaning {
    Normal,
    Bytes ///< the value represents bytes  pretty print in KiB, MiB, GiB, TiB, PiB, EiB
};

enum CustomDataRole {
	CustomDataTypeRole = Qt::UserRole, ///< Requist the basic type of the value
	CustomReferencedTypeRole, ///<
    CustomDataMeaningRole,
    CustomSortRole, // return data in a form that will sort correctly
    // Add other enum before this one as we might want to have multiple hidden values
	FirstHiddenValue, ///< Used to request value from a model which is not handed to the view
};

#endif // CUSTOMDATAROLE_H
