﻿#ifndef BACKUPDIALOG_H
#define BACKUPDIALOG_H

#include <QDialog>
#include <QTextCharFormat>
#include "ConnectionConfig.h"
#include <QProcess>


class QLabel;
class QPushButton;
class QTabWidget;
class QVBoxLayout;
class QHBoxLayout;
class QComboBox;
class QSpinBox;
class QCheckBox;
class QPlainTextEdit;
class QLineEdit;
class QFormLayout;
class QStackedLayout;


class BackupDialog : public QDialog
{
	Q_OBJECT

public:
	explicit BackupDialog(QWidget *parent = nullptr);
	~BackupDialog();

	void ConnectTo(QProcess *process);

	void disconnectCurrentProcess();
	void setConfig(const ConnectionConfig &cfg);
	void retranslateUi();
private:

	QProcess *m_process = nullptr;
	ConnectionConfig m_config;

	QLabel *labelFileName;
	QLineEdit *editFilename;
	QPushButton *selectDestination;
	QHBoxLayout *layoutDestination;
	QWidget *widgetDestination;

	QPushButton *btnStart;
	QWidget *optionsView;

	QLabel *labelFormat;
	QComboBox *backupFormat;
	QLabel *labelJobs;
	QSpinBox *jobs;
	QCheckBox *chkbxVerbose;
	QLabel *labelCompression;
	QSpinBox *compression;
	QCheckBox *chkbxIncludeBlobs;
	QCheckBox *chkbxClean;
	QCheckBox *chkbxCreate;
	QCheckBox *noOwner;
	QCheckBox *oids;
	QComboBox *what;
	QCheckBox *noAcl;
	QWidget *progressView;
	QPlainTextEdit *stdOutput;
	QTextCharFormat m_OutputOkFormat;
	QTextCharFormat m_OutputErrorFormat;

	QLabel *labelDataOrSchema;

	QPushButton *btnBack;
	QPushButton *btnClose;
	QStackedLayout *viewStack;

	enum class Format {
		Normal, Success, Error
	};
	void writeOutput(const QString &s, Format f = Format::Normal);

	void setParams(QStringList &args);
private slots:

	void process_readyRead();
	void process_errorOccurred(QProcess::ProcessError error);
	void process_finished(int exitCode, QProcess::ExitStatus exitStatus);
	void on_btnStart_clicked();
	void on_backupFormat_currentIndexChanged(int index);
	void on_selectDestination_clicked();
	void on_btnBack_clicked();
};

#endif // BACKUPDIALOG_H
