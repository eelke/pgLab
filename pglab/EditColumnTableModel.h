﻿#ifndef EDITCOLUMNTABLEMODEL_H
#define EDITCOLUMNTABLEMODEL_H

#include <QAbstractTableModel>
#include "Pgsql_oids.h"
#include <memory>

class PgDatabaseCatalog;

class Column {
public:
	QString name;
	Oid type = InvalidOid;
	int length = -1; // varchar, date/time, numeric (precision)
	int scale = -1; // numeric scale
	Oid collate = InvalidOid;
	bool notNull = true;
	QString def;
	int primaryKey = 0;
	bool unique = false;
	QString comment;
};


class EditColumnTableModel: public QAbstractTableModel {
	Q_OBJECT
public:
	enum Columns : int {
		NameCol = 0,
		TypeCol,
		LengthCol,
		ScaleCol,
		CollateCol,
		NotNullCol,
		DefaultCol,
		PrimaryKeyCol,
		UniqueCol,
		CommentCol,

		colCount
	};

	EditColumnTableModel(std::shared_ptr<PgDatabaseCatalog> catalog, QObject *parent = nullptr);

	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	virtual QVariant data(const QModelIndex &index, int role) const override;

	virtual Qt::ItemFlags flags(const QModelIndex &) const override;
	virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;
	virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

public slots:
//	virtual bool submit() override;
//	virtual void revert() override;

private:
	using ColumnList = std::vector<Column>;

	std::shared_ptr<PgDatabaseCatalog> m_catalog;
	ColumnList m_data; ///< Better call it data as columns would be confusing
};


#endif // EDITCOLUMNTABLEMODEL_H
