#-------------------------------------------------
#
# Project created by QtCreator 2016-11-11T09:17:13
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets sql


TARGET = pglab
TEMPLATE = app

! include( ../common.pri ) {
error( "Couldn't find the common.pri file!" )
}

DEFINES += _WIN32_WINNT=0x0501

win32:RC_ICONS += pglab.ico

SOURCES += main.cpp\
    About.cpp \
    catalog/delegates/IconColumnDelegate.cpp \
    catalog/models/BaseTableModel.cpp \
    catalog/models/ColumnTableModel.cpp \
    catalog/models/ConstraintModel.cpp \
    catalog/models/DatabasesTableModel.cpp \
    catalog/models/DependantsTableModel.cpp \
    catalog/models/IndexModel.cpp \
    catalog/models/ProcTableModel.cpp \
    catalog/models/RolesTableModel.cpp \
    catalog/models/SequenceModel.cpp \
    catalog/models/TriggerTableModel.cpp \
    catalog/tables/TablesTableModel.cpp \
    catalog/widgets/CatalogConstraintPage.cpp \
    catalog/widgets/CatalogFunctionsPage.cpp \
    catalog/widgets/CatalogIndexPage.cpp \
    catalog/widgets/CatalogInspector.cpp \
    catalog/widgets/CatalogNamespacePage.cpp \
    catalog/widgets/CatalogPageBase.cpp \
    catalog/widgets/CatalogSequencesPage.cpp \
    catalog/widgets/CatalogTablesPage.cpp \
    catalog/widgets/CatalogTypesPage.cpp \
    catalog/widgets/ColumnPage.cpp \
    catalog/widgets/DependantsPage.cpp \
    catalog/widgets/TriggerPage.cpp \
    codeeditor/CodeEditor.cpp \
    codeeditor/EditorGutter.cpp \
    codeeditor/GutterPainter.cpp \
    crud/CrudModel.cpp \
    crud/CrudTab.cpp \
    querytool/QueryExplainModel.cpp \
    querytool/QueryParamListController.cpp \
    querytool/QueryResultModel.cpp \
    querytool/QueryTool.cpp \
    serverinspector/DatabasesPage.cpp \
    serverinspector/RolesPage.cpp \
    serverinspector/ServerInspector.cpp \
    util/PgLabItemDelegate.cpp \
    util/PgLabTableView.cpp \
    util/SqlSyntaxHighlighter.cpp \
    ConnectionConfigurationWidget.cpp \
    ConnectionController.cpp \
    ManagedPage.cpp \
    PgDumpOutputHighlighter.cpp \
    CreateDatabaseDialog.cpp \
    ConnectionManagerWindow.cpp \
    ConnectionListModel.cpp \
    SslModeModel.cpp \
    stopwatch.cpp \
    TuplesResultWidget.cpp \
    BackupDialog.cpp \
    MasterController.cpp \
    ParamTypeDelegate.cpp \
    OpenDatabase.cpp \
    ProcessStdioWidget.cpp \
    ResultTableModelUtil.cpp \
    NamespaceItemModel.cpp \
    PropertyProxyModel.cpp \
    CodeGenerator.cpp \
    UserConfiguration.cpp \
    widgets/SqlCodePreview.cpp \
    CustomFilterSortModel.cpp \
    PropertiesPage.cpp \
    PasswordPromptDialog.cpp \
    EditTableWidget.cpp \
    EditColumnTableModel.cpp \
    DatabaseWindow.cpp  \
    widgets/SingleRecordModel.cpp \
    widgets/SingleRecordWidget.cpp

HEADERS  += \
    About.h \
    Config.h \
    catalog/delegates/IconColumnDelegate.h \
    catalog/models/BaseTableModel.h \
    catalog/models/ColumnTableModel.h \
    catalog/models/ConstraintModel.h \
    catalog/models/DatabasesTableModel.h \
    catalog/models/DependantsTableModel.h \
    catalog/models/IndexModel.h \
    catalog/models/ProcTableModel.h \
    catalog/models/RolesTableModel.h \
    catalog/models/SequenceModel.h \
    catalog/models/TriggerTableModel.h \
    catalog/tables/TablesTableModel.h \
    catalog/widgets/CatalogConstraintPage.h \
    catalog/widgets/CatalogFunctionsPage.h \
    catalog/widgets/CatalogIndexPage.h \
    catalog/widgets/CatalogInspector.h \
    catalog/widgets/CatalogNamespacePage.h \
    catalog/widgets/CatalogPageBase.h \
    catalog/widgets/CatalogSequencesPage.h \
    catalog/widgets/CatalogTablesPage.h \
    catalog/widgets/CatalogTypesPage.h \
    catalog/widgets/ColumnPage.h \
    catalog/widgets/DependantsPage.h \
    catalog/widgets/TriggerPage.h \
    codeeditor/CodeEditor.h \
    codeeditor/EditorGutter.h \
    codeeditor/GutterPainter.h \
    crud/CrudModel.h \
    crud/CrudTab.h \
    querytool/QueryExplainModel.h \
    querytool/QueryParamListController.h \
    querytool/QueryResultModel.h \
    querytool/QueryTool.h \
    serverinspector/DatabasesPage.h \
    serverinspector/RolesPage.h \
    serverinspector/ServerInspector.h \
    util/PgLabItemDelegate.h \
    util/PgLabTableView.h \
    util/PgLabTableViewHelper.h \
    util/SqlSyntaxHighlighter.h \
    ConnectionConfigurationWidget.h \
    ConnectionController.h \
    IDatabaseWindow.h \
    ManagedPage.h \
    PgDumpOutputHighlighter.h \
    CreateDatabaseDialog.h \
    ConnectionManagerWindow.h \
    ConnectionListModel.h \
    SslModeModel.h \
    stopwatch.h \
    TuplesResultWidget.h \
    BackupDialog.h \
    MasterController.h \
    ParamTypeDelegate.h \
    OpenDatabase.h \
    ProcessStdioWidget.h \
    ResultTableModelUtil.h \
    NamespaceItemModel.h \
    AbstractCommand.h \
    PropertyProxyModel.h \
    CustomDataRole.h \
    CodeGenerator.h \
    UserConfiguration.h \
    widgets/SqlCodePreview.h \
    CustomFilterSortModel.h \
    PropertiesPage.h \
    PasswordPromptDialog.h \
    EditTableWidget.h \
    EditColumnTableModel.h \
    DatabaseWindow.h \
    NamespaceFilter.h  \
    widgets/SingleRecordModel.h \
    widgets/SingleRecordWidget.h

FORMS    += \
	ConnectionManagerWindow.ui \
	CreateDatabaseDialog.ui \
        DatabaseWindow.ui \
    TuplesResultWidget.ui \
    querytool/QueryTab.ui \
    ProcessStdioWidget.ui \
    crud/CrudTab.ui \
    CodeGenerator.ui \
    widgets/SingleRecordWidget.ui

RESOURCES += \
    resources.qrc

#QMAKE_LFLAGS_WINDOWS = /SUBSYSTEM:WINDOWS,5.01

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../core/release/ -lcore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../core/debug/ -lcore

INCLUDEPATH += $$PWD/../core
DEPENDPATH += $$PWD/../core

win32:CONFIG(debug, debug|release): LIBS += -lbotand
else:win32:CONFIG(release, debug|release): LIBS += -lbotan

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/libcore.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/libcore.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/release/core.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../core/debug/core.lib


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../pgsql/release/ -lpgsql
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../pgsql/debug/ -lpgsql
else:unix:!macx: LIBS += -L$$OUT_PWD/../pgsql/ -lpgsql

INCLUDEPATH += $$PWD/../pgsql
DEPENDPATH += $$PWD/../pgsql

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pgsql/release/libpgsql.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pgsql/debug/libpgsql.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pgsql/release/pgsql.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pgsql/debug/pgsql.lib
else:unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../pgsql/libpgsql.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../pglablib/release/ -lpglablib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../pglablib/debug/ -lpglablib
else:unix:!macx: LIBS += -L$$OUT_PWD/../pglablib/ -lpglablib

INCLUDEPATH += $$PWD/../pglablib
DEPENDPATH += $$PWD/../pglablib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pglablib/release/libpglablib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pglablib/debug/libpglablib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pglablib/release/pglablib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../pglablib/debug/pglablib.lib
else:unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../pglablib/libpglablib.a
