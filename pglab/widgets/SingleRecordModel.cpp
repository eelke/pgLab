#include "SingleRecordModel.h"
#include "CustomDataRole.h"

SingleRecordModel::SingleRecordModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QVariant SingleRecordModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    return {};
}

int SingleRecordModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return elems.size();
}

int SingleRecordModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return ColCount;
}

QVariant SingleRecordModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const int row = index.row();
    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
            case NameCol:
                return elems[row].name;
            case ValueCol:
                return elems[row].value;
        }
    }
    else if (role == CustomDataTypeRole)
    {
        switch (index.column())
        {
            case NameCol:
                return Pgsql::text_oid;
            case ValueCol:
                return elems[row].oid;
        }
    }
    else if (role == CustomDataMeaningRole)
    {
    }

    return QVariant();
}
