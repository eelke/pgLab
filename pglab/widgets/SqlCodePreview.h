﻿#ifndef SQLCODEPREVIEW_H
#define SQLCODEPREVIEW_H

#include "catalog/PgDatabaseCatalog.h"
#include <QPlainTextEdit>
#include <memory>

class SqlSyntaxHighlighter;
class PgDatabaseCatalog;

class SqlCodePreview : public QPlainTextEdit {
	Q_OBJECT
public:
	SqlCodePreview(QWidget *parent = nullptr);

	/** Sets the database catalog that the syntax highlighter is to use.
	 */
	void setCatalog(std::shared_ptr<const PgDatabaseCatalog> catalog);


	SqlSyntaxHighlighter *highlighter() { return m_highlighter; }
private:
	SqlSyntaxHighlighter *m_highlighter = nullptr;
	std::shared_ptr<const PgDatabaseCatalog> m_catalog;

private slots:
	void catalogRefresh(const PgDatabaseCatalog *catalog, PgDatabaseCatalog::RefreshFlags flags);
};

#endif // SQLCODEPREVIEW_H
