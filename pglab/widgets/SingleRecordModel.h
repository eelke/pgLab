#pragma once

#include <QAbstractTableModel>
#include "Pgsql_oids.h"

class SingleRecordModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit SingleRecordModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    enum Columns {
        NameCol,
        ValueCol,

        ColCount
    };

    class Elem {
    public:
        QString name;
        QString value;
        Oid oid;
    };
    using Elems = std::vector<Elem>;
    Elems elems;
};

