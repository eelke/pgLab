#include "SingleRecordWidget.h"
#include "ui_SingleRecordWidget.h"

SingleRecordWidget::SingleRecordWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SingleRecordWidget)
{
    ui->setupUi(this);
}

SingleRecordWidget::~SingleRecordWidget()
{
    delete ui;
}
