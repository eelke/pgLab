#pragma once

#include <QWidget>

namespace Ui {
    class SingleRecordWidget;
}

class SingleRecordWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SingleRecordWidget(QWidget *parent = nullptr);
    ~SingleRecordWidget();

private:
    Ui::SingleRecordWidget *ui;
};

