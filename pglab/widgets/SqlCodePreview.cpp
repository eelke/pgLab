﻿#include "SqlCodePreview.h"
#include "UserConfiguration.h"
#include "util/SqlSyntaxHighlighter.h"

SqlCodePreview::SqlCodePreview(QWidget *parent)
	: QPlainTextEdit(parent)
{
	auto&& config = UserConfiguration::instance();
	setFont(config->codeFont());
	setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::TextSelectableByKeyboard);
	setWordWrapMode(QTextOption::NoWrap);
	m_highlighter = new SqlSyntaxHighlighter(document());
}


void SqlCodePreview::setCatalog(std::shared_ptr<const PgDatabaseCatalog> catalog)
{
	connect(catalog.get(), &PgDatabaseCatalog::refreshed, this, &SqlCodePreview::catalogRefresh);
	catalogRefresh(catalog.get(), PgDatabaseCatalog::All);
}


void SqlCodePreview::catalogRefresh(const PgDatabaseCatalog *catalog, PgDatabaseCatalog::RefreshFlags flags)
{
	if (flags & PgDatabaseCatalog::Types) {
		m_highlighter->setTypes(*catalog->types());
	}
}
