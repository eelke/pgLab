﻿#ifndef CONNECTIONCONFIGURATIONWIDGET_H
#define CONNECTIONCONFIGURATIONWIDGET_H

#include <QWidget>
#include <QUuid>
#include <tuple>
#include "TestConnection.h"

class ConnectionController;
class ConnectionConfig;
class QCheckBox;
class QComboBox;
class QDataWidgetMapper;
class QFormLayout;
class QLabel;
class QLineEdit;
class QPushButton;
class QSpinBox;

class ConnectionTreeModel;

class ConnectionConfigurationWidget : public QWidget
{
	Q_OBJECT
public:
    static void editExistingInWindow(ConnectionController *ctrl, const ConnectionConfig &cfg,
                                     std::function<void(ConnectionConfigurationWidget&)> save_func);

    explicit ConnectionConfigurationWidget(
            ConnectionController *connection_controller,
            QWidget *parent = nullptr);

	void retranslateUi();
	void setData(const ConnectionConfig &cfg);
    ConnectionConfig data() const;
    QString group() const;

    bool savePassword() const;
    bool clearPassword() const;

public slots:
    void testConnection();

private:
    ConnectionController *m_connectionController;
    ConnectionTreeModel *m_connectionModel;

	QUuid m_uuid;
    QByteArray encodedPassword;
    bool passwordChanged;

	QLabel *lblGroup;
	QComboBox *cmbbxGroup;
	QLabel *lblName;
	QLineEdit *edtName;
	QLabel *lblHost;
	QLineEdit *edtHost;
	QLabel *lblPort;
	QSpinBox *spinPort;
	QLabel *lblUser;
	QLineEdit *edtUser;
    QLabel *lblPassword;
    QLineEdit *edtPassword;
    QCheckBox *cbSavePassword;
    QLabel *lblDbName;
    QComboBox *cmbDbname;
	QLabel *lblSsl;
	QComboBox *cmbbxSsl;
	QLabel *lblCert;
	QLineEdit *edtCert;
	QLabel *lblKey;
	QLineEdit *edtKey;
	QLabel *lblRootCert;
	QLineEdit *edtRootCert;
	QLabel *lblCrl;
	QLineEdit *edtCrl;

    QPushButton *btnTest;
    QLabel *lblResult;
    QFormLayout *formLayout;

    void handleTestResult(TestConnectionResult result);
    void passwordEdited(const QString &);
};

#endif // CONNECTIONCONFIGURATIONWIDGET_H
