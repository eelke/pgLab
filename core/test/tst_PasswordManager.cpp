﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "PasswordManager.h"

using namespace testing;


TEST(PasswordManager, initial_changeMasterPassword_returns_true)
{
	PasswordManager pwm;

	auto res = pwm.changeMasterPassword("", "my test passphrase");
	ASSERT_NO_THROW( res.get() );
	ASSERT_TRUE(res.get());
}

TEST(PasswordManager, unlock_succeeds)
{
	PasswordManager pwm;

	std::string passphrase = "my test passphrase";

	auto res = pwm.changeMasterPassword("", passphrase);
	ASSERT_NO_THROW( res.get() );
	ASSERT_TRUE(res.get());

	auto res2 = pwm.unlock(passphrase);
	ASSERT_NO_THROW( res2.get() );
	ASSERT_TRUE(res2.get());
}

TEST(PasswordManager, unlock_fails)
{
	PasswordManager pwm;

	std::string passphrase = "my test passphrase";

	auto res = pwm.changeMasterPassword("", passphrase);
	ASSERT_NO_THROW(res.get());
	ASSERT_TRUE(res.get());

	auto res2 = pwm.unlock(passphrase + "2");
	ASSERT_NO_THROW(res2.get());
	ASSERT_FALSE(res2.get());
}

TEST(PasswordManager, test_save_get)
{
	PasswordManager pwm;

	std::string passphrase = "my test passphrase";

	auto res = pwm.changeMasterPassword("", passphrase);
	ASSERT_NO_THROW( res.get() );
	ASSERT_TRUE(res.get());

//	auto res2 = pwm.unlock(passphrase + "2");
//	ASSERT_NO_THROW( res2.get() );
//	ASSERT_THAT( res2.get(), Eq(false) );

	const std::string password = "password123";
	const std::string key = "abc";

	auto res2 = pwm.savePassword(key, password);
	ASSERT_TRUE(res2.valid());

	std::string result;
	auto res3 = pwm.getPassword(key, result);
	ASSERT_TRUE(res3.valid());
	ASSERT_TRUE(res3.get());
	ASSERT_EQ(result, password);
}
