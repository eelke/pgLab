﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "ScopeGuard.h"

using namespace testing;


TEST(ScopeGuard, normal_run_fun_on_destruction_1)
{
	bool result = false;
	auto sg = scopeGuard([&result]() { result = true; });
	ASSERT_FALSE(result);
}

TEST(ScopeGuard, normal_run_fun_on_destruction_2)
{
	bool result = false;
	{
		auto sg = scopeGuard([&result]() { result = true; });
	}

	ASSERT_TRUE(result);
}

TEST(ScopeGuard, dismiss)
{
	bool result = false;
	{
		auto sg = scopeGuard([&result]() { result = true; });
		sg.dismiss();
	}

	ASSERT_FALSE(result);
}

TEST(ScopeGuard, SCOPE_EXIT_macro_1)
{
	bool result = false;
	{
		SCOPE_EXIT { result = true; };
		ASSERT_FALSE(result); // prove previous statement hasn't run yet
	}

}

TEST(ScopeGuard, SCOPE_EXIT_macro_2)
{
	bool result = false;
	{
		SCOPE_EXIT { result = true; };
	}

	ASSERT_TRUE(result);
}



//TEST(expected, get_when_valid_returns_value)
//{
//	Expected<int> v = getAnswerToEverything();
//	ASSERT_THAT(v.get(), Eq(42));
//}

