﻿#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "SqlLexer.h"

using namespace testing;


TEST(SqlLexer, lexer)
{
	QString input = " SELECT  ";
	SqlLexer lexer(input, LexerState::Null);

	int startpos, length;
	BasicTokenType tokentype;
	QString out;
	lexer.nextBasicToken(startpos, length, tokentype, out);

	ASSERT_EQ(startpos, 1);
	ASSERT_EQ(length, 6);
	ASSERT_EQ(tokentype, BasicTokenType::Symbol);
	ASSERT_EQ( out, QString("SELECT") );
}

TEST(SqlLexer, lexer_quote_in_string)
{
    QString input = " 'abc''def'  ";
    SqlLexer lexer(input, LexerState::Null);

    int startpos, length;
    BasicTokenType tokentype;
    QString out;
    lexer.nextBasicToken(startpos, length, tokentype, out);
    
    ASSERT_EQ(startpos, 1);
    ASSERT_EQ(length, 10);
    ASSERT_EQ(tokentype, BasicTokenType::QuotedString);
}

