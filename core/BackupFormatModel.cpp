﻿#include "BackupFormatModel.h"
#include <vector>

namespace {

	class BackupFormatItem {
	public:
		const QString shortFlag;
		const QString longFlag;
		const QString description;

		BackupFormatItem(QString s, QString l, QString d)
			: shortFlag(std::move(s))
			, longFlag(std::move(l))
			, description(std::move(d))
		{}
	};

	using t_BackupFormatItemVector = std::vector<BackupFormatItem>;


	t_BackupFormatItemVector g_BackupFormats = {
		BackupFormatItem{ "p", "plain (-Fp)", "Output a plaintext SQL script, restore with psql" },
		BackupFormatItem{ "c", "custom (-Fc)", "Postgresql's own format most flexible and compressed, restore with pg_restore" },
		BackupFormatItem{ "d", "directory (-Fd)", "Generates a directory with a file for each table or blob" },
		BackupFormatItem{ "t", "tar (-Ft)", "Similar to directory if untarred it results in a valid directory backup" }
	};

} // end of unnamed namespace



BackupFormatModel::BackupFormatModel(QObject *parent)
	: QAbstractListModel(parent)
{

}

int BackupFormatModel::rowCount(const QModelIndex &) const
{
	int size = static_cast<int>(g_BackupFormats.size());
	return size;
}

int BackupFormatModel::columnCount(const QModelIndex &) const
{
	return 3;

}

QVariant BackupFormatModel::data(const QModelIndex &index, int role) const
{
	QVariant result;
	if (index.isValid()) {
		const int row = index.row();
		const int col = index.column();

		if (role == Qt::DisplayRole) {
			const auto &item = g_BackupFormats.at(static_cast<size_t>(row));
			switch (col) {
			case ColumnShort:
				result = item.shortFlag;
				break;
			case ColumnLong:
				result = item.longFlag;
				break;
			case ColumnDescription:
				result = item.description;
				break;
			}
		}
		else if (role == Qt::ToolTipRole) {
			const auto &item = g_BackupFormats.at(static_cast<size_t>(row));
			result = item.description;
		}
	}
	return result;
}
