﻿#include <sstream>
#include <stdexcept>

namespace boost
{
	void assertion_failed(char const * expr, char const * function, char const * file, long line)
	{
		std::ostringstream out;
		out << "Assertion failure int " << function << " " << file << ":" << line << "\n" << expr;
		throw std::runtime_error(out.str());
	}

	void assertion_failed_msg(char const * expr, char const * msg, char const * function, char const * file, long line)
	{
		std::ostringstream out;
		out << "Assertion failure int " << function << " " << file << ":" << line << "\n" << msg << "\n" << expr;
		throw std::runtime_error(out.str());
	}
}

