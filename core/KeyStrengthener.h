﻿#ifndef KEYSTRENGTHENER_H
#define KEYSTRENGTHENER_H

#include <QSqlDatabase>
#include <botan/pwdhash.h>
#include <botan/secmem.h>
#include <memory>

class KeyStrengthener {
public:
	KeyStrengthener() = default;
	KeyStrengthener(std::unique_ptr<Botan::PasswordHash> hasher, Botan::secure_vector<uint8_t> salt, size_t keysize);

	KeyStrengthener(const KeyStrengthener&) = delete;
	KeyStrengthener& operator=(const KeyStrengthener &) = delete;

	KeyStrengthener(KeyStrengthener &&rhs);

	KeyStrengthener& operator=(KeyStrengthener &&rhs);

	Botan::secure_vector<uint8_t> derive(const std::string &passphrase);
	void saveParams(QSqlDatabase &db, const QString &table_name);
private:
	std::unique_ptr<Botan::PasswordHash> m_hasher;
	Botan::secure_vector<uint8_t> m_salt;
	size_t m_keySize;
};

#endif // KEYSTRENGTHENER_H
