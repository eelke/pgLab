#-------------------------------------------------
#
# Project created by QtCreator 2017-02-26T10:51:14
#
#-------------------------------------------------

QT       -= gui
QT       += sql

TARGET = core
TEMPLATE = lib
CONFIG += staticlib

! include( ../common.pri ) {
error( "Couldn't find the common.pri file!" )
}

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += my_boost_assert_handler.cpp \
    KeyStrengthener.cpp \
    SqlLexer.cpp \
    PasswordManager.cpp \
    CsvWriter.cpp \
    BackupFormatModel.cpp \
    ExplainTreeModelItem.cpp \
    jsoncpp.cpp

HEADERS += PasswordManager.h \
    KeyStrengthener.h \
    SqlLexer.h \
    ScopeGuard.h \
    CsvWriter.h \
    BackupFormatModel.h \
	Expected.h \
    ExplainTreeModelItem.h \
    json/json.h \
    std_utils.h \
    IntegerRange.h
	
unix {
    target.path = /usr/lib
    INSTALLS += target
}
