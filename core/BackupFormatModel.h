﻿#ifndef BACKUPFORMATMODEL_H
#define BACKUPFORMATMODEL_H

#include <QAbstractListModel>


class BackupFormatModel : public QAbstractListModel
{
	Q_OBJECT

public:
	enum Column { ColumnShort=1, ColumnLong=0, ColumnDescription=2 };

	explicit BackupFormatModel(QObject *parent);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:

};

#endif // BACKUPFORMATMODEL_H
