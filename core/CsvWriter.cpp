﻿#include "CsvWriter.h"

CsvWriter::CsvWriter() = default;

CsvWriter::CsvWriter(QTextStream *output)
	: m_output(output)
{}

void CsvWriter::setDestination(QTextStream *output)
{
	m_output = output;
	m_column = 0;
}

void CsvWriter::setSeperator(QChar ch)
{
	m_seperator = ch;
}

void CsvWriter::setQuote(QChar ch)
{
	m_quote = ch;
}

void CsvWriter::writeField(QString field)
{
	QTextStream &out = *m_output;
	if (m_column > 0) {
		out << m_seperator;
	}
	// if field contains any of seperator, quote or newline then it needs to be quoted
	// when quoted quotes need to be doubled to escape them
	bool needs_quotes = false;
	for (auto ch : field) {
		if (ch == '\n' || ch == m_seperator || ch == m_quote) {
			needs_quotes = true;
			break;
		}
	}

	if (needs_quotes) {
		out << m_quote;
		for (auto ch : field) {
			if (ch == m_quote)
				out << m_quote;
			out << ch;
		}
		out << m_quote;
	}
	else {
		out << field;
	}
	++m_column;
}


void CsvWriter::nextRow()
{
	QTextStream &out = *m_output;
	out << '\n';
	m_column = 0;
}

