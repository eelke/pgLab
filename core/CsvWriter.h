﻿#ifndef CSVWRITER_H
#define CSVWRITER_H

#include <ostream>
#include <QString>
#include <QTextStream>

class CsvWriter {
public:
	CsvWriter();
	explicit CsvWriter(QTextStream *output);
	void setDestination(QTextStream *output);
	void setSeperator(QChar ch);
	void setQuote(QChar ch);
	void writeField(QString field);
	void nextRow();
private:
	QChar m_seperator = ',';
	QChar m_quote = '"';
	QTextStream *m_output = nullptr;
	int m_column = 0;
};

#endif // CSVWRITER_H
