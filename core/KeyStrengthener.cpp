﻿#include "KeyStrengthener.h"
#include <botan/base64.h>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>
#include <stdexcept>

KeyStrengthener::KeyStrengthener(std::unique_ptr<Botan::PasswordHash> hasher, Botan::secure_vector<uint8_t> salt, size_t keysize)
	: m_hasher (std::move(hasher))
	, m_salt   (std::move(salt))
	, m_keySize(keysize)
{}

KeyStrengthener::KeyStrengthener(KeyStrengthener &&rhs)
	: m_hasher (std::move(rhs.m_hasher))
	, m_salt   (std::move(rhs.m_salt))
	, m_keySize(rhs.m_keySize)
{}

KeyStrengthener &KeyStrengthener::operator=(KeyStrengthener &&rhs)
{
	if (&rhs != this) {
		m_hasher  = std::move(rhs.m_hasher);
		m_salt    = std::move(rhs.m_salt);
		m_keySize = rhs.m_keySize;
	}
	return *this;
}

Botan::secure_vector<uint8_t> KeyStrengthener::derive(const std::string &passphrase)
{
	Botan::secure_vector<uint8_t> master_key(m_keySize);
	m_hasher->derive_key(master_key.data(), master_key.size(), passphrase.c_str(), passphrase.length(), m_salt.data(), m_salt.size());

	return master_key;
}

void KeyStrengthener::saveParams(QSqlDatabase &db, const QString &table_name)
{
    size_t i1 = m_hasher->memory_param();
    size_t i2 = m_hasher->iterations();
    size_t i3 = m_hasher->parallelism();

	auto salt_str = QString::fromUtf8(Botan::base64_encode(m_salt).c_str());
	// SAVE parameters in database
	QSqlQuery insert_statement(db);
	insert_statement.prepare("INSERT OR REPLACE INTO " + table_name + "(id, algo, i1, i2, i3, ks, salt) "
								+ "VALUES(:id, :algo, :i1, :i2, :i3, :ks, :salt)");
	insert_statement.bindValue(":id", 1);
	insert_statement.bindValue(":algo", "Scrypt");
	insert_statement.bindValue(":i1", i1);
	insert_statement.bindValue(":i2", i2);
	insert_statement.bindValue(":i3", i3);
	insert_statement.bindValue(":ks", m_keySize);
	insert_statement.bindValue(":salt", salt_str);
	if (!insert_statement.exec()) {
		throw std::runtime_error("PasswordManager::KeyStrengthener::saveParams failed");
//		auto err = insert_statement.lastError();
	}
}
