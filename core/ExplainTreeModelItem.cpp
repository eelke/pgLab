﻿#include "ExplainTreeModelItem.h"
#include "json/json.h"
#include <limits>

namespace {

	class registerMetaTypes {
	public:
		registerMetaTypes()
		{
			qRegisterMetaType<ExplainRoot::SPtr>();
		}
	} registerMetaTypes_instance;

	ExplainTreeModelItemPtr createPlanItemFromJson(Json::Value &plan)
	{

		const auto json_null = Json::Value::nullSingleton();
		ExplainTreeModelItemPtr result = std::make_shared<ExplainTreeModelItem>();
		result->nodeType = QString::fromStdString(plan.get("Node Type", json_null).asString());
		result->parallelAware = plan.get("Parallel Aware", json_null).asBool();
		result->strategy = QString::fromStdString(plan.get("Strategy", json_null).asString());
		result->joinType = QString::fromStdString(plan.get("Join Type", json_null).asString());
		result->startupCost = plan.get("Startup Cost", json_null).asFloat();
		result->totalCost = plan.get("Total Cost", json_null).asFloat();
		result->estimatedRows = plan.get("Plan Rows", json_null).asInt();
		result->planWidth = plan.get("Plan Width", json_null).asInt();
		result->actualStartupTime = plan.get("Actual Startup Time", json_null).asFloat();
		result->actualTotalTime = plan.get("Actual Total Time", json_null).asFloat();
		result->actualRows = plan.get("Actual Rows", json_null).asInt();
		result->actualLoops = plan.get("Actual Loops", json_null).asInt();

		result->relationName = QString::fromStdString(plan.get("Relation Name", json_null).asString());
		result->alias = QString::fromStdString(plan.get("Alias", json_null).asString());
		result->scanDirection = QString::fromStdString(plan.get("Scan Direction", json_null).asString());
		result->indexName = QString::fromStdString(plan.get("Index Name", json_null).asString());
		result->indexCondition = QString::fromStdString(plan.get("Index Cond", json_null).asString());
		result->indexRecheck = QString::fromStdString(plan.get("Rows Removed by Index Recheck", json_null).asString());
		result->filter = QString::fromStdString(plan.get("Filter", json_null).asString());
		result->hashCondition = QString::fromStdString(plan.get("Hash Cond", json_null).asString());

		result->sortKey = QString::fromStdString(plan.get("Sort Key", json_null).toStyledString());
		result->sortMethod = QString::fromStdString(plan.get("Sort Method", json_null).asString());

		result->sortSpaceUsed = plan.get("Sort Space Used", json_null).asInt();
		result->sortSpaceType = QString::fromStdString(plan.get("Sort Space Type", json_null).asString());

		result->sharedBlocks.hit = plan.get("Shared Hit Blocks", json_null).asInt();
		result->sharedBlocks.read = plan.get("Shared Read Blocks", json_null).asInt();
		result->sharedBlocks.dirtied = plan.get("Shared Dirtied Blocks", json_null).asInt();
		result->sharedBlocks.written = plan.get("Shared Written Blocks", json_null).asInt();

		result->localBlocks.hit = plan.get("Local Hit Blocks", json_null).asInt();
		result->localBlocks.read = plan.get("Local Read Blocks", json_null).asInt();
		result->localBlocks.dirtied = plan.get("Local Dirtied Blocks", json_null).asInt();
		result->localBlocks.written = plan.get("Local Written Blocks", json_null).asInt();

		result->tempBlocks.read = plan.get("Temp Read Blocks", json_null).asInt();
		result->tempBlocks.written = plan.get("Temp Written Blocks", json_null).asInt();
		result->ioTimes.read = plan.get("I/O Read Time", json_null).asDouble();
		result->ioTimes.write = plan.get("I/O Write Time", json_null).asDouble();

		Json::Value &plans = plan["Plans"];
		if (plans.isArray()) {
			for (auto p : plans) {
				result->appendChild(
					createPlanItemFromJson(p));
			}
		}

//		"Parallel Aware": false,
		return result;
	}

} // END of unnamed namespace

ExplainRoot::SPtr ExplainRoot::createFromJson(Json::Value &json)
{
	auto res = std::make_shared<ExplainRoot>();
	// Explain always seems to be an array with one element
	if (json.isArray()) {
		if (!json.empty()) {
			Json::Value &explain = json[0];

			Json::Value &plan = explain["Plan"];
			res->plan = createPlanItemFromJson(plan);

			res->planningTime  = explain["Planning Time"].asFloat();
			res->executionTime = explain["Execution Time"].asFloat();
			res->totalRuntime  = explain["Total Runtime"].asFloat();
		}
	}
	return res;
}

ExplainTreeModelItem::ExplainTreeModelItem() = default;

ExplainTreeModelItem::~ExplainTreeModelItem() = default;

void ExplainTreeModelItem::appendChild(const ItemPtr &child)
{
	child->setParent(shared_from_this());
	m_childItems.push_back(child);
}

ExplainTreeModelItemPtr ExplainTreeModelItem::child(int row)
{
	return m_childItems.at(row);
}

int ExplainTreeModelItem::childCount() const
{
	return static_cast<int>(m_childItems.size());
}

int ExplainTreeModelItem::row() const
{
	int idx = 0;
	auto p = m_parentItem.lock();
	if (p) {
		idx = std::find(p->m_childItems.begin(), p->m_childItems.end(), shared_from_this()) - p->m_childItems.begin();
	}
	return idx;
}

void ExplainTreeModelItem::setParent(const ItemPtr &parent)
{
	m_parentItem = parent;
}

ExplainTreeModelItemPtr ExplainTreeModelItem::parent()
{
	auto p = m_parentItem.lock();
	return p;
}

float ExplainTreeModelItem::exclusiveTime() const
{
	float tt = inclusiveTime();
	for (auto&& c : m_childItems) {
		tt -= c->inclusiveTime();
	}
	return tt;
}

float ExplainTreeModelItem::inclusiveTime() const
{
	float t = actualTotalTime * actualLoops;
	return t;
}

float ExplainTreeModelItem::estimateError() const
{
	float res = 1.0;
	if (estimatedRows > actualRows) {
		if (actualRows > 0) {
			res = float(estimatedRows) / actualRows;
		}
		else {
			res = std::numeric_limits<float>::infinity();
		}
	}
	else if (actualRows > estimatedRows) {
		if (estimatedRows > 0) {
			res = float(actualRows) / estimatedRows;
		}
		else {
			res = std::numeric_limits<float>::infinity();
		}
		res = -res;
	}
	return res;
}

QString ExplainTreeModelItem::detailString() const
{
	QString s;
	if (!joinType.isEmpty()) {
		s += joinType + " " + nodeType + " ";
	}
	if (!strategy.isEmpty()) {
		s += strategy + " " + nodeType + " ";
	}
	if (!indexName.isEmpty()) {
		s+= scanDirection + " "
				+ nodeType + "\n";
		if (!indexCondition.isEmpty()) {
			s += "cond: " + indexCondition + " ";
		}
		if (!filter.isEmpty()) {
			s += "filter: " + filter + "\n";
		}
		if (!indexRecheck.isEmpty()) {
			s += "removed by recheck: " + indexRecheck + "\n";
		}
		s += "idx: " + indexName + " rel: " + alias + " ";
	}
	else {
		if (!alias.isEmpty()) {
			s += nodeType + " rel: " + alias + " ";
		}
	}
	if (!hashCondition.isEmpty()) {
		s += hashCondition + " ";
	}
	if (!sortMethod.isEmpty()) {
		s += sortMethod + " " + sortSpaceType + " "
				+ QString::number(sortSpaceUsed) + "kB "
				+ sortKey + " ";

	}

	return s.trimmed();
}
