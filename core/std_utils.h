﻿#ifndef STD_UTILS_H
#define STD_UTILS_H

#include <vector>

template <typename T>
const T& value_or(const std::vector<T> &vec, const size_t index, const T &def)
{
	if (index < 0 || index >= vec.size())
		return def;
	return vec[index];
}

#endif // STD_UTILS_H
